Feature: STOPit feature

  Scenario: As a valid user I can log into my app 
    When I press "Decline"
    Then I see "DECLINE"
    Then I wait
    Then I press "Decline"
    Then I see "Back"
    Then I wait
    Then I press "Back"
    Then I see "Decline"
    Then I wait
    Then I scroll down 
    Then I scroll down 
    Then I scroll down 
    Then I scroll down 
    Then I scroll down 
    Then I scroll down 
    Then I scroll down 
    Then I wait
    Then I wait
    Then I wait
    Then I wait
    Then I wait
    Then I wait
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I scroll down 
    Then I wait
    Then I see "I Accept"
    Then I press "I Accept"
    Then I wait
    Then I see "Let\'s get started"
    Then I press "Let\'s get started"
    Then I wait
    Then I see "I know my access code"
    Then I press "I know my access code"
    Then I wait
    Then I see "Access Code"
    Then I enter "parkhillmays" into "Access Code"
    Then I wait
    Then I press "Next"
    Then I wait
    Then I press "Continue"
    Then I wait
    Then I press "Report"
    Then I wait
    Then I enter "This is from automated test" into "Add message or notes"
    Then I wait
    Then I press "Send"
    Then I wait
    Then I wait
    Then I press "Report"
    Then I wait
    Then I go back
    Then I wait
