package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;

import java.util.HashMap;

/**
 * Main STOPit android application
 */
public class StopitApplication extends Application {

    private static Context context;

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     *
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    private final HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();
    public static Tracker tracker;

    @Override
    public void onCreate() {
        super.onCreate();
        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        if (RemoteDataManager.isPROD) {
            //Prod Key
            Parse.initialize(this, "X1oDeeFYY0mx4iqhG5q5UBHEkxdkV2hadg6iIuoF", "uCrwnlsLenEUZRLiA6yQXG0PNIpM9X498EQn74wy");
        }else {
            //pre-prod Key
            Parse.initialize(this, "HCNCJA2dVNKGZznt0CSXZ0IAYKUbXll5H0kmW95u", "xoikgUOCkfDjNk8vqzm3VDFgtocIYDtXwHEJ8uG6");
        }
        ParseInstallation.getCurrentInstallation().saveInBackground();


        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(30);

        tracker = getTracker(TrackerName.APP_TRACKER);


        AnalyticsManager.sendEvent(this, AnalyticsManager.ANALYTICS_CATEGORY_APP_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INITIAL_LAUNCH, AnalyticsManager.ANALYTICS_ACTION_INITIAL_LAUNCH);

        StopitApplication.context = getApplicationContext();
    }

    private synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = analytics.newTracker(R.xml.app_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }
    public static Context getAppContext() {
        return StopitApplication.context;
    }

}
