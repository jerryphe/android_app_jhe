package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

import com.amazonaws.regions.Regions;

/**
 * Object model to store data related to Amazon Web Services upload.
 */
public class AWS {
    private String cognito_pool_id;
    private String region;
    private String bucket;
    private Regions region_code;

    public Regions getRegion_code() {
        Regions rc = Regions.US_EAST_1;

        switch (this.region) {
            case "us-east-1":
                rc = Regions.US_EAST_1;
                break;
            case "eu-west-1":
                rc = Regions.EU_WEST_1;
                break;
            case "ap-northeast-1":
                rc = Regions.AP_NORTHEAST_1;
                break;
        }

        return rc;
    }

    public Regions getBucketRegion_code() {
        Regions rc = getRegion_code();

        if (getRegion_code() == Regions.EU_WEST_1)
        {
            rc = Regions.US_WEST_2;
        }

        return rc;
    }

    public String getCognito_pool_id() {
        return cognito_pool_id;
    }

    public void setCognito_pool_id(String cognito_pool_id) {
        this.cognito_pool_id = cognito_pool_id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }
}
