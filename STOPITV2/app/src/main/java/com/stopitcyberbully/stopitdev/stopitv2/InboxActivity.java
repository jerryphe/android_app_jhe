package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.os.Handler;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.InboxMessage;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.MessageStatus;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class InboxActivity extends AppBaseActivity implements InboxMessageFragment.OnListFragmentInteractionListener {
    private TextView unreadCount;
    private InboxMessageRecyclerViewAdapter adapter;
    private Button editButton;
    Button markAsUnReadButton;
    Button deleteButton;
    private ProgressBar inboxRefreshing;
    Boolean isEditing = true;
    ProgressDialog barProgressDialog;
    Handler updateBarHandler;
    boolean shouldRefresh = false;

    public boolean isSwipeMenuOpen() {
        return isSwipeMenuOpen;
    }

    public void setIsSwipeMenuOpen(boolean isSwipeMenuOpen, int pos) {
        this.isSwipeMenuOpen = isSwipeMenuOpen;
        if ( (this.position != -1) && (this.position != pos))
        {
            adapter.notifyItemChanged(this.position);
        }
        if (this.position == pos) this.position = -1;

        if (this.isSwipeMenuOpen()) {
            this.position = pos;
        }else{
            this.position = -1;
        }
    }

    boolean isSwipeMenuOpen = false;
    int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        inboxRefreshing = (ProgressBar) findViewById(R.id.inbox_refresh);

        editButton = (Button) findViewById(R.id.inbox_edit_mode);
        deleteButton = (Button) findViewById(R.id.inbox_button_mark_as_delete);
        markAsUnReadButton = (Button) findViewById(R.id.inbox_button_mark_as_unread);

        if (editButton != null)
        {
            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isEditing())
                    {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_CLICKED_CANCEL_ON_EDITING_NOTIFICATIONS, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_CLICKED_CANCEL_ON_EDITING_NOTIFICATIONS);
                    }else{
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_CLICKED_EDIT_ON_EDITING_NOTIFICATIONS, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_CLICKED_EDIT_ON_EDITING_NOTIFICATIONS);
                    }
                    setIsSwipeMenuOpen(false, -1);
                    toggleEdit();
                }
            });
            resetEditButton();
        }

        if (markAsUnReadButton != null)
        {
            markAsUnReadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialogWhileProcessing(v, "unread");
                }
            });
        }

        if (deleteButton != null)
        {
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    AlertDialog alertInbox;
                    AlertDialog.Builder inboxAlert = new AlertDialog.Builder(InboxActivity.this);
                    inboxAlert.setMessage(getResources().getString(R.string.inbox_delete_selected_confirm));

                    inboxAlert.setCancelable(false);
                    inboxAlert.setPositiveButton(getResources().getString(R.string.inbox_delete_cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_SELECTED_NO_WHEN_ASKED_TO_DELETE, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_SELECTED_NO_WHEN_ASKED_TO_DELETE);
                                    dialog.cancel();
                                }
                            });

                    inboxAlert.setNegativeButton(getResources().getString(R.string.inbox_delete_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_SELECTED_YES_WHEN_ASKED_TO_DELETE, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_SELECTED_YES_WHEN_ASKED_TO_DELETE);
                                    dialog.cancel();
                                    showDialogWhileProcessing(v, "delete");
                                }
                            });

                    alertInbox = inboxAlert.create();
                    alertInbox.show();
                }
            });
        }




        CrownMolding reportButton = (CrownMolding) findViewById(R.id.btn_report);
        CrownMolding helpButton = (CrownMolding) findViewById(R.id.btn_help);


        if ((reportButton != null) && (helpButton != null)) {
            if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
                reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
                helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.GREEN_MOLDING);
            } else {
                reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.REPORT_MOLDING);
                helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);
            }
            reportButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    refreshRegistrationPriorToReport();

                }
            });
            helpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoHelpScreen();
                }
            });
        }

        TextView reportView = (TextView) findViewById(R.id.txt_report);
        CustomFontHelper.setCustomFont(reportView, "source-sans-pro.semibold.ttf", this);

        TextView helpView = (TextView) findViewById(R.id.txt_help);
        CustomFontHelper.setCustomFont(helpView, "source-sans-pro.semibold.ttf", this);
        refreshUnreadCount();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_inbox);
        adapter = new InboxMessageRecyclerViewAdapter(RemoteDataManager.getInboxMessageArrayList(), this, this);
        if ( (recyclerView != null) ) {
            recyclerView.setAdapter(adapter);
        }


        if (savedInstanceState != null) {
            //If the editing mode is on, we flip it because the toggle call will flip it back again.
            isEditing = !savedInstanceState.getBoolean("EDIT_MODE");
            toggleEdit(true);
        }else {
            toggleEdit();
        }






        RecyclerView.OnItemTouchListener onItemTouchListener = new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        };

        recyclerView.addOnItemTouchListener(onItemTouchListener);

        if (savedInstanceState == null)
        {
            shouldRefresh = true;
        }else{
            //First check if the app is in Editing mode.
            isEditing = savedInstanceState.getBoolean("EDIT_MODE");
            if (isEditing()) {
                shouldRefresh = false;
                if (inboxRefreshing != null) {
                    inboxRefreshing.setVisibility(View.INVISIBLE);
                }
            }else {
                shouldRefresh = true;
            }
        }


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("EDIT_MODE", isEditing());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        if (shouldRefresh)
        {
            getInboxList();
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }
    public boolean isEditing()
    {
        return isEditing;
    }

    private void toggleEdit()
    {
        toggleEdit(false);
    }

    private void toggleEdit(Boolean rotationInEffect )
    {
        isEditing = !isEditing;
        LinearLayout bottomActionLayout = (LinearLayout) findViewById(R.id.inbox_bottom_banner);
        RelativeLayout bottomLayout = (RelativeLayout) findViewById(R.id.layout_bottom_action);
        if (isEditing & !isSwipeMenuOpen)
        {
            if (!rotationInEffect) {
                RemoteDataManager.deselectInBoxMessages();
            }
            editButton.setText(R.string.inbox_cancel_mode);

            if ( (bottomLayout != null) && (bottomActionLayout != null))
            {

                bottomActionLayout.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams paramsAction = bottomActionLayout.getLayoutParams();
                paramsAction.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
                bottomActionLayout.setLayoutParams(paramsAction);

                ViewGroup.LayoutParams params = bottomLayout.getLayoutParams();
                params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 155, getResources().getDisplayMetrics());
                bottomLayout.setLayoutParams(params);

            }
        }else {
            editButton.setText(R.string.inbox_edit_mode);
            if ( (bottomLayout != null) && (bottomActionLayout != null))
            {
                bottomActionLayout.setVisibility(View.INVISIBLE);
                ViewGroup.LayoutParams paramsAction = bottomActionLayout.getLayoutParams();
                paramsAction.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, getResources().getDisplayMetrics());
                bottomActionLayout.setLayoutParams(paramsAction);

                ViewGroup.LayoutParams params = bottomLayout.getLayoutParams();
                params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 110, getResources().getDisplayMetrics());
                bottomLayout.setLayoutParams(params);
            }
        }
        if (adapter!= null)
        {
            adapter.notifyDataSetChanged();
        }

        boolean unreadSelected = false;
        boolean anythingSelected = false;

        for (int i=0; i<RemoteDataManager.getInboxMessageArrayList().size(); i++){
            InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(i);
            if (inboxMessage.getIsSelected()) {
                anythingSelected = true;
                if (inboxMessage.getMessageStatusID() == MessageStatus.INBOX_MESSAGE_STATUS_READ) {
                    unreadSelected = true;
                    break;
                }
            }

        }
        updateActionButtons(unreadSelected, anythingSelected);
    }
    private void refreshUnreadCount()
    {
        if (unreadCount == null)
        {
            unreadCount = (TextView) findViewById(R.id.unread_count);
        }
        if (unreadCount != null) {
            if (RemoteDataManager.getUnreadMessageCount(this) > 0) {
                unreadCount.setText(getResources().getString(R.string.inbox_count, RemoteDataManager.getUnreadMessageCount(this)));
                unreadCount.setVisibility(View.VISIBLE);
            } else {
                unreadCount.setText("");
                unreadCount.setVisibility(View.INVISIBLE);
            }
        }
        refreshMenuCount();
    }

//Inbox Related
private void getInboxList()
{
    if (inboxRefreshing != null)
    {
        inboxRefreshing.setVisibility(View.VISIBLE);
    }
    RemoteDataManager.getInboxMessageList(this, RemoteDataManager.getIntellicode(this), new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            adapter.notifyDataSetChanged();
            if (inboxRefreshing != null)
            {
                inboxRefreshing.setVisibility(View.INVISIBLE);
            }
            updateInboxCounts(false);
            resetEditButton();
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            adapter.notifyDataSetChanged();
            if (inboxRefreshing != null)
            {
                inboxRefreshing.setVisibility(View.INVISIBLE);
            }
            resetEditButton();
        }
    });
}

    private void resetEditButton()
    {
        if (RemoteDataManager.getInboxMessageArrayList().size() <=0)
        {
            editButton.setEnabled(false);
        }else{
            editButton.setEnabled(true);
        }
    }
    private void gotoReportScreen(){
        if (RemoteDataManager.isConnected(this)){
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_REPORT_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_REPORT_SCREEN_USING_MENU);

            Intent reportIntent = new Intent(InboxActivity.this, ReportActivity.class);
            InboxActivity.this.startActivity(reportIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoReportScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void gotoHelpScreen(){
        if (RemoteDataManager.isConnected(this)){
//            if (RemoteDataManager.getRegistration().getOrg().getOrgTypeID() != RemoteDataManager.ORG_TYPE_HIGHERED) {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_GET_HELP_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_GET_HELP_SCREEN_USING_MENU);
            Intent helpIntent = new Intent(InboxActivity.this, GetHelpActivity.class);
            InboxActivity.this.startActivity(helpIntent);
//            } else {
//                Intent helpHigherEdIntent = new Intent(HomeActivity.this, GetHigherEdHelpActivity.class);
//                HomeActivity.this.startActivity(helpHigherEdIntent);
//
//            }
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoHelpScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void refreshRegistrationPriorToReport()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    if (json.getClass().equals(JSONObject.class)) {
                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    progressDialog.dismiss();
                                    if (!RemoteDataManager.getRegistration().getOrg().getIsAlive()) {
                                        //Org is not alive, we need to show blocked screen
                                        Intent blockedIntent = new Intent(InboxActivity.this, BlockedGateActivity.class);
                                        blockedIntent.putExtra("ORG_NOT_LIVE", true);
                                        InboxActivity.this.startActivity(blockedIntent);
                                    } else if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                        String tag = "recess_dialog";
                                        DialogFragment recessFragment =
                                                RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                        recessFragment.show(getSupportFragmentManager(), tag);
                                    } else {

                                        gotoReportScreen();
                                    }
                                }
                            } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(InboxActivity.this, BlockedGateActivity.class);
                                InboxActivity.this.startActivity(blockedIntent);
                            }
                        } catch (JSONException je) {
                            progressDialog.dismiss();
                            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(InboxActivity.this);
                            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                            anonymousAlert.setCancelable(false);
                            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            progressDialog.dismiss();
                                            refreshRegistrationPriorToReport();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = anonymousAlert.create();
                            alert.show();

                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(InboxActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    refreshRegistrationPriorToReport();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }

    @Override
    public void onListFragmentInteraction(final InboxMessage item, final int position) {
        if (RemoteDataManager.SHOW_LOG) Log.d("Inbox", item.toString());
        if (!isEditing() && !isSwipeMenuOpen())
        {
            if (RemoteDataManager.isConnected(this)) {
                if (inboxRefreshing != null) {
                    inboxRefreshing.setVisibility(View.VISIBLE);
                }
                RemoteDataManager.getInboxMessage(this, RemoteDataManager.getIntellicode(this), item.getMessageID(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (RemoteDataManager.SHOW_LOG) Log.d("Inbox", response.toString());
                        Intent inboxIntent = new Intent(InboxActivity.this, InboxMessageReaderActivity.class);
                        inboxIntent.putExtra("INBOX_MESSAGE", response.toString());
                        inboxIntent.putExtra("INBOX_ITEM", item);
                        inboxIntent.putExtra("INBOX_POSITION", position);
                        try {
                            inboxIntent.putExtra("INBOX_ATTACHMENT", response.getInt("mime_type"));
                        }catch (JSONException je)
                        {
                            if (RemoteDataManager.SHOW_LOG) Log.e("Inbox", je.getMessage());
                        }
                        InboxActivity.this.startActivity(inboxIntent);
                        if (inboxRefreshing != null) {
                            inboxRefreshing.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (RemoteDataManager.SHOW_LOG) Log.e("Inbox", error.getMessage());
                        if (inboxRefreshing != null) {
                            inboxRefreshing.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
        }
    }

    public void showDialogWhileProcessing(View view, String action) {

        int selected_count = 0;
        for (InboxMessage inboxMessage: RemoteDataManager.getInboxMessageArrayList()) {
            if (inboxMessage.getIsSelected()) {
                selected_count++;
            }
        }
        //If nothing is selected do not do anything
        if (selected_count == 0) return;

        barProgressDialog = new ProgressDialog(InboxActivity.this);
        barProgressDialog.setTitle(getResources().getString(R.string.inbox_app_activity_test));
        barProgressDialog.setMessage(getResources().getString(R.string.inbox_app_activity_test));
        barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        barProgressDialog.setProgress(0);
        barProgressDialog.setMax(selected_count);
        barProgressDialog.show();

        for (InboxMessage inboxMessage: RemoteDataManager.getInboxMessageArrayList())
        {
            if (inboxMessage.getIsSelected())
            {
                RemoteDataManager.UpdateInboxMessageStatus(this, RemoteDataManager.getIntellicode(this), inboxMessage.getMessageID(), action, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        barProgressDialog.incrementProgressBy(1);
                        if (barProgressDialog.getProgress() == barProgressDialog.getMax()) {
                            barProgressDialog.dismiss();
                            getInboxList();
                            setIsSwipeMenuOpen(false, -1);
                            if (isEditing()) toggleEdit();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        barProgressDialog.incrementProgressBy(1);
                        if (barProgressDialog.getProgress() == barProgressDialog.getMax()) {
                            barProgressDialog.dismiss();
                            getInboxList();
                            if (isEditing()) toggleEdit();
                        }
                    }
                });
            }
        }
    }

    @Override
    public void updateActionButtons(boolean readSelected, boolean anythingSelected) {
        if (readSelected) {
            markAsUnReadButton.setEnabled(true);
        }else {
            markAsUnReadButton.setEnabled(false);
        }
        if (anythingSelected) {
            deleteButton.setEnabled(true);
        }else {
            deleteButton.setEnabled(false);
        }
    }

}
