package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.ArrayList;

/**
 * Custom adapter to show different images for different rows.
 */
public class GenericImageListAdapter extends ArrayAdapter<String>

    {

    private final Activity context;
    private final ArrayList<String > web;
    private final Integer[] imageId;


    public GenericImageListAdapter(Activity context, ArrayList<String> labels, Integer[] imageId) {
        super(context, R.layout.tablecell);
        this.context = context;
        this.web =labels;
        this.imageId = imageId;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();

        if (convertView==null)
        {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.tablecell, parent, false);

            viewHolder.itemImageView = (ImageView) convertView.findViewById(R.id.cell_icon);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.cell_title);

            //Check if this is a workplace user, we will have to change back color for them
//            if (RemoteDataManager.getSavedOrgType(this.context) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
//
//                LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.cell_background_layout);
//                if (layout != null) {
//                    layout.setBackgroundResource(R.drawable.pro_onboarding_selector);
//                }
//            }

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }



        viewHolder.textView.setText(web.get(position));
        CustomFontHelper.setCustomFont(viewHolder.textView, "source-sans-pro.semibold.ttf", context);

        if (imageId.length <= position)
        {
            viewHolder.itemImageView.setImageResource(imageId[0]);
        }else {
            viewHolder.itemImageView.setImageResource(imageId[position]);
        }
        return convertView;
    }

        @Override
        public String getItem(int position) {
            return this.web.get(position);
        }

        @Override
        public int getCount() {
            return this.web.size();
        }



        private static class ViewHolder {
            private ImageView itemImageView;
            private TextView textView;
        }
    }
