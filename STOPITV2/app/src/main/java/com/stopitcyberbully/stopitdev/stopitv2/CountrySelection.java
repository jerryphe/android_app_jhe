package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.CodeNameImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Country;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Organization;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CountrySelection extends AppCompatActivity {

    private CodeNameImageListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        setContentView(R.layout.activity_country_selection);
        final ListView listview = (ListView) findViewById(R.id.country_selection_listview);

        EditText inputSearch = (EditText) findViewById(R.id.search_country);
        CustomFontHelper.setCustomFont(inputSearch, "source-sans-pro.semibold.ttf", this);
        if (inputSearch != null) {
            inputSearch.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.search_country_text) + "</small>"));
        }


        TextView textCountry = (TextView) findViewById(R.id.describe_heading_view);
        CustomFontHelper.setCustomFont(textCountry, "source-sans-pro.semibold.ttf", this);


// Set some constants
        Bitmap sourceGlobeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.globe);


// Crop bitmap
        Bitmap globeBitmap = Bitmap.createBitmap(sourceGlobeBitmap, 0, 0, sourceGlobeBitmap.getWidth() / 2, sourceGlobeBitmap.getHeight(), null, false);



        ImageView globeImage = (ImageView) findViewById(R.id.image_globe);
        globeImage.setImageBitmap(globeBitmap);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        globeImage.setAnimation(alphaAnimation);

// Set some constants
        Bitmap sourceEclipseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ellipse);


// Crop bitmap
        Bitmap eclipseBitmap = Bitmap.createBitmap(sourceEclipseBitmap, 0, 0, sourceEclipseBitmap.getWidth() / 2, sourceEclipseBitmap.getHeight(), null, false);



        ImageView eclipseImage = (ImageView) findViewById(R.id.image_eclipse);
        eclipseImage.setImageBitmap(eclipseBitmap);
        eclipseImage.setAnimation(alphaAnimation);

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CountrySelection.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        Integer[] imageId = {R.drawable.globe_white};

        adapter = new CodeNameImageListAdapter(this,RemoteDataManager.getCountryList(),imageId);

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                String countryName = adapter.getItem(position).getName();
                for (Country country : RemoteDataManager.getCountryList())
                {
                    if (country.getName().equals(countryName))
                    {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SELECTED_COUNTRY, countryName);

                        RemoteDataManager.setCountry(country);
                        findOrganizations(country.getCode(), RemoteDataManager.getCURRENT_ORG_TYPE());
                    }
                }

            }
        });


    }

    private void findOrganizations (String countryCode, int org_type) {
        RequestQueue queue = RemoteDataManager.sharedManager(this);
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();
        final JSONObject jsoninputObject = new JSONObject();
        final JSONObject encryptedJsonObject = new JSONObject();
        try {
            jsoninputObject.put("country_id", countryCode);
            jsoninputObject.put("org_name", "");
            jsoninputObject.put("org_type_id", org_type);

            encryptedJsonObject.put("data", RemoteDataManager.encrypt(specialKey, jsoninputObject.toString(), randomIV));
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e) {
            if (RemoteDataManager.SHOW_LOG)
            {
                Log.e("Country", e.getMessage());
            }
            e.printStackTrace();
        }




        AuthRequest orgRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getOrgSearchURL(), encryptedJsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {
                        if (dataEncrypted.has("data"))
                        {
                            JSONObject jsonObject = null;
                            if (dataEncrypted.has("data")) {

                                try {
                                    String countryencrypted = dataEncrypted.getString("data");
                                    String decodedCountry = RemoteDataManager.decrypt(specialKey, countryencrypted, randomIV);
                                    jsonObject = new JSONObject(decodedCountry);
                                } catch (Exception e) {
                                    //Unable to decode
                                }
                            }
                            if (jsonObject == null)
                            {
                                return;
                            }
                            ArrayList<Organization> orgArray = new ArrayList<>();
                            try {
                                JSONArray orgs = jsonObject.getJSONArray("orgs");
                                for (int i = 0; i < orgs.length(); i++) {
                                    Organization org = new Organization();
                                    JSONObject orgJson = (JSONObject) orgs.get(i);
                                    org.setOrgName(orgJson.getString("org_name"));
                                    org.setOrgTypeID(jsoninputObject.getInt("org_type_id"));
                                    org.setOrgID(orgJson.getInt("org_id"));
                                    org.setRosterLabel(orgJson.getString("roster_label"));
                                    org.setRosterType(orgJson.getString("roster_type"));
                                    orgArray.add(org);
                                }
                                RemoteDataManager.setOrgList(orgArray);
                                Intent orgIntent = new Intent(CountrySelection.this, FindOrganizationActivity.class);
                                CountrySelection.this.startActivity(orgIntent);
                            }catch (JSONException e)
                            {
                                Toast.makeText(CountrySelection.this, getResources().getString(R.string.error_in_retrieval), Toast.LENGTH_SHORT).show();

                            }
                        }else{
                            String errorMessage;
                            switch (RemoteDataManager.getCURRENT_ORG_TYPE())
                            {
                                case RemoteDataManager.ORG_TYPE_K12:
                                    errorMessage = getResources().getString(R.string.not_found_K12);
                                    break;
                                case RemoteDataManager.ORG_TYPE_HIGHERED:
                                    errorMessage = getResources().getString(R.string.not_found_COLLEGE);
                                    break;
                                case RemoteDataManager.ORG_TYPE_WORKPLACE:
                                    errorMessage = getResources().getString(R.string.not_found_WORK);
                                    break;
                                default:
                                    errorMessage = getResources().getString(R.string.not_found_K12);
                            }

                            Toast.makeText(CountrySelection.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(CountrySelection.this, getResources().getString(R.string.error_in_retrieval), Toast.LENGTH_SHORT).show();
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(orgRequest);
    }

}
