package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

import java.util.ArrayList;

/**
 * Object model to store data related to contact information obtained from webservice (typically
 * contacts api)
 */
public class UserContacts {
    private ArrayList<Contact> contactArrayList;
    public ArrayList<Contact> getContactArrayList() {
        return contactArrayList;
    }
    public void setContactArrayList(ArrayList<Contact> contactArrayList) {
        this.contactArrayList = contactArrayList;
    }

    public ArrayList<String> getContactArrayStringList() {
        ArrayList<String> stringContacts = new ArrayList<>();
        for (Contact contact : contactArrayList)
        {
            stringContacts.add(contact.getName());
        }
        return stringContacts;
    }
    public  ArrayList<Contact> getNonOrgContacts(){
        ArrayList<Contact> nonOrg = new ArrayList<>();
        for (Contact contact : contactArrayList)
        {
            if (contact.getOrg_id()==0)
            {
                nonOrg.add(contact);
            }
        }
        return nonOrg;
    }
    public  ArrayList<Contact> getTrustedContacts(){
        ArrayList<Contact> trusted = new ArrayList<>();
        if (contactArrayList != null) {
            for (Contact contact : contactArrayList) {
                if (contact.isTrustedContact()) {
                    trusted.add(contact);
                }
            }
        }
        return trusted;
    }

    public  ArrayList<Contact> getHelpContacts(){
        ArrayList<Contact> help = new ArrayList<>();

        if (contactArrayList != null) {
            for (Contact contact : contactArrayList) {
                if (contact.isGetHelpContact()) {
                    help.add(contact);
                }
            }
        }
        return help;
    }
}
