package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


public class InboxMessage implements Parcelable {
    String subject;
    String messageID;
    Date createdDate;
    Boolean isSelected;
    MessageStatus messageStatusID;

    public MessageStatus getMessageStatusID() {
        return messageStatusID;
    }

    public void setMessageStatusID(MessageStatus messageStatusID) {
        this.messageStatusID = messageStatusID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public Boolean getIsSelected() {
        if (isSelected == null) isSelected = false;
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public InboxMessage(String pSubject, String pMessageID, Date pCreateDate, Boolean pSelected, MessageStatus pMessageStatus )
    {
        subject = pSubject;
        messageID = pMessageID;
        createdDate = pCreateDate;
        isSelected = pSelected;
        messageStatusID = pMessageStatus;
    }

    protected InboxMessage(Parcel in) {
        subject = in.readString();
        messageID = in.readString();
        long tmpCreatedDate = in.readLong();
        createdDate = tmpCreatedDate != -1 ? new Date(tmpCreatedDate) : null;
        byte isSelectedVal = in.readByte();
        isSelected = isSelectedVal == 0x02 ? null : isSelectedVal != 0x00;
        messageStatusID = (MessageStatus) in.readValue(MessageStatus.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subject);
        dest.writeString(messageID);
        dest.writeLong(createdDate != null ? createdDate.getTime() : -1L);
        if (isSelected == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isSelected ? 0x01 : 0x00));
        }
        dest.writeValue(messageStatusID);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<InboxMessage> CREATOR = new Parcelable.Creator<InboxMessage>() {
        @Override
        public InboxMessage createFromParcel(Parcel in) {
            return new InboxMessage(in);
        }

        @Override
        public InboxMessage[] newArray(int size) {
            return new InboxMessage[size];
        }
    };

    @Override
    public boolean equals(Object other) {
        if (other == null)
        {
            return false;
        }

        if (this.getClass() != other.getClass())
        {
            return false;
        }else{
            InboxMessage otherMessage = (InboxMessage ) other;

            if (this.getMessageID() == null) return false;
            if (otherMessage.getMessageID() == null) return false;
            if (
                    (this.getMessageID().equals(otherMessage.getMessageID()))
                            &&
                            (this.getMessageID().equals(otherMessage.getMessageID()))
                    )
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getMessageID().hashCode();
    }
}
