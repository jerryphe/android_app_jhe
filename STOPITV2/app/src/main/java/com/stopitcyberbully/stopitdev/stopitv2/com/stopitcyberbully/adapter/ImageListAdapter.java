package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.IncidentObject;

import java.util.List;

/**
 * Custom adapter to store array of images.
 */
public class ImageListAdapter extends ArrayAdapter<IncidentObject> {

    private static class ViewHolder {
        private ImageView itemImageView;
    }

    private List <IncidentObject> incidents = null;

    public ImageListAdapter(Context context, int resource, List<IncidentObject> incidentImageList) {
        super(context, resource);
        this.incidents = incidentImageList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();

        if (convertView==null)
        {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.imagecells, parent, false);

            viewHolder.itemImageView = (ImageView) convertView.findViewById(R.id.imported_image);
            convertView.setTag(viewHolder);

        }

        IncidentObject incidentObject = getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image_for_upload);

        Picasso.with(convertView.getContext()).load(incidentObject.getUri()).placeholder(R.drawable.video_placeholder).resize(80, 80).centerCrop().into(imageView);
        return convertView;
    }

    @Override
    public int getCount() {
        return this.incidents.size();
    }

    @Override
    public IncidentObject getItem(int position) {
        return this.incidents.get(position);
    }

}
