package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.OnboardingImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Country;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class DescribeActivity extends AppCompatActivity {


    private static final int DESCRIBE_K12 = 0;
    private static final int DESCRIBE_COLLEGE = 1;
    private static final int DESCRIBE_WORKPLACE = 2;
    private static final int DESCRIBE_ACCESS_CODE = 3;

    private ProgressDialog progressDialog;


    private RequestQueue queue = null;
    private ArrayList<Country> countryArray = null;
    private ProgressBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_describe);
        queue = RemoteDataManager.sharedManager(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }


// Set some constants
        Bitmap sourceUserBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.users);


// Crop bitmap
        Bitmap userBitmap = Bitmap.createBitmap(sourceUserBitmap, 0, 0, sourceUserBitmap.getWidth()/2, sourceUserBitmap.getHeight(), null, false);



        ImageView userImage = (ImageView) findViewById(R.id.image_user);
        userImage.setImageBitmap(userBitmap);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        userImage.setAnimation(alphaAnimation);



// Set some constants
        Bitmap sourceEclipseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ellipse);


// Crop bitmap
        Bitmap eclipseBitmap = Bitmap.createBitmap(sourceEclipseBitmap, 0, 0, sourceEclipseBitmap.getWidth() / 2, sourceEclipseBitmap.getHeight(), null, false);



        ImageView eclipseImage = (ImageView) findViewById(R.id.image_eclipse);
        eclipseImage.setImageBitmap(eclipseBitmap);
        eclipseImage.setAnimation(alphaAnimation);

        TextView textHeading = (TextView) findViewById(R.id.describe_heading_view);
        CustomFontHelper.setCustomFont(textHeading, "source-sans-pro.semibold.ttf", this);

        AnalyticsManager.sendEvent(this, AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_DESCRIBE_YOURSELF_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_DESCRIBE_YOURSELF_SCREEN);

        bar = (ProgressBar) findViewById(R.id.country_load_progreassbar);

            final ListView listview = (ListView) findViewById(R.id.description_list);
            String[] values = new String[] {
                    getResources().getString(R.string.ONBOARDING_K12),
                    getResources().getString(R.string.ONBOARDING_COLLEGE),
                    getResources().getString(R.string.ONBOARDING_WORK),
            };

            final ArrayList<String> list = new ArrayList<>();
            Collections.addAll(list, values);

            //call get countries
            getCountries();


        Integer[] imageId = {R.drawable.school_white,
        R.drawable.college_white,
        R.drawable.work_white,
        R.drawable.access_skip};
            OnboardingImageListAdapter adapter = new OnboardingImageListAdapter(this, list, imageId);


            listview.setAdapter(adapter);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {

                    if (countryArray == null)
                    {
                        Toast.makeText(DescribeActivity.this, "Unable to get list of countries", Toast.LENGTH_SHORT).show();
                     return;
                    }
                    switch (position)
                    {
                        case DESCRIBE_K12:
                        {

                            if (countryArray.size() >0)
                            {
                                AnalyticsManager.sendEvent( AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SELECTED_K12_OPTION, AnalyticsManager.ANALYTICS_ACTION_SELECTED_K12_OPTION);
                                Intent countryIntent = new Intent(DescribeActivity.this, CountrySelection.class);
                                RemoteDataManager.setCURRENT_ORG_TYPE(RemoteDataManager.ORG_TYPE_K12);
                                RemoteDataManager.setCountryList(countryArray);
                                DescribeActivity.this.startActivity(countryIntent);
                            }
                        }
                            break;
                        case DESCRIBE_COLLEGE:
                        {
                            if (countryArray.size() >0)
                            {
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SELECTED_COLLEGE_OPTION, AnalyticsManager.ANALYTICS_ACTION_SELECTED_COLLEGE_OPTION);
                                Intent countryIntent = new Intent(DescribeActivity.this, CountrySelection.class);
                                RemoteDataManager.setCURRENT_ORG_TYPE(RemoteDataManager.ORG_TYPE_HIGHERED);
                                RemoteDataManager.setCountryList(countryArray);
                                DescribeActivity.this.startActivity(countryIntent);
                            }

                        }
                            break;
                        case DESCRIBE_WORKPLACE:
                        {
                            if (countryArray.size() >0)
                            {
                                AnalyticsManager.sendEvent( AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SELECTED_OFFICE_OPTION, AnalyticsManager.ANALYTICS_ACTION_SELECTED_OFFICE_OPTION);
                                Intent countryIntent = new Intent(DescribeActivity.this, CountrySelection.class);
                                RemoteDataManager.setCURRENT_ORG_TYPE(RemoteDataManager.ORG_TYPE_WORKPLACE);
                                RemoteDataManager.setCountryList(countryArray);
                                DescribeActivity.this.startActivity(countryIntent);
                            }

                        }
                            break;
                        case DESCRIBE_ACCESS_CODE:
                        {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SELECTED_ACCESS_CODE_OPTION, AnalyticsManager.ANALYTICS_ACTION_SELECTED_ACCESS_CODE_OPTION);

                            Intent accessCodeIntent = new Intent(DescribeActivity.this, EnterAccessCodeActivity.class);
                            DescribeActivity.this.startActivity(accessCodeIntent);

                        }
                            break;
                    }
                }
            });
    }
    private void getCountries ()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        bar.setVisibility(View.VISIBLE);
        JSONObject countryRequestObject = null;
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();
        try{
             countryRequestObject = new JSONObject();
            countryRequestObject.put("data",randomIV);
        }catch (JSONException je)
        {
            return;
        }
        //get countries
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getCOUNTRY_URL(), countryRequestObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {
                        JSONObject json = null;
                        try {
                            if (dataEncrypted.has("data")) {
                                String countryencrypted = dataEncrypted.getString("data");

                                try {
                                    String decodedCountry = RemoteDataManager.decrypt(specialKey, countryencrypted, randomIV);
                                    json = new JSONObject(decodedCountry);
                                } catch (Exception e) {
                                    //Unable to decode
                                }
                                if (json == null)
                                {
                                    return;
                                }
                                countryArray = new ArrayList<>();
                                JSONArray countries = json.getJSONArray("countries");
                                for (int c = 0; c < countries.length(); c++) {
                                    Country country = new Country();
                                    country.setCode(countries.getJSONObject(c).getString("code"));
                                    country.setName(countries.getJSONObject(c).getString("name"));
                                    countryArray.add(country);
                                }
                                bar.setVisibility(View.GONE);
                            }
                        }catch (JSONException je)
                        {
                            if (RemoteDataManager.SHOW_LOG) Log.d("DescribeActivity", je.toString());
                        }
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //show alert
                progressDialog.dismiss();
                AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(bar.getContext());
                anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                anonymousAlert.setCancelable(false);
                anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getCountries();
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = anonymousAlert.create();
                alert.show();
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(stringRequest);
    }
    private void showProgress(String message)
    {
        progressDialog = ProgressDialog.show(DescribeActivity.this, "", message);
        new Thread() {

            public void run() {

                try{
                    sleep(6000*60*60);
                } catch (Exception e) {
                    if (RemoteDataManager.SHOW_LOG) Log.e("tag", e.getMessage());
                }
                progressDialog.dismiss();

            }

        }.start();

    }

}
