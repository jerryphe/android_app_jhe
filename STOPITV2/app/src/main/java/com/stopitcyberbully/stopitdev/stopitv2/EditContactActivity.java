package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


public class EditContactActivity extends AppBaseActivity {
    private EditText contactName;
    private EditText contactMobilePhone;
    private EditText contactEmail;

    private Button button_save;

    private CheckBox checkBox_trusted_adult;
    private CheckBox checkbox_1touch_contact;
    private CheckBox checkbox_gethelp_contact;


    private int last_operation;

    private static final int OP_ADD = 1;
    private static final int OP_SAVE = 2;
    private static final int OP_DELETE = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        //Contact Name
        contactName = (EditText) findViewById(R.id.contact_name);
        contactMobilePhone = (EditText) findViewById(R.id.contact_mobile_phone);
        contactEmail = (EditText) findViewById(R.id.contact_email);

        contactName.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.hint_contact_name) + "</small>"));
        contactMobilePhone.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.hint_contact_mobile_phone) + "</small>"));
        contactEmail.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.hint_contact_email) + "</small>"));


        button_save = (Button)findViewById(R.id.button_save);
        Button button_cancel = (Button) findViewById(R.id.button_cancel);
        Button button_delete = (Button) findViewById(R.id.button_delete);

        checkBox_trusted_adult = (CheckBox) findViewById(R.id.checkbox_trusted_adult);
        checkbox_1touch_contact = (CheckBox) findViewById(R.id.checkbox_1touch_contact);
        checkbox_gethelp_contact = (CheckBox) findViewById(R.id.checkbox_gethelp_contact);


        checkBox_trusted_adult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox_trusted_adult.isChecked())
                {
                    checkbox_1touch_contact.setEnabled(true);
                }else{
                    checkbox_1touch_contact.setChecked(false);
                    checkbox_1touch_contact.setEnabled(false);
                }
            }
        });



        //Title
        TextView textView = (TextView) findViewById(R.id.title_add_update_contacts);
        CustomFontHelper.setCustomFont(textView, "source-sans-pro.semibold.ttf", this);

        //Check if this is a workplace user, we will have to change back color for them
        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {

            View bgView = findViewById(R.id.background_gradient_view);
            if (bgView != null) {
                bgView.setBackgroundResource(R.drawable.pro_onboarding_selector);
            }
            button_save.setBackgroundResource(R.color.pro_contact_button);
            button_save.setTextColor(ContextCompat.getColor(this, R.color.pro_contact_button_text_color));

            button_cancel.setBackgroundResource(R.color.pro_contact_button);
            button_cancel.setTextColor(ContextCompat.getColor(this, R.color.pro_contact_button_text_color));

            button_delete.setBackgroundResource(R.color.pro_contact_button);
            button_delete.setTextColor(ContextCompat.getColor(this, R.color.pro_contact_button_text_color));

            textView.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));


        }

        CustomFontHelper.setCustomFont(button_save, "source-sans-pro.semibold.ttf", this);
        CustomFontHelper.setCustomFont(button_cancel, "source-sans-pro.semibold.ttf", this);
        CustomFontHelper.setCustomFont(button_delete, "source-sans-pro.semibold.ttf", this);

        CustomFontHelper.setCustomFont(checkBox_trusted_adult, "source-sans-pro.semibold.ttf", this);
        CustomFontHelper.setCustomFont(checkbox_1touch_contact, "source-sans-pro.semibold.ttf", this);
        CustomFontHelper.setCustomFont(checkbox_gethelp_contact, "source-sans-pro.semibold.ttf", this);


        ImageView imageView_trusted_adult = (ImageView) findViewById(R.id.help_trusted_adult);
        ImageView imageView_1touch_contact = (ImageView) findViewById(R.id.help_1touch_contact);
        ImageView imageView_gethelp_contact = (ImageView) findViewById(R.id.help_gethelp_contact);

        imageView_trusted_adult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showPopup(ImageView_trusted_adult);
                Intent viewHelpIntent = new Intent(EditContactActivity.this, HelpViewActivity.class);
                viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/" + getString(R.string.reportcontacts) + ".html");
                EditContactActivity.this.startActivity(viewHelpIntent);

                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_1TouchHelp_Report_Contact, AnalyticsManager.ANALYTICS_ACTION_OPENED_1TouchHelp_Report_Contact);


            }
        });

        imageView_1touch_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showPopup(ImageView_1touch_contact);
                Intent viewHelpIntent = new Intent(EditContactActivity.this, HelpViewActivity.class);
                viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/" + getString(R.string.touchcontacts) + ".html");
                EditContactActivity.this.startActivity(viewHelpIntent);

                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_1TouchHelp_1Touch_Contact, AnalyticsManager.ANALYTICS_ACTION_OPENED_1TouchHelp_1Touch_Contact);

            }
        });


        imageView_gethelp_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showPopup(ImageView_gethelp_contact);
                Intent viewHelpIntent = new Intent(EditContactActivity.this, HelpViewActivity.class);
                viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/" + getString(R.string.gethelpcontacts) + ".html");
                EditContactActivity.this.startActivity(viewHelpIntent);

                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_1TouchHelp_Get_Help_Contact, AnalyticsManager.ANALYTICS_ACTION_OPENED_1TouchHelp_Get_Help_Contact);

            }
        });



        if (RemoteDataManager.getCurrentContact()!=null)
        {
            textView.setText(getResources().getText(R.string.edit_existing_contact));
            contactName.setText( RemoteDataManager.getCurrentContact().getName());
            if (RemoteDataManager.getCurrentContact().getMobile_phone() != null) {
                contactMobilePhone.setText(PhoneNumberUtils.formatNumber(RemoteDataManager.getCurrentContact().getMobile_phone()));


            }
            contactEmail.setText(RemoteDataManager.getCurrentContact().getEmail());

            if (RemoteDataManager.getCurrentContact().isTrustedContact()) {
                checkBox_trusted_adult.setChecked(true);
            }else {
                checkBox_trusted_adult.setChecked(false);
            }
            if (RemoteDataManager.getCurrentContact().isOneTouchContact()) {
                checkbox_1touch_contact.setChecked(true);
            }else {
                checkbox_1touch_contact.setChecked(false);
            }
            if (RemoteDataManager.getCurrentContact().isGetHelpContact()) {
                checkbox_gethelp_contact.setChecked(true);
            }else {
                checkbox_gethelp_contact.setChecked(false);
            }
            toggleSaveButtonState();
        }else{
            textView.setText(getResources().getText(R.string.add_new_contact));
            button_delete.setVisibility(View.GONE);
            button_save.setEnabled(false);
            checkbox_1touch_contact.setEnabled(false);
            checkbox_gethelp_contact.setEnabled(false);

        }

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (! isValidEmail(contactEmail.getText()))
                {
                    showGenericDialog(getResources().getString(R.string.invalid_email_address));
                    return;
                }

                if (contactMobilePhone.getText().length()>0)
                {
                    if (!isValidPhone(contactMobilePhone.getText().toString()))
                    {
                        showGenericDialog(getResources().getString(R.string.invalid_phone_number));
                        return;
                    }
                }

                saveContact();
            }
        });

        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteConfirmDialog();
            }
        });
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CANCELED_ON_CONTACT, AnalyticsManager.ANALYTICS_ACTION_CANCELED_ON_CONTACT);

                finish();
            }
        });

        contactName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                toggleSaveButtonState();
            }
        });

        contactMobilePhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        contactMobilePhone.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                toggleSaveButtonState();
            }
        });

        contactEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                toggleSaveButtonState();
            }
        });

    }
    private void toggleSaveButtonState()
    {

        if ( (contactName.getText().length()>0)  && (contactEmail.getText().length()>0))
        {
            button_save.setEnabled(true);
        }else{
            button_save.setEnabled(false);
        }

        if (contactMobilePhone.getText().length()==0)
        {
            checkbox_gethelp_contact.setChecked(false);
            checkbox_gethelp_contact.setEnabled(false);
        }else{
            checkbox_gethelp_contact.setEnabled(true);
        }

    }
    private void showDeleteConfirmDialog()
    {
        AlertDialog.Builder confirmDeleteBuilder = new AlertDialog.Builder(EditContactActivity.this);
        confirmDeleteBuilder.setMessage(getResources().getString(R.string.message_contact_delete_confirm));
        confirmDeleteBuilder.setCancelable(true);
        confirmDeleteBuilder.setPositiveButton(getResources().getString(R.string.YES),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteContact();
                        dialog.cancel();
                    }
                });
        confirmDeleteBuilder.setNegativeButton(getResources().getString(R.string.NO),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog errorDialog = confirmDeleteBuilder.create();
        errorDialog.show();
    }
private void deleteContact(){
    if ( RemoteDataManager.getCurrentContact()!=null) {
        last_operation = OP_DELETE;
        showProgress(getResources().getString(R.string.message_contact_deleting));
        RequestQueue queue = RemoteDataManager.sharedManager(this);
        AuthRequest deleteRequest = new AuthRequest(Request.Method.DELETE, RemoteDataManager.getSaveContactsURL() + "/" + RemoteDataManager.getCurrentContact().getUser_id(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_DELETED_CONTACT, AnalyticsManager.ANALYTICS_ACTION_DELETED_CONTACT);

                getUserContacts();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showRetryDialog();
            }
        },RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(deleteRequest);
    }
}

    private void saveContact(){
        try {
            JSONObject jsonUser = new JSONObject();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("first_name", contactName.getText());
            jsonObject.put("mobile_phone", PhoneNumberUtils.stripSeparators(contactMobilePhone.getText().toString()));
            jsonObject.put("email", contactEmail.getText());
            jsonObject.put("is_one_touch", checkbox_1touch_contact.isChecked() ? "Y":"N");
            jsonObject.put("is_get_help", checkbox_gethelp_contact.isChecked() ? "Y":"N");
            jsonObject.put("is_trusted", checkBox_trusted_adult.isChecked() ? "Y":"N");

            if (RemoteDataManager.getRegistration().getUser().getSalt().length()>0) {
                String unEncrypted = jsonObject.toString();
                String password = RemoteDataManager.getRegistration().getUser().getIntellicode() + RemoteDataManager.getRegistration().getUser().getSalt();

                String enCrypted = RemoteDataManager.encrypt(password, unEncrypted);
                jsonUser.put("data", enCrypted);
            }else {
                jsonUser.put("data", jsonObject);
            }

            RequestQueue queue = RemoteDataManager.sharedManager(this);

            if (RemoteDataManager.getCurrentContact() != null) {
                last_operation = OP_SAVE;
                showProgress(getResources().getString(R.string.message_contact_updating));
                AuthRequest updateRequest = new AuthRequest(Request.Method.PUT, RemoteDataManager.getSaveContactsURL()+"/" + RemoteDataManager.getCurrentContact().getUser_id(), jsonUser, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_UPDATED_CONTACT, AnalyticsManager.ANALYTICS_ACTION_UPDATED_CONTACT);

                        getUserContacts();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        showRetryDialog();
                    }
                }, RemoteDataManager.DEFAULT_TIMEOUT);
                queue.add(updateRequest);
            }else {
                last_operation = OP_ADD;
                showProgress(getResources().getString(R.string.message_contact_saving));
                AuthRequest saveRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getSaveContactsURL(), jsonUser, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ADDED_NEW_CONTACT, AnalyticsManager.ANALYTICS_ACTION_ADDED_NEW_CONTACT);
                        if (checkbox_1touch_contact.isChecked())
                        {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ADDED_1TOUCH_CONTACT, AnalyticsManager.ANALYTICS_ACTION_ADDED_1TOUCH_CONTACT);

                        }
                        if (checkbox_gethelp_contact.isChecked())
                        {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ADDED_GET_HELP_CONTACT, AnalyticsManager.ANALYTICS_ACTION_ADDED_GET_HELP_CONTACT);

                        }
                        if (checkBox_trusted_adult.isChecked())
                        {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ADDED_REPORT_CONTACT, AnalyticsManager.ANALYTICS_ACTION_ADDED_REPORT_CONTACT);
                        }
                        getUserContacts();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        showRetryDialog();
                    }
                }, RemoteDataManager.DEFAULT_TIMEOUT);
                queue.add(saveRequest);
            }

        }catch (JSONException je){
            if (RemoteDataManager.SHOW_LOG) Log.e("EditContact", "Unexpected JSON Data");
            showRetryDialog();
        }catch (Exception ee){
            if (RemoteDataManager.SHOW_LOG) Log.e("EditContact", "Unable to connect to server");
            showRetryDialog();
        }

    }

    private void getUserContacts () {
        RemoteDataManager.getContacts(this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject o) {
                showSuccessDialog();
                if (RemoteDataManager.SHOW_LOG) Log.d("EnterLoginInfoActivity", "Received contacts");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showRetryDialog();
                if (RemoteDataManager.SHOW_LOG) Log.d("EnterLoginInfoActivity", volleyError.toString());
            }
        });

    }
    private void showRetryDialog()
    {
        progressDialog.dismiss();
        AlertDialog.Builder errorBuilder = new AlertDialog.Builder(EditContactActivity.this);
        errorBuilder.setMessage(getResources().getString(R.string.message_contact_save_failure));
        errorBuilder.setCancelable(true);
        errorBuilder.setPositiveButton(getResources().getString(R.string.YES),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (last_operation!=OP_DELETE) {
                            saveContact();
                        }else{
                            deleteContact();
                        }
                        dialog.cancel();
                    }
                });
        errorBuilder.setNegativeButton(getResources().getString(R.string.NO),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog errorDialog = errorBuilder.create();
        errorDialog.show();
    }
    private void showSuccessDialog()
    {
        progressDialog.dismiss();
        AlertDialog.Builder successBuider = new AlertDialog.Builder(EditContactActivity.this);
        if (last_operation == OP_ADD) {
            successBuider.setMessage(getResources().getString(R.string.message_contact_add_successful));
        }else if (last_operation == OP_SAVE){
            successBuider.setMessage(getResources().getString(R.string.message_contact_save_successful));
        }else{
            successBuider.setMessage(getResources().getString(R.string.message_contact_delete_successful));
        }
        successBuider.setCancelable(true);
        successBuider.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        setResult(RESULT_OK, null);
                        finish();
                    }
                });

        AlertDialog successDialog = successBuider.create();
        successDialog.show();
    }
    private void showGenericDialog(String message)
    {
        AlertDialog.Builder genericBuilder = new AlertDialog.Builder(EditContactActivity.this);
        genericBuilder.setMessage(message);
        genericBuilder.setCancelable(true);
        genericBuilder.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog successDialog = genericBuilder.create();
        successDialog.show();
    }
    private static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    private static boolean isValidPhone(String PhoneNumber){
        boolean isValid = false;
        int MAX_DIGITS_NUM_1_US_PHONE = 11;
        int MAX_DIGITS_NUM_US_PHONE = 10;
        int MAX_DIGITS_NON_US_PHONE = 11;

        String plainPhone = PhoneNumberUtils.stripSeparators(PhoneNumber);


        if (!Locale.getDefault().getCountry().toLowerCase().equals("jp")) {
            //Non Japanese locale
            if (plainPhone.startsWith("1")) {
                if (plainPhone.length() == MAX_DIGITS_NUM_1_US_PHONE) {
                    isValid = true;
                }
            } else {
                if (plainPhone.length() == MAX_DIGITS_NUM_US_PHONE) {
                    isValid = true;
                }

            }
        }else{
            //Japanese Locale
            if (plainPhone.length() == MAX_DIGITS_NUM_US_PHONE) {
                isValid = true;
            }
            if (plainPhone.length() == MAX_DIGITS_NON_US_PHONE) {
                isValid = true;
            }
        }

        return isValid;
    }

    public void showPopup(View anchorView)
    {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.activity_help_view, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);

        popupWindow.setHeight(200);
        popupWindow.setWidth(200);
        popupWindow.showAsDropDown(anchorView);
    }

}
