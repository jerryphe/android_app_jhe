package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;


/**
 * shows a message when the org is in recess.
 */
public class RecessDialog extends DialogFragment {

    private static String AlertText = "ALERT_TEXT";

    public static RecessDialog newInstance(String currentTime) {
        // Create a new Fragment instance with the specified
        // parameters.
        RecessDialog fragment = new RecessDialog();
        Bundle args = new Bundle();
        args.putString(AlertText, currentTime);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create the new Dialog using the AlertBuilder.
        AlertDialog.Builder alertBuilder =
                new AlertDialog.Builder(getActivity());

        // Configure the Dialog UI.
        alertBuilder.setMessage(getArguments().getString(AlertText));

        // Return the configured Dialog.
        return alertBuilder.create();
    }
}
