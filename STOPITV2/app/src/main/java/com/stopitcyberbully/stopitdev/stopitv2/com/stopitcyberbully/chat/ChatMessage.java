package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat;



import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Object model to store the inbound/ outbound message information.
 */
public class ChatMessage implements Comparable<ChatMessage>{
    private String message;
    private String from;
    private String to;
    private Date messageDate;
    private boolean incoming;
    private long messageID;
    private String uniqueID;

    public long getMessageID() {
        return messageID;
    }

    public void setMessageID(long messageID) {
        this.messageID = messageID;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public boolean isIncoming() {
        return incoming;
    }

    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }
    @Override
    public int hashCode() {
        return this.message.hashCode() + this.getUniqueID().hashCode();
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == null)
        {
            return false;
        }

        if (this.getClass() != other.getClass())
        {
            return false;
        }else{
            ChatMessage otherMessage = (ChatMessage ) other;

            if (this.getUniqueID() == null) return false;
            if (otherMessage.getUniqueID() == null) return false;
            if (
                    (this.getUniqueID().equals(otherMessage.getUniqueID()))
                &&
                            (this.message.equals(otherMessage.message))
                    )
            {
                return true;
            }
        }
        return false;
    }
    @Override
    public int compareTo(@NonNull ChatMessage message) {
        return getMessageDate().compareTo(message.getMessageDate());
    }
}
