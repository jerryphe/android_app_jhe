package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Contact;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.ArrayList;

/**
 * Custom adapter to handle report contact list
 */
public class ReportContactAdapter extends ArrayAdapter<Contact> {

    private final ArrayList<Contact> web;
    private final Activity context1;

public ReportContactAdapter(Activity context,ArrayList<Contact>labels){
        super(context, R.layout.tablecell);
        context1 = context;
        this.web=labels;

        }
@Override
public View getView(final int position,View convertView,ViewGroup parent){

        ViewHolder viewHolder=new ViewHolder();

        if(convertView==null)
        {
        convertView= LayoutInflater.from(this.getContext()).inflate(R.layout.reportcontacts,parent,false);

        viewHolder.contactCheckBox=(CheckBox)convertView.findViewById(R.id.report_contact_name);
        convertView.setTag(viewHolder);
        }else{
        viewHolder=(ViewHolder)convertView.getTag();
        }

        viewHolder.contactCheckBox.setText(web.get(position).getName());

        //check uncheck the checkbox state
        Contact contact = web.get(position);
        viewHolder.contactCheckBox.setTextSize(18.0f);

        if (contact.getOrg_id() > 0) {
            viewHolder.contactCheckBox.setTextColor(ContextCompat.getColor(context1, R.color.pro_heading_blue));

        }else{
            viewHolder.contactCheckBox.setChecked(web.get(position).isSelected());
            if (RemoteDataManager.getSavedOrgType(context1) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
                viewHolder.contactCheckBox.setTextColor(ContextCompat.getColor(context1, R.color.pro_app_text_maroon));
            }else {
                viewHolder.contactCheckBox.setTextColor(ContextCompat.getColor(context1, R.color.pro_app_text_maroon));
            }
        }
    viewHolder.contactCheckBox.setChecked(contact.isSelected());

    CustomFontHelper.setCustomFont(viewHolder.contactCheckBox, "source-sans-pro.semibold.ttf", context1);


        viewHolder.contactCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckBox checkbox = (CheckBox) v;


                Contact contact = web.get(position);
                if ((contact.getOrg_id() > 0) && (RemoteDataManager.getRegistration().getOrg().getIsIncidentToReportManagerRequired())) {
                    checkbox.setChecked(true);
                    contact.setIsSelected(true);
                    AlertDialog.Builder messengerAlert = new AlertDialog.Builder(context1);
                    messengerAlert.setMessage(context1.getResources().getString(R.string.deselect_org_contact_message));
                    messengerAlert.setCancelable(false);
                    messengerAlert.setPositiveButton(context1.getResources().getString(R.string.OK),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert = messengerAlert.create();
                    alert.show();
                } else {
                    contact.setIsSelected(!contact.isSelected());
                }
            }
        });

        return convertView;
        }

@Override
public Contact getItem(int position){
        return this.web.get(position);
        }

@Override
public int getCount(){
        return this.web.size();
        }



private static class ViewHolder {
    private CheckBox contactCheckBox;
}
}
