package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.InboxMessage;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;


public class InboxMessageReaderActivity extends AppBaseActivity {
    private TextView unreadCount;
    private TextView inbox_message_body;
    private TextView inbox_message_subject;
    private TextView inbox_message_date;
    private InboxMessage inboxMessage;
    private int position;
    private ProgressBar inboxRefreshing;
    private Button attachmentButton;
    private Button playAttachmentButton;
    private ImageView thumbImageView;
    private VideoView thumbVideoView;

    final int NO_ATTACHMENT = -1;
    final int IMAGE_ATTACHMENT = 1;
    final int VIDEO_ATTACHMENT = 2;
    final int URL_ATTACHMENT = 3;


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("LAST_POSITION", position);
        outState.putParcelable("LAST_MESSAGE", inboxMessage);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_message_reader);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        thumbImageView = (ImageView) findViewById(R.id.remote_image);
        thumbVideoView = (VideoView) findViewById(R.id.remote_media);

        playAttachmentButton = (Button) findViewById(R.id.launch_attachment);
        playAttachmentButton.setVisibility(View.INVISIBLE);
        attachmentButton = (Button) findViewById(R.id.button_attachment);


        Button inboxMessageDelete = (Button) findViewById(R.id.button_delete);

        inboxRefreshing = (ProgressBar) findViewById(R.id.inbox_progress_bar);

        CrownMolding reportButton = (CrownMolding) findViewById(R.id.btn_report);
        CrownMolding helpButton = (CrownMolding) findViewById(R.id.btn_help);

        Drawable drawableTop = getResources().getDrawable(R.drawable.delete);


        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
            reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
            helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.GREEN_MOLDING);
            if (inboxMessageDelete != null)
            {
                drawableTop = getResources().getDrawable(R.drawable.pro_delete);
                inboxMessageDelete.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);
            }
            if (attachmentButton != null)
            {
                attachmentButton.setBackgroundResource(R.color.pro_heading_blue);
            }
        }else{
            reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.REPORT_MOLDING);
            helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);
            if (inboxMessageDelete != null)
            {
                inboxMessageDelete.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);
            }
            if (attachmentButton != null)
            {
                attachmentButton.setBackgroundResource(R.color.heading_blue);
            }
        }

        if (savedInstanceState != null)
        {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_READ_NOTIFICATION, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_READ_NOTIFICATION);
        }

        TextView reportView = (TextView) findViewById(R.id.txt_report);
        CustomFontHelper.setCustomFont(reportView, "source-sans-pro.semibold.ttf", this);

        TextView helpView = (TextView) findViewById(R.id.txt_help);
        CustomFontHelper.setCustomFont(helpView, "source-sans-pro.semibold.ttf", this);
        refreshUnreadCount();

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                refreshRegistrationPriorToReport();

            }
        });
        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoHelpScreen();
            }
        });

        if (inboxMessageDelete != null)
        {
            inboxMessageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_SELECTED_TRASH_CAN_ON_NOTIFICATION, AnalyticsManager.ANALYTICS_ACTION_USER_SELECTED_TRASH_CAN_ON_NOTIFICATION);

                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(InboxMessageReaderActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.inbox_delete_confirm));
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.inbox_delete_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (RemoteDataManager.SHOW_LOG)
                                        Log.d("inbox", "Deleting " + inboxMessage);

                                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_DELETED_NOTIFICATION_USING_TRASH_CAN, AnalyticsManager.ANALYTICS_ACTION_USER_DELETED_NOTIFICATION_USING_TRASH_CAN);
                                    deleteMessage();
                                    dialog.cancel();
                                }
                            });
                    anonymousAlert.setNegativeButton(getResources().getString(R.string.inbox_delete_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_CLOSED_NOTIFICATION_TRASH_CAN, AnalyticsManager.ANALYTICS_ACTION_USER_CLOSED_NOTIFICATION_TRASH_CAN);
                            dialog.cancel();
                        }
                    });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }

        inbox_message_subject = (TextView) findViewById(R.id.inbox_message_subject);
        CustomFontHelper.setCustomFont(inbox_message_subject, "source-sans-pro.semibold.ttf", this);

        inbox_message_body = (TextView) findViewById(R.id.inbox_message_body);
        CustomFontHelper.setCustomFont(inbox_message_body, "SourceCodePro.Regular.ttf", this);

        Button buttonBack = (Button) findViewById(R.id.button_back_to_list);
        if (buttonBack != null)
        {
            buttonBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
         Button buttonPrev = (Button) findViewById(R.id.button_prev);
        if (buttonPrev != null)
        {
            buttonPrev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goPrevious();
                }
            });
        }
        Button buttonPrevDummy = (Button) findViewById(R.id.button_dummy_prev);
        buttonPrevDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goPrevious();
            }
        });

        Button buttonNext = (Button) findViewById(R.id.button_next);
        if (buttonNext != null)
        {
            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goNext();
                }
            });
        }
        Button buttonNextDummy = (Button) findViewById(R.id.button_dummy_next);
        buttonNextDummy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNext();
            }
        });

        if (savedInstanceState == null) {
            if (getIntent().hasExtra("INBOX_MESSAGE")) {
                String inboxString = getIntent().getStringExtra("INBOX_MESSAGE");
                try {
                    JSONObject inboxBodyJson = new JSONObject(inboxString);
                    inbox_message_subject.setText(inboxBodyJson.getString("subject"));
                    inbox_message_body.setText(inboxBodyJson.getString("body"));
                } catch (JSONException je) {
                    if (RemoteDataManager.SHOW_LOG) Log.e("inbox", je.getMessage());
                }
            }
            if (getIntent().hasExtra("INBOX_ITEM")) {
                inboxMessage = getIntent().getParcelableExtra("INBOX_ITEM");

                inbox_message_date = (TextView) findViewById(R.id.inbox_message_date);
                if (inbox_message_date != null) {
                    inbox_message_date.setText(DateFormat.getDateFormat(this).format(inboxMessage.getCreatedDate()));
                }
            }
            if (getIntent().hasExtra("INBOX_POSITION")) {
                position = getIntent().getIntExtra("INBOX_POSITION", 0);
            }
        }else {
             position = savedInstanceState.getInt("LAST_POSITION", 0);
            inboxMessage = savedInstanceState.getParcelable("LAST_MESSAGE");
            showMessage();
        }


        if (getIntent().hasExtra("INBOX_ATTACHMENT")) {
            int mime = getIntent().getIntExtra("INBOX_ATTACHMENT", NO_ATTACHMENT);
            if (mime == NO_ATTACHMENT)
            {
                attachmentButton.setVisibility(View.INVISIBLE);
            }else{
                attachmentButton.setVisibility(View.VISIBLE);
                showThumbNail();
            }
        }
            attachmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAttachment();
            }
        });

    }
    private void goPrevious(){
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_NAVIGATED_TO_PREVIOUS_NOTIFICATION, AnalyticsManager.ANALYTICS_ACTION_USER_NAVIGATED_TO_PREVIOUS_NOTIFICATION);
        position--;
        if (position < 0) {
            position = RemoteDataManager.getInboxMessageArrayList().size() - 1;
        }
        inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);
        showMessage();
    }

    private void goNext(){
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_NAVIGATED_TO_NEXT_NOTIFICATION, AnalyticsManager.ANALYTICS_ACTION_USER_NAVIGATED_TO_NEXT_NOTIFICATION);
        position++;
        if (position >= RemoteDataManager.getInboxMessageArrayList().size()) {
            position = 0;
        }
        inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);
        showMessage();
    }
private void showAttachment()
{
    RemoteDataManager.getInboxMessageAttchment(InboxMessageReaderActivity.this,
            RemoteDataManager.getIntellicode(InboxMessageReaderActivity.this),
            inboxMessage.getMessageID(),
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Intent remoteContent = new Intent(InboxMessageReaderActivity.this, RemoteMedia.class);

                        String mime = response.getString("mime_type");

                        String url = response.getString("url");
                        if (!url.startsWith("http"))
                        {
                            url = "http://" + url;
                        }

                        if (mime.startsWith("video")) {
                            remoteContent.putExtra("STOPIT_VIDEO_URL", url);
                        } else if (mime.startsWith("image")) {
                            remoteContent.putExtra("STOPIT_IMAGE_URL", url);
                        } else if (mime.startsWith("url")) {
                            remoteContent.putExtra("STOPIT_WEB_URL", url);
                        }
                        InboxMessageReaderActivity.this.startActivity(remoteContent);
                    } catch (JSONException je) {
                        if (RemoteDataManager.SHOW_LOG)
                            Log.d("InboxReader", "Error viewing inbox attachment");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
}
    private void showThumbNail()
    {
        thumbVideoView.setVisibility(View.INVISIBLE);
        thumbImageView.setVisibility(View.INVISIBLE);
        attachmentButton.setVisibility(View.INVISIBLE);
        playAttachmentButton.setVisibility(View.INVISIBLE);
        RemoteDataManager.getInboxMessageAttchment(InboxMessageReaderActivity.this,
                RemoteDataManager.getIntellicode(InboxMessageReaderActivity.this),
                inboxMessage.getMessageID(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String mime = response.getString("mime_type");
                            String url = response.getString("url");
                            if (mime.startsWith("video")) {

                                if (playAttachmentButton != null) {
                                    playAttachmentButton.setBackgroundResource(R.drawable.play_video);
                                    playAttachmentButton.setVisibility(View.VISIBLE);
                                    playAttachmentButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showAttachment();
                                        }
                                    });
                                }
                                thumbVideoView.setVisibility(View.VISIBLE);
                                thumbVideoView.setVideoPath(url);
                                thumbVideoView.requestFocus(View.FOCUS_FORWARD);
                                thumbVideoView.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View v, MotionEvent event) {
                                        showAttachment();
                                        return false;
                                    }
                                });


                                thumbVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mp.seekTo(2000);
                                        mp.start();
                                        mp.pause();
                                    }
                                });
                            } else if (mime.startsWith("image")) {
                                if (playAttachmentButton != null) {
                                    playAttachmentButton.setBackgroundResource(R.drawable.play_image);
                                    playAttachmentButton.setVisibility(View.VISIBLE);
                                    playAttachmentButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showAttachment();
                                        }
                                    });
                                }
                                Picasso.with(InboxMessageReaderActivity.this).load(url).placeholder(R.drawable.video_placeholder).centerInside().fit().into(thumbImageView);
                                thumbImageView.setVisibility(View.VISIBLE);
                                thumbImageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showAttachment();
                                    }
                                });
                            } else if (mime.startsWith("url")) {

                                if (
                                        (url.contains("youtube."))
                                                ||
                                                (url.contains("youtu.be"))
                                        ) {

                                    Uri uri = Uri.parse(url);
                                    String videoID = uri.getQueryParameter("v");
                                    if (videoID != null) {
                                        String youTubeThumnail = "https://i1.ytimg.com/vi/" + videoID + "/2.jpg";
                                        Picasso.with(InboxMessageReaderActivity.this).load(youTubeThumnail).placeholder(R.drawable.video_placeholder).centerInside().fit().into(thumbImageView);
                                        thumbImageView.setVisibility(View.VISIBLE);
                                        thumbImageView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showAttachment();
                                            }
                                        });

                                    }

                                    if (playAttachmentButton != null) {
                                        playAttachmentButton.setBackgroundResource(R.drawable.play_video);
                                        playAttachmentButton.setVisibility(View.VISIBLE);
                                        playAttachmentButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showAttachment();
                                            }
                                        });
                                    }
                                } else {
                                    attachmentButton.setVisibility(View.VISIBLE);
                                    attachmentButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showAttachment();
                                        }
                                    });
                                }
                            }

                        } catch (JSONException je) {
                            thumbVideoView.setVisibility(View.INVISIBLE);
                            thumbImageView.setVisibility(View.INVISIBLE);
                            attachmentButton.setVisibility(View.INVISIBLE);
                            playAttachmentButton.setVisibility(View.INVISIBLE);

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }
    private void refreshUnreadCount()
    {
        if (unreadCount == null)
        {
            unreadCount = (TextView) findViewById(R.id.unread_count);
        }
        if (unreadCount != null) {
            if (RemoteDataManager.getUnreadMessageCount(this) > 0) {
                unreadCount.setText(" " + RemoteDataManager.getUnreadMessageCount(this) + " ");
                unreadCount.setVisibility(View.VISIBLE);
            } else {
                unreadCount.setText("");
                unreadCount.setVisibility(View.INVISIBLE);
            }
        }
    }
    private void gotoReportScreen(){
        if (RemoteDataManager.isConnected(this)){
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_REPORT_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_REPORT_SCREEN_USING_MENU);
            Intent reportIntent = new Intent(InboxMessageReaderActivity.this, ReportActivity.class);
            InboxMessageReaderActivity.this.startActivity(reportIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoReportScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void gotoHelpScreen(){
        if (RemoteDataManager.isConnected(this)){
//            if (RemoteDataManager.getRegistration().getOrg().getOrgTypeID() != RemoteDataManager.ORG_TYPE_HIGHERED) {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_GET_HELP_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_GET_HELP_SCREEN_USING_MENU);
            Intent helpIntent = new Intent(InboxMessageReaderActivity.this, GetHelpActivity.class);
            InboxMessageReaderActivity.this.startActivity(helpIntent);
//            } else {
//                Intent helpHigherEdIntent = new Intent(HomeActivity.this, GetHigherEdHelpActivity.class);
//                HomeActivity.this.startActivity(helpHigherEdIntent);
//
//            }
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoHelpScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void refreshRegistrationPriorToReport()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    if (json.getClass().equals(JSONObject.class)) {
                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    progressDialog.dismiss();
                                    if (!RemoteDataManager.getRegistration().getOrg().getIsAlive()) {
                                        //Org is not alive, we need to show blocked screen
                                        Intent blockedIntent = new Intent(InboxMessageReaderActivity.this, BlockedGateActivity.class);
                                        blockedIntent.putExtra("ORG_NOT_LIVE", true);
                                        InboxMessageReaderActivity.this.startActivity(blockedIntent);
                                    } else if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                        String tag = "recess_dialog";
                                        DialogFragment recessFragment =
                                                RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                        recessFragment.show(getSupportFragmentManager(), tag);
                                    } else {

                                        gotoReportScreen();
                                    }
                                }
                            } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(InboxMessageReaderActivity.this, BlockedGateActivity.class);
                                InboxMessageReaderActivity.this.startActivity(blockedIntent);
                            }
                        } catch (JSONException je) {
                            progressDialog.dismiss();
                            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(InboxMessageReaderActivity.this);
                            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                            anonymousAlert.setCancelable(false);
                            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            progressDialog.dismiss();
                                            refreshRegistrationPriorToReport();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = anonymousAlert.create();
                            alert.show();

                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(InboxMessageReaderActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    refreshRegistrationPriorToReport();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }
    private void deleteMessage()
    {
        RemoteDataManager.UpdateInboxMessageStatus(this, RemoteDataManager.getIntellicode(this), inboxMessage.getMessageID(), "delete", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
    }
    private void showMessage()
    {

        if (thumbVideoView != null) {
            thumbVideoView.setVisibility(View.INVISIBLE);
        }
        if (thumbImageView != null) {
            thumbImageView.setVisibility(View.INVISIBLE);
        }
        if (attachmentButton != null) {
            attachmentButton.setVisibility(View.INVISIBLE);
        }else {
            attachmentButton = (Button) findViewById(R.id.button_attachment);
        }
        if (playAttachmentButton != null) {
            playAttachmentButton.setVisibility(View.INVISIBLE);
        }

        inboxRefreshing.setVisibility(View.VISIBLE);
        inbox_message_date = (TextView) findViewById(R.id.inbox_message_date);
        if (inbox_message_date != null)
        {
            inbox_message_date.setText(DateFormat.getDateFormat(this).format(inboxMessage.getCreatedDate()));
        }
        RemoteDataManager.getInboxMessage(this, RemoteDataManager.getIntellicode(this), inboxMessage.getMessageID(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (RemoteDataManager.SHOW_LOG) Log.d("Inbox", response.toString());
                try {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_NOTIFICATION_CONTENT_SCREEN, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_NOTIFICATION_CONTENT_SCREEN);
                    inbox_message_subject.setText(response.getString("subject"));
                    inbox_message_body.setText(response.getString("body"));
                    int mime = response.getInt("mime_type");
                    if (mime == NO_ATTACHMENT)
                    {
                        attachmentButton.setVisibility(View.INVISIBLE);
                    }else{
                        attachmentButton.setVisibility(View.VISIBLE);
                        showThumbNail();
                    }
                }catch (JSONException je)
                {
                    inbox_message_body.setText(getResources().getString(R.string.inbox_general_error));
                }
                if (inboxRefreshing != null) {
                    inboxRefreshing.setVisibility(View.INVISIBLE);
                }
                updateInboxCounts(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (RemoteDataManager.SHOW_LOG) Log.e("Inbox", error.getMessage());
                if (inboxRefreshing != null) {
                    inboxRefreshing.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

}
