package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

/**
 * This enumeration records various states used by the inbox messages
 */
public enum MessageStatus {
    INBOX_MESSAGE_STATUS_UNUSED,
    INBOX_MESSAGE_STATUS_NEW,
    INBOX_MESSAGE_STATUS_OLDREAD,
    INBOX_MESSAGE_STATUS_RESEND,
    INBOX_MESSAGE_STATUS_RESEND_UNREAD,
    INBOX_MESSAGE_STATUS_DELETE,
    INBOX_MESSAGE_STATUS_READ_UNUSED,
    INBOX_MESSAGE_STATUS_READ;

    public static MessageStatus fromIntValue(int status)
    {
        switch (status)
        {
            case 0:
                return INBOX_MESSAGE_STATUS_NEW;
            case 1:
                return INBOX_MESSAGE_STATUS_NEW;
            case 2:
            case 7:
                return INBOX_MESSAGE_STATUS_READ;
            case 3:
                return INBOX_MESSAGE_STATUS_NEW;
            case 4:
                return INBOX_MESSAGE_STATUS_NEW;
            case 5:
                return INBOX_MESSAGE_STATUS_DELETE;
            default:
                return INBOX_MESSAGE_STATUS_NEW;
        }

    }
}
