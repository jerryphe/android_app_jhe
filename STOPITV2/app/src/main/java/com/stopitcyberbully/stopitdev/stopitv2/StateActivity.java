package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.CodeNameImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Organization;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.State;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class StateActivity extends Activity {
    private CodeNameImageListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state);


//Set some constants
        Bitmap sourcePinBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.map_pin_grey);


// Crop bitmap
        Bitmap pinBitmap = Bitmap.createBitmap(sourcePinBitmap, 0, 0, sourcePinBitmap.getWidth() / 2, sourcePinBitmap.getHeight(), null, false);



        ImageView pinImage = (ImageView) findViewById(R.id.image_map_pin);
        pinImage.setImageBitmap(pinBitmap);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        pinImage.setAnimation(alphaAnimation);


// Set some constants
        Bitmap sourceEclipseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ellipse);


// Crop bitmap
        Bitmap eclipseBitmap = Bitmap.createBitmap(sourceEclipseBitmap, 0, 0, sourceEclipseBitmap.getWidth() / 2, sourceEclipseBitmap.getHeight(), null, false);



        ImageView eclipseImage = (ImageView) findViewById(R.id.image_eclipse);
        eclipseImage.setImageBitmap(eclipseBitmap);
        eclipseImage.setAnimation(alphaAnimation);


        TextView textCountry = (TextView) findViewById(R.id.describe_heading_view);
        CustomFontHelper.setCustomFont(textCountry, "source-sans-pro.semibold.ttf", this);

        final ListView listview = (ListView) findViewById(R.id.state_selection_listview);

        EditText stateSearch = (EditText) findViewById(R.id.search_state);
        CustomFontHelper.setCustomFont(stateSearch, "source-sans-pro.semibold.ttf", this);
        stateSearch.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.search_edit_text) + "</small>"));


        stateSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                   StateActivity.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        Integer[] imageId = {R.drawable.map_pin_white};

        adapter = new CodeNameImageListAdapter(this,RemoteDataManager.getCountry().getStates(),imageId);


        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String stateName = adapter.getItem(position).getName();

                for (State state : RemoteDataManager.getCountry().getStates()) {
                    if (state.getName().equals(stateName)) {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SELECTED_STATE, stateName);

                        findOrganizations(state.getCode(), RemoteDataManager.getCURRENT_ORG_TYPE());
                    }
                }
            }
        });

    }

    private void findOrganizations (String stateCode, int org_type) {
        RequestQueue queue = RemoteDataManager.sharedManager(this);

        final JSONObject jsoninputObject = new JSONObject();

        try {
            jsoninputObject.put("country_id",RemoteDataManager.getCountry().getCode());
            jsoninputObject.put("state_id", stateCode);
            jsoninputObject.put("org_name", "");
            jsoninputObject.put("org_type_id", org_type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AuthRequest orgRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getOrgSearchURL(), jsoninputObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        if (jsonObject.has("data"))
                        {
                            ArrayList<Organization> orgArray = new ArrayList<>();
                            try {
                                JSONObject data = jsonObject.getJSONObject("data");
                                JSONArray orgs = data.getJSONArray("orgs");
                                for (int i = 0; i < orgs.length(); i++) {
                                    Organization org = new Organization();
                                    JSONObject orgJson = (JSONObject) orgs.get(i);
                                    org.setOrgName(orgJson.getString("org_name"));
                                    org.setOrgTypeID(jsoninputObject.getInt("org_type_id"));
                                    org.setOrgID(orgJson.getInt("org_id"));
                                    org.setRosterLabel(orgJson.getString("roster_label"));
                                    org.setRosterType(orgJson.getString("roster_type"));
                                    orgArray.add(org);
                                }
                                RemoteDataManager.setOrgList(orgArray);
                                Intent orgIntent = new Intent(StateActivity.this, FindOrganizationActivity.class);
                                StateActivity.this.startActivity(orgIntent);
                            }catch (JSONException e)
                            {
                                Toast.makeText(StateActivity.this, getResources().getString(R.string.error_in_retrieval), Toast.LENGTH_SHORT).show();

                            }
                        }else{
                            String errorMessage;
                            switch (RemoteDataManager.getCURRENT_ORG_TYPE())
                            {
                                case RemoteDataManager.ORG_TYPE_K12:
                                    errorMessage = getResources().getString(R.string.not_found_K12);
                                    break;
                                case RemoteDataManager.ORG_TYPE_HIGHERED:
                                    errorMessage = getResources().getString(R.string.not_found_COLLEGE);
                                    break;
                                case RemoteDataManager.ORG_TYPE_WORKPLACE:
                                    errorMessage = getResources().getString(R.string.not_found_WORK);
                                    break;
                                default:
                                    errorMessage = getResources().getString(R.string.not_found_K12);
                            }

                            Toast.makeText(StateActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(StateActivity.this, getResources().getString(R.string.error_in_retrieval), Toast.LENGTH_SHORT).show();
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(orgRequest);
    }


}
