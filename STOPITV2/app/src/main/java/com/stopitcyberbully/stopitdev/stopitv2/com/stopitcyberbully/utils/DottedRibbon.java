package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

/**
 * Custom view to add dotted circles in help screen
 */
public class DottedRibbon extends View {


    //paint for drawing custom view
    private Paint circlePaint;

    private boolean proMode;


    private ArrayList<Paint> colorArray;
    private Paint colorPaint1, colorPaint2, colorPaint3, colorPaint4, colorPaint5, colorPaint6, colorPaint7;


    public void setProMode(boolean proMode) {
        this.proMode = proMode;
    }

    public DottedRibbon(Context context, AttributeSet attrs) {
        super(context, attrs);
        //paint object for drawing in onDraw
        circlePaint = new Paint();
        colorArray = new ArrayList<>();
        colorPaint1 = new Paint();
        colorPaint2 = new Paint();
        colorPaint3 = new Paint();
        colorPaint4 = new Paint();
        colorPaint5 = new Paint();
        colorPaint6 = new Paint();
        colorPaint7 = new Paint();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        circlePaint.setStyle(Style.FILL);
        circlePaint.setAntiAlias(true);
        circlePaint.setARGB(255, 114, 39, 199);


        colorArray.clear();

        if (proMode)
        {
            colorPaint1.setARGB(15 * 255 / 100, 0, 74, 135);
            colorArray.add(colorPaint1);

            colorPaint2.setARGB(25 * 255 / 100, 0, 74, 135);
            colorArray.add(colorPaint2);

            colorPaint3.setARGB(35 * 255 / 100, 0, 74, 135);
            colorArray.add(colorPaint3);

            colorPaint4.setARGB(50 * 255 / 100, 0, 74, 135);
            colorArray.add(colorPaint4);

            colorPaint5.setARGB(75 * 255 / 100, 0, 74, 135);
            colorArray.add(colorPaint5);

            colorPaint6.setARGB(255, 0, 74, 135);
            colorArray.add(colorPaint6);

            colorPaint7.setARGB(255, 0, 74, 135);
            colorArray.add(colorPaint7);

            colorArray.add(colorPaint6);
            colorArray.add(colorPaint5);
            colorArray.add(colorPaint4);
            colorArray.add(colorPaint3);
            colorArray.add(colorPaint2);
            colorArray.add(colorPaint1);
        }else {
            colorPaint1.setARGB(15 * 255 / 100, 84, 166, 235);
            colorArray.add(colorPaint1);

            colorPaint2.setARGB(25 * 255 / 100, 84, 166, 235);
            colorArray.add(colorPaint2);

            colorPaint3.setARGB(35 * 255 / 100, 84, 166, 235);
            colorArray.add(colorPaint3);

            colorPaint4.setARGB(50 * 255 / 100, 84, 166, 235);
            colorArray.add(colorPaint4);

            colorPaint5.setARGB(75 * 255 / 100, 84, 166, 235);
            colorArray.add(colorPaint5);

            colorPaint6.setARGB(255, 93, 127, 224);
            colorArray.add(colorPaint6);

            colorPaint7.setARGB(255, 114, 39, 199);
            colorArray.add(colorPaint7);

            colorArray.add(colorPaint6);
            colorArray.add(colorPaint5);
            colorArray.add(colorPaint4);
            colorArray.add(colorPaint3);
            colorArray.add(colorPaint2);
            colorArray.add(colorPaint1);
        }


        //get half of the width and height as we are working with a circle
        int viewWidthHalf = 5;
        int viewHeightHalf = 5;

//get the radius as half of the width or height, whichever is smaller
//subtract ten so that it has some space around it

        int control_width = this.getMeasuredWidth();
        int dots_width = colorArray.size() * 2* viewWidthHalf + 2 *colorArray.size();

        int start_x = control_width/2 - dots_width/2 + (viewWidthHalf);

        for (int i = 0; i < colorArray.size(); i++) {
            Paint colorValue = colorArray.get(i);
            canvas.drawCircle(viewWidthHalf + 2*i*viewHeightHalf + 2 + start_x, viewHeightHalf, viewWidthHalf, colorValue);
        }

    }
}
