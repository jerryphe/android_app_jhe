package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

/**
 * Custom interface to handle lookups either by code or org name.
 */
public interface CodeNameObject {
    String getCode();

    void setCode(String code);

    String getName();

    void setName(String name);
}
