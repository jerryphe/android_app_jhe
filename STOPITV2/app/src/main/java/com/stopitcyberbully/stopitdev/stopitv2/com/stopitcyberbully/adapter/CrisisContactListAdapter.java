package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.CrisisCenter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.List;

/**
 * Custom adapter to show crisis contact details
 */
public class CrisisContactListAdapter extends ArrayAdapter<CrisisCenter> {
    private Context adapterContext;
    private static class ViewHolder {
        private CrownMolding callImageButton;
        private TextView contactName;
        private CrownMolding textImageButton;
    }
    public CrisisContactListAdapter(Context context, int resource,  List<CrisisCenter> contactList) {
        super(context, resource);
        this.contactList = contactList;
        adapterContext = context;
    }

    private List<CrisisCenter> contactList;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();

        if (convertView==null)
        {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.helpcontacts, parent, false);

            viewHolder.contactName = (TextView) convertView.findViewById(R.id.help_contact_name);
            viewHolder.callImageButton = (CrownMolding) convertView.findViewById(R.id.call_contact_button);
            viewHolder.textImageButton = (CrownMolding) convertView.findViewById(R.id.text_contact_button);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final CrisisCenter crisisCenter = getItem(position);

        viewHolder.contactName.setText(crisisCenter.getCrisisCenterName().toCharArray(), 0, crisisCenter.getCrisisCenterName().length());

        CustomFontHelper.setCustomFont(viewHolder.contactName, "source-sans-pro.semibold.ttf", adapterContext);

        if (RemoteDataManager.getSavedOrgType(this.getContext())==RemoteDataManager.ORG_TYPE_WORKPLACE) {
            viewHolder.contactName.setTextColor(ContextCompat.getColor(adapterContext, R.color.pro_crisis_center_name));
        }else{
            viewHolder.contactName.setTextColor(ContextCompat.getColor(adapterContext, R.color.crisis_contact_text_color));
        }


        if (crisisCenter.getCrisisCenterPhone()==null) {
            viewHolder.callImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.DISABLED_CRISIS_CENTER);
        }else{
            if (RemoteDataManager.getSavedOrgType(this.getContext())==RemoteDataManager.ORG_TYPE_WORKPLACE) {
                viewHolder.callImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.GREEN_MOLDING);
            }else{
                viewHolder.callImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.CRISIS_CENTER_MOLDING);
            }
        }
        if (crisisCenter.getCrisisCenterMobilePhone()==null)
        {
            viewHolder.textImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.DISABLED_CRISIS_CENTER);
        }else {
            if (RemoteDataManager.getSavedOrgType(this.getContext())==RemoteDataManager.ORG_TYPE_WORKPLACE) {
                viewHolder.textImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.GREEN_MOLDING);
            }else {
                viewHolder.textImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.CRISIS_CENTER_MOLDING);
            }
        }

        viewHolder.textImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (crisisCenter.getCrisisCenterMobilePhone()==null) return;
                try {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_CHATTED_WITH_SOMEONE, AnalyticsManager.ANALYTICS_ACTION_USER_CHATTED_WITH_SOMEONE);

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("sms:" + crisisCenter.getCrisisCenterMobilePhone()));
                    getContext().startActivity(intent);
                }catch (ActivityNotFoundException anf)
                {
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.not_available_sms), Toast.LENGTH_SHORT).show();

                }
            }
        });
        viewHolder.callImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_CALLED_WITH_SOMEONE, AnalyticsManager.ANALYTICS_ACTION_USER_CALLED_WITH_SOMEONE);

                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + crisisCenter.getCrisisCenterPhone()));
                    getContext().startActivity(intent);
                } catch (ActivityNotFoundException anf) {
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.not_available_tel), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        if (this.contactList == null)
        {
            return 0;
        }
        return this.contactList.size();
    }

    @Override
    public CrisisCenter getItem(int position) {
        return this.contactList.get(position);
    }
}
