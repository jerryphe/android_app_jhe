package com.stopitcyberbully.stopitdev.stopitv2;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.webkit.WebView;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class HelpViewActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_view);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }




        String URL = this.getIntent().getStringExtra("HELP_URL");

        if (URL == null) URL = "file:///android_asset/helpcontacts.html";

        WebView webView = (WebView) findViewById(R.id.help_webview);
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept-Language", Locale.getDefault().getLanguage());
        webView.clearCache(true);
        webView.loadUrl(URL, headers);
    }


}
