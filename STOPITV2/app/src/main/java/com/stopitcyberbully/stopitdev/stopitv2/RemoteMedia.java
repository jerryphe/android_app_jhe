package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;

public class RemoteMedia extends AppBaseActivity {

    VideoView videoView;
    ImageView imageView;
    WebView   webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_media);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        videoView = (VideoView) findViewById(R.id.remote_media);
        imageView = (ImageView) findViewById(R.id.remote_image);
        webView = (WebView) findViewById(R.id.remote_webview);

        if (savedInstanceState != null)
        {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_ATTACHMENT_OR_MEDIA_ON_NOTIFICATION, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_ATTACHMENT_OR_MEDIA_ON_NOTIFICATION);
        }

        Intent webIntent = this.getIntent();
        if(webIntent.hasExtra("STOPIT_VIDEO_URL")) {
            String url = webIntent.getStringExtra("STOPIT_VIDEO_URL");
            if (url != null) {
                Uri videoURI = Uri.parse(url);
                videoView.setVisibility(View.VISIBLE);
                MediaController mc = new MediaController(this);
                mc.setMediaPlayer(videoView);
                videoView.setMediaController(mc);
                videoView.setVideoURI(videoURI);
                videoView.requestFocus();
                videoView.start();
            }
        }else if (webIntent.hasExtra("STOPIT_IMAGE_URL")) {
            String url = webIntent.getStringExtra("STOPIT_IMAGE_URL");
            Picasso.with(this).load(url).placeholder(R.drawable.video_placeholder).centerInside().fit().into(imageView);
            imageView.setVisibility(View.VISIBLE);

        }else if (webIntent.hasExtra("STOPIT_WEB_URL")) {
            String url = webIntent.getStringExtra("STOPIT_WEB_URL");
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setVisibility(View.VISIBLE);
            webView.setWebViewClient(new MyWebViewClient());
            if (url != null)
            {
                webView.loadUrl(url);
            }else {
                webView.loadUrl("http://www.stopitcyberbully.com");
            }
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
