package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

/**
 * Model object to store contact information for user.
 */
public class Contact {
    private String name;
    private String email;
    private int user_id;
    private int org_id;
    private String mobile_phone;
    private boolean isGetHelpContact;
    private boolean isTrustedContact;
    private boolean isOneTouchContact;
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public boolean isGetHelpContact() {
        return isGetHelpContact;
    }

    public void setGetHelpContact(boolean pIsGetHelpContact) {
        this.isGetHelpContact = pIsGetHelpContact;
    }

    public boolean isTrustedContact() {
        return isTrustedContact;
    }

    public void setTrustedContact(boolean pIsTrustedContact) {
        this.isTrustedContact = pIsTrustedContact;
    }

    public boolean isOneTouchContact() {
        return isOneTouchContact;
    }

    public void setOneTouchContact(boolean pIsOneTouchContact) {
        this.isOneTouchContact = pIsOneTouchContact;
    }
}
