package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.CodeNameImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Organization;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;



public class FindOrganizationActivity extends AppCompatActivity {
    private CodeNameImageListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_organization);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }


        TextView headingText = (TextView)findViewById(R.id.describe_heading_view);
        CustomFontHelper.setCustomFont(headingText, "source-sans-pro.semibold.ttf", this);

        ImageView orgType = (ImageView) findViewById(R.id.image_org_type);


// Set some constants
        Bitmap orgBitMap  = null;




        switch (RemoteDataManager.getCURRENT_ORG_TYPE())
        {
            case RemoteDataManager.ORG_TYPE_K12:
                orgBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.grey_school);

                headingText.setText(getResources().getText(R.string.heading_k12_selection));
                break;
            case RemoteDataManager.ORG_TYPE_HIGHERED:
                headingText.setText(getResources().getText(R.string.heading_highered_selection));
                orgBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.grey_college);
                break;
            case RemoteDataManager.ORG_TYPE_WORKPLACE:
                headingText.setText(getResources().getText(R.string.heading_org_selection));
                orgBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.grey_workspace);
                break;
        }

        if (orgBitMap != null) {
// Crop bitmap
            Bitmap orgCroppedBitMap = Bitmap.createBitmap(orgBitMap, 0, 0, orgBitMap.getWidth() / 2, orgBitMap.getHeight(), null, false);

            orgType.setImageBitmap(orgCroppedBitMap);
        }


// Set some constants
        Bitmap sourceEclipseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ellipse);


// Crop bitmap
        Bitmap eclipseBitmap = Bitmap.createBitmap(sourceEclipseBitmap, 0, 0, sourceEclipseBitmap.getWidth() / 2, sourceEclipseBitmap.getHeight(), null, false);

        final AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        orgType.setAnimation(alphaAnimation);

        ImageView eclipseImage = (ImageView) findViewById(R.id.image_eclipse);
        eclipseImage.setImageBitmap(eclipseBitmap);
        eclipseImage.setAnimation(alphaAnimation);

        final ListView listview = (ListView) findViewById(R.id.org_selection_listview);




        Integer[] imageId = {R.drawable.ic_stat_name};
        adapter = new CodeNameImageListAdapter(this, RemoteDataManager.getOrgList(), imageId);

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String orgName = adapter.getItem(position).getName();

                for (Organization orgLoop : RemoteDataManager.getOrgList())
                    if (orgLoop.getOrgName().equals(orgName)) {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SELECTED_ORG, orgName);
                        RemoteDataManager.setSelectedOrganization(orgLoop);
                        Intent enterInfoIntent = new Intent(FindOrganizationActivity.this, EnterLoginInfoActivity.class);
                        FindOrganizationActivity.this.startActivity(enterInfoIntent);
                        break;
                    }

            }
        });



        EditText orgSearch = (EditText) findViewById(R.id.search_org);
        CustomFontHelper.setCustomFont(orgSearch, "source-sans-pro.semibold.ttf", this);
        orgSearch.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.search_edit_text) + "</small>"));



        orgSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                FindOrganizationActivity.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
