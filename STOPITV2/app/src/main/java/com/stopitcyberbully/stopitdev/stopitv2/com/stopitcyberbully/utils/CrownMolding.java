package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.ImageButton;

import java.util.ArrayList;

/**
 * Custom view to show different circles around the imageview.
 */
public class CrownMolding extends ImageButton{
    private  CROWN_MOLDING_MODE mode;


    private ArrayList<Point> pointArrayList;
    private Point p1, p2, p3, p4, p5, p6, p7, centerPoint;
    private ArrayList<Paint> colorArray;

    private Paint colorPaint1, colorPaint2, colorPaint3, colorPaint4, colorPaint5, colorPaint6, colorPaint7, mainColor, strokeColor;

    public void setMode(CROWN_MOLDING_MODE mode) {
        this.mode = mode;
    }

    public CrownMolding(Context context, CROWN_MOLDING_MODE mode) {
        super(context);
        this.mode = mode;
        pointArrayList = new ArrayList<>();

        p1 = new Point();
        p2 = new Point();
        p3 = new Point();
        p4 = new Point();
        p5 = new Point();
        p6 = new Point();
        p7 = new Point();
        centerPoint = new Point();

        pointArrayList.add(p1);
        pointArrayList.add(p2);
        pointArrayList.add(p3);
        pointArrayList.add(p4);
        pointArrayList.add(p5);
        pointArrayList.add(p6);
        pointArrayList.add(p7);
        pointArrayList.add(centerPoint);
        pointArrayList.add(centerPoint); // for stroke


        colorArray = new ArrayList<>();

        colorPaint1 = new Paint();
        colorPaint2 = new Paint();
        colorPaint3 = new Paint();
        colorPaint4 = new Paint();
        colorPaint5 = new Paint();
        colorPaint6 = new Paint();
        colorPaint7 = new Paint();

        mainColor = new Paint();
        strokeColor = new Paint();

    }

    public CrownMolding(Context context, AttributeSet attrs) {
        super(context, attrs);
        pointArrayList = new ArrayList<>();

        p1 = new Point();
        p2 = new Point();
        p3 = new Point();
        p4 = new Point();
        p5 = new Point();
        p6 = new Point();
        p7 = new Point();
        centerPoint = new Point();

        pointArrayList.add(p1);
        pointArrayList.add(p2);
        pointArrayList.add(p3);
        pointArrayList.add(p4);
        pointArrayList.add(p5);
        pointArrayList.add(p6);
        pointArrayList.add(p7);
        pointArrayList.add(centerPoint);
        pointArrayList.add(centerPoint); // for stroke


        colorArray = new ArrayList<>();

        colorPaint1 = new Paint();
        colorPaint2 = new Paint();
        colorPaint3 = new Paint();
        colorPaint4 = new Paint();
        colorPaint5 = new Paint();
        colorPaint6 = new Paint();
        colorPaint7 = new Paint();

        mainColor = new Paint();
        strokeColor = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (this.mode == null)
        {
            this.mode = CROWN_MOLDING_MODE.REPORT_MOLDING;
        }
        //get half of the width and height as we are working with a circle
        int half = Math.min (this.getMeasuredWidth()/2, this.getMeasuredHeight()/2);

        float radius = (float) 0.9 * half;

        float offset = half - radius;


        //Add all points
        p1.set(Math.round(2 * offset), 0);

        p2.set(0, Math.round(2 * offset));

        p3.set(Math.round(2 * offset), Math.round(2 * offset));

        p4.set(Math.round(offset), Math.round(offset));

        p5.set(Math.round(offset), 0);

        p6.set(0, Math.round(offset));

        p7.set(0, 0);



        centerPoint.set(half - Math.round(radius), (this.getMeasuredHeight() / 2) - Math.round(radius));



        colorArray.clear();
        //Now based on mode, add colors

        switch (this.mode)
        {
            case REPORT_MOLDING:
            {
                colorPaint1.setARGB(13 * 255 / 100, 137, 43, 107);
                colorArray.add(colorPaint1);


                colorPaint2.setARGB(13 * 255 / 100, 177, 36, 75);
                colorArray.add(colorPaint2);



                colorPaint3.setARGB(13 * 255 / 100, 152, 40, 96);
                colorArray.add(colorPaint3);


                colorPaint4.setARGB(13 * 255 / 100, 124, 44, 117);
                colorArray.add(colorPaint4);


                colorPaint5.setARGB(13 * 255 / 100, 133, 215, 228);
                colorArray.add(colorPaint5);

                colorPaint6.setARGB(13 * 255 / 100, 247, 147, 30);
                colorArray.add(colorPaint6);


                colorPaint7.setARGB(13 * 255 / 100, 36, 150, 58);
                colorArray.add(colorPaint7);


                mainColor.setARGB(255, 168, 13, 86);
                mainColor.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.argb(255, 158, 16, 91),   Color.argb(255, 100, 31, 119), Shader.TileMode.MIRROR));
                colorArray.add(mainColor);



                strokeColor.setARGB(255, 168, 168, 168);
                strokeColor.setStyle(Paint.Style.STROKE);
                strokeColor.setStrokeWidth(5.0f);
                colorArray.add(strokeColor);

            }
            break;
            case HELP_MOLDING:
            {
                colorPaint1.setARGB(13 * 255 / 100, 137, 43, 107);
                colorArray.add(colorPaint1);


                colorPaint2.setARGB(13 * 255 / 100, 177, 36, 75);
                colorArray.add(colorPaint2);



                colorPaint3.setARGB(13 * 255 / 100, 152, 40, 96);
                colorArray.add(colorPaint3);


                colorPaint4.setARGB(13 * 255 / 100, 124, 44, 117);
                colorArray.add(colorPaint4);


                colorPaint5.setARGB(13 * 255 / 100, 133, 215, 228);
                colorArray.add(colorPaint5);

                colorPaint6.setARGB(13 * 255 / 100, 247, 147, 30);
                colorArray.add(colorPaint6);


                colorPaint7.setARGB(13 * 255 / 100, 36, 150, 58);
                colorArray.add(colorPaint7);


                mainColor.setARGB(255, 116, 30, 196);
                mainColor.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.argb(255, 113, 42, 200), Color.argb(255, 84, 176, 238), Shader.TileMode.MIRROR));
                colorArray.add(mainColor);



                strokeColor.setARGB(255, 168, 168, 168);
                strokeColor.setStyle(Paint.Style.STROKE);
                strokeColor.setStrokeWidth(5.0f);
                colorArray.add(strokeColor);

            }
            break;
            case BLUE_MOLDING:
            {
                colorPaint1.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint1);


                colorPaint2.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint2);



                colorPaint3.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint3);


                colorPaint4.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint4);


                colorPaint5.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint5);

                colorPaint6.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint6);


                colorPaint7.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint7);


                mainColor.setARGB(255, 177, 8, 15);
                mainColor.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.argb(255, 177, 8, 15), Color.argb(255, 177, 8, 15), Shader.TileMode.MIRROR));
                colorArray.add(mainColor);



                strokeColor.setARGB(255, 255, 255, 255);
                strokeColor.setStyle(Paint.Style.STROKE);
                strokeColor.setStrokeWidth(5.0f);
                colorArray.add(strokeColor);

            }
            break;
            case GREEN_MOLDING:
            {
                colorPaint1.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint1);


                colorPaint2.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint2);



                colorPaint3.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint3);


                colorPaint4.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint4);


                colorPaint5.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint5);

                colorPaint6.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint6);


                colorPaint7.setARGB(13 * 255 / 100, 0, 74, 135);
                colorArray.add(colorPaint7);



                mainColor.setARGB(255, 8, 79, 109);
                mainColor.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.argb(255, 8, 79, 109), Color.argb(255, 8, 79, 109), Shader.TileMode.MIRROR));
                colorArray.add(mainColor);



                strokeColor.setARGB(255, 255, 255, 255);
                strokeColor.setStyle(Paint.Style.STROKE);
                strokeColor.setStrokeWidth(5.0f);
                colorArray.add(strokeColor);

            }
            break;
            case CRISIS_CENTER_MOLDING:
            {
                colorPaint1.setARGB(13 * 255 / 100, 137, 43, 107);
                colorArray.add(colorPaint1);


                colorPaint2.setARGB(13 * 255 / 100, 177, 36, 75);
                colorArray.add(colorPaint2);



                colorPaint3.setARGB(13 * 255 / 100, 152, 40, 96);
                colorArray.add(colorPaint3);


                colorPaint4.setARGB(13 * 255 / 100, 124, 44, 117);
                colorArray.add(colorPaint4);


                colorPaint5.setARGB(13 * 255 / 100, 133, 215, 228);
                colorArray.add(colorPaint5);

                colorPaint6.setARGB(13 * 255 / 100, 247, 147, 30);
                colorArray.add(colorPaint6);


                colorPaint7.setARGB(13 * 255 / 100, 36, 150, 58);
                colorArray.add(colorPaint7);


                mainColor.setARGB(255, 168, 13, 86);
                mainColor.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.argb(255, 41, 187, 178), Color.argb(255, 36, 147, 200), Shader.TileMode.MIRROR));
                colorArray.add(mainColor);



                strokeColor.setARGB(255, 168, 168, 168);
                strokeColor.setStyle(Paint.Style.STROKE);
                strokeColor.setStrokeWidth(5.0f);
                colorArray.add(strokeColor);

            }
            break;
            case DISABLED_CRISIS_CENTER:
            {
                colorPaint1.setARGB(13 * 255 / 100, 137, 43, 107);
                colorArray.add(colorPaint1);


                colorPaint2.setARGB(13 * 255 / 100, 177, 36, 75);
                colorArray.add(colorPaint2);


                colorPaint3.setARGB(13 * 255 / 100, 152, 40, 96);
                colorArray.add(colorPaint3);


                colorPaint4.setARGB(13 * 255 / 100, 124, 44, 117);
                colorArray.add(colorPaint4);


                colorPaint5.setARGB(13 * 255 / 100, 133, 215, 228);
                colorArray.add(colorPaint5);

                colorPaint6.setARGB(13 * 255 / 100, 247, 147, 30);
                colorArray.add(colorPaint6);


                colorPaint7.setARGB(13 * 255 / 100, 36, 150, 58);
                colorArray.add(colorPaint7);


                mainColor.setARGB(255, 168, 13, 86);
                mainColor.setShader(new LinearGradient(0, 0, 0, getHeight(), Color.argb(255, 185, 185, 185), Color.argb(255, 185, 185, 185), Shader.TileMode.MIRROR));
                colorArray.add(mainColor);



                strokeColor.setARGB(255, 168, 168, 168);
                strokeColor.setStyle(Paint.Style.STROKE);
                strokeColor.setStrokeWidth(5.0f);
                colorArray.add(strokeColor);

            }
            break;
        }




        if (radius <= 0) radius = 20;


        for (int i = 0; i < colorArray.size(); i++) {
            Paint colorValue = colorArray.get(i);
            Point point = pointArrayList.get(i);
            canvas.drawCircle(radius + point.x, radius + point.y, radius, colorValue);
        }

    }

    public enum CROWN_MOLDING_MODE{
        REPORT_MOLDING,
        HELP_MOLDING,
        BLUE_MOLDING,
        GREEN_MOLDING,
        CRISIS_CENTER_MOLDING,
        DISABLED_CRISIS_CENTER
    }

}