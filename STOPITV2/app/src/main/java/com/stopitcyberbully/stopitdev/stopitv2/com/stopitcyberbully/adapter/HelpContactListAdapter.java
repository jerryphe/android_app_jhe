package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Contact;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.List;

/**
 * Custom Adapter to store list of help contacts
 */
public class HelpContactListAdapter extends ArrayAdapter<Contact> {
    private Context adapterContext;
    private static class ViewHolder {
        private CrownMolding callImageButton;
        private TextView contactName;
        private CrownMolding textImageButton;
    }

    public HelpContactListAdapter(Context context, int resource,  List<Contact> contactList) {
        super(context, resource);
        this.contactList = contactList;
        adapterContext = context;
    }

    private List<Contact> contactList;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();

        if (convertView==null)
        {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.helpcontacts, parent, false);

            viewHolder.contactName = (TextView) convertView.findViewById(R.id.help_contact_name);
            viewHolder.callImageButton = (CrownMolding) convertView.findViewById(R.id.call_contact_button);
            viewHolder.textImageButton = (CrownMolding) convertView.findViewById(R.id.text_contact_button);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Contact contact = getItem(position);
        viewHolder.contactName.setText(contact.getName().toCharArray(), 0, contact.getName().length());

        CustomFontHelper.setCustomFont(viewHolder.contactName, "source-sans-pro.semibold.ttf", adapterContext);
        if (RemoteDataManager.getSavedOrgType(this.getContext())==RemoteDataManager.ORG_TYPE_WORKPLACE) {
            viewHolder.contactName.setTextColor(ContextCompat.getColor(adapterContext, R.color.pro_crisis_center_name));
        }else{
            viewHolder.contactName.setTextColor(ContextCompat.getColor(adapterContext, R.color.app_text_maroon));
        }

        if (contact.getMobile_phone()==null) {
            viewHolder.callImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.DISABLED_CRISIS_CENTER);

        }else {
            if (RemoteDataManager.getSavedOrgType(this.getContext()) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
                viewHolder.callImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
            } else {
                viewHolder.callImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);
            }
        }

        if (contact.getMobile_phone()==null)
        {
            viewHolder.textImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.DISABLED_CRISIS_CENTER);
        }else {
            if (RemoteDataManager.getSavedOrgType(this.getContext())==RemoteDataManager.ORG_TYPE_WORKPLACE) {
                viewHolder.textImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
            }else {
                viewHolder.textImageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);
            }
        }

        viewHolder.textImageButton.setTag(position);
        viewHolder.callImageButton.setTag(position);


        viewHolder.textImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_CHATTED_WITH_SOMEONE, AnalyticsManager.ANALYTICS_ACTION_USER_CHATTED_WITH_SOMEONE);

                    View parentRow = (View) v.getParent();
                    ListView listView = (ListView) parentRow.getParent().getParent();
                    final int position = listView.getPositionForView(parentRow);
                    Contact contact = getItem(position);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("sms:" + contact.getMobile_phone()));
                    getContext().startActivity(intent);
                } catch (ActivityNotFoundException anf) {
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.not_available_tel), Toast.LENGTH_SHORT).show();
                }
            }
        });
        viewHolder.callImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_CALLED_WITH_SOMEONE, AnalyticsManager.ANALYTICS_ACTION_USER_CALLED_WITH_SOMEONE);

                    View parentRow = (View) v.getParent();
                    ListView listView = (ListView) parentRow.getParent().getParent();
                    final int position = listView.getPositionForView(parentRow);
                    Contact contact = getItem(position);

                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + contact.getMobile_phone()));
                    getContext().startActivity(intent);
                } catch (ActivityNotFoundException anf) {
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.not_available_tel), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        if (this.contactList == null)
        {
            return 0;
        }
        return this.contactList.size();
    }

    @Override
    public Contact getItem(int position) {
        return this.contactList.get(position);
    }
}
