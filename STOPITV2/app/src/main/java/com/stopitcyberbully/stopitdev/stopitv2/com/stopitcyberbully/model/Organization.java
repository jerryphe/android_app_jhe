package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

/**
 * Object model to store data related to Organization information obtained from webservice (device registration/ roster)
 */
public class Organization implements CodeNameObject{
    private int orgID;
    private String orgName;
    private int orgTypeID;
    private String rosterType;
    private Boolean isManageContactsPermitted;
    private Boolean isIncidentToReportManagerRequired;
    private Boolean isAlive;
    private String logoPath;
    private Boolean isMediaUploadDisabled;

    public Boolean getIsMediaUploadDisabled() {
        return isMediaUploadDisabled;
    }

    public void setIsMediaUploadDisabled(Boolean isMediaUploadDisabled) {
        this.isMediaUploadDisabled = isMediaUploadDisabled;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    private String videoURL;

    public Boolean getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(Boolean isAlive) {
        this.isAlive = isAlive;
    }

    public Boolean getIsIncidentToReportManagerRequired() {
        return isIncidentToReportManagerRequired;
    }

    public void setIsIncidentToReportManagerRequired(Boolean isIncidentToReportManagerRequired) {
        this.isIncidentToReportManagerRequired = isIncidentToReportManagerRequired;
    }

    private Boolean includeMessenger;

    public Boolean getIncludeMessenger() {
        return includeMessenger;
    }

    public void setIncludeMessenger(Boolean includeMessenger) {
        this.includeMessenger = includeMessenger;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public Boolean getIsManageContactsPermitted() {
        return isManageContactsPermitted;
    }

    public void setIsManageContactsPermitted(Boolean isManageContactsPermitted) {
        this.isManageContactsPermitted = isManageContactsPermitted;
    }

    public boolean isInRecess() {
        return is_in_recess;
    }

    public void setInRecess(boolean is_in_recess) {
        this.is_in_recess = is_in_recess;
    }

    private boolean is_in_recess;

    public String getRosterType() {
        if (!rosterType.toLowerCase().equals("email"))
        {
            rosterType = "id";
        }
        return rosterType;
    }

    public void setRosterType(String rosterType) {
        this.rosterType = rosterType;
    }

    public String getRosterLabel() {
        return rosterLabel;
    }

    public void setRosterLabel(String rosterLabel) {
        this.rosterLabel = rosterLabel;
    }

    private String rosterLabel;

    public int getOrgID() {
        return orgID;
    }

    public void setOrgID(int orgID) {
        this.orgID = orgID;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getOrgTypeID() {
        return orgTypeID;
    }

    public void setOrgTypeID(int orgTypeID) {
        this.orgTypeID = orgTypeID;
    }

    public String getCode() {
        return "" + getOrgID();
    }

    public void setCode(String code) {
        //do nothing
    }
    public String getName() {
        return this.orgName;
    }

    public void setName(String name) {
        //do nothing
    }
}
