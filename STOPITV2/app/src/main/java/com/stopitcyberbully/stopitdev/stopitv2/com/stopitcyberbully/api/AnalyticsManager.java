package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api;

import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.stopitcyberbully.stopitdev.stopitv2.StopitApplication;

/**
 * Custom class which stores the labels/ categories for Google Analytics.
 */
public class AnalyticsManager {
    //Categories of the GA
    public static  final  String ANALYTICS_CATEGORY_USER_ACTIVITY = "User Activity Category";
    public static  final  String ANALYTICS_CATEGORY_ALERT_ACTIVITY = "Alert Activity Category";
    public static  final  String ANALYTICS_CATEGORY_APP_ACTIVITY  = "App Activity Category";
    public static  final  String ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY = "App Activity Inbox Category";

    //Action and Labels for GA
    public static  final  String ANALYTICS_ACTION_ALERT_SERVER_ERROR = "User received Server Error Alert";
    public static  final  String ANALYTICS_ACTION_ALERT_NETWORK_ERROR = "User received Network Error Alert";
    public static  final  String ANALYTICS_ACTION_ALERT_FETCH_ERROR = "User received Fetch Error Alert";

    public static  final  String ANALYTICS_ACTION_INITIAL_LAUNCH ="App Initially Launched Action";
    public static  final  String ANALYTICS_ACTION_APPACTIVATED ="App Activated";
    public static  final  String ANALYTICS_ACTION_ACCEPTED_PRIVACYPOLICY ="User Accepted Application Terms of Use";
    public static  final  String ANALYTICS_ACTION_DECLINED_PRIVACYPOLICY ="User Declined Application Terms of Use";
    public static  final  String ANALYTICS_ACTION_OPENED_PRIVACYPOLICY_WEBPAGE ="User Opened Privacy Policy WebPage";
    public static  final  String ANALYTICS_ACTION_BACKBUTTON_AFTER_DECLINE_PRIVACY ="User Clicked Back Button after Declining Privacy Policy";
    public static  final  String ANALYTICS_ACTION_OPENED_LANDING_SCREEN ="User Opened Landing Screen";
    public static  final  String ANALYTICS_ACTION_OPENED_DESCRIBE_YOURSELF_SCREEN ="User Opened Describe Yourself Screen";
    public static  final  String ANALYTICS_ACTION_SELECTED_K12_OPTION ="User Selected K-12th Grade on Describe Yourself";
    public static  final  String ANALYTICS_ACTION_SELECTED_COLLEGE_OPTION ="User Selected College on Describe Yourself";
    public static  final  String ANALYTICS_ACTION_SELECTED_OFFICE_OPTION ="User Selected Office/Working on Describe Yourself";
    public static  final  String ANALYTICS_ACTION_SELECTED_ACCESS_CODE_OPTION ="User Opened Access Code Screen";
    public static  final  String ANALYTICS_ACTION_FINDING_COUNTRY  ="Country Selection";
    public static  final  String ANALYTICS_ACTION_FINDING_STATE  ="State Selection";
    public static  final  String ANALYTICS_ACTION_SELECTED_COUNTRY  ="User Selected Country";
    public static  final  String ANALYTICS_ACTION_SELECTED_STATE  ="User Selected State";
    public static  final  String ANALYTICS_ACTION_FINDING_ORG  ="School/Organization Selection";
    public static  final  String ANALYTICS_ACTION_SELECTED_ORG  ="User Selected School/Organization";
    public static  final  String ANALYTICS_ACTION_SELECTED__SPANISH_LANGUAGE  ="User Selected Spanish Language";
    public static  final  String ANALYTICS_ACTION_SELECTED__ENGLISH_LANGUAGE  ="User Selected English Language";
    public static  final  String ANALYTICS_ACTION_SELECTED__FRECH_LANGUAGE  ="User Selected French Language";
    public static  final  String ANALYTICS_ACTION_OPENED_SET_LANGUAGE_SCREEN ="User Opened Set Language Screen";
    public static  final  String ANALYTICS_ACTION_LANGUAGE_SELECTION  ="Language Selection";
    public static  final  String ANALYTICS_ACTION_OPENED_MANAGE_CONTACTS_SCREEN ="User Opened Manage Contacts Screen";
    public static  final  String ANALYTICS_ACTION_ADDED_NEW_CONTACT ="User Added New Contact";
    public static  final  String ANALYTICS_ACTION_UPDATED_CONTACT ="User Updated Contact";
    public static  final  String ANALYTICS_ACTION_DELETED_CONTACT ="User Deleted Contact";
    public static  final  String ANALYTICS_ACTION_CANCELED_ON_CONTACT ="User Clicked on Cancel Button in Adding/Editing Contact";
    public static  final  String ANALYTICS_ACTION_ADDED_REPORT_CONTACT ="User Added Report Contact";
    public static  final  String ANALYTICS_ACTION_ADDED_1TOUCH_CONTACT ="User Added 1-Touch Contact";
    public static  final  String ANALYTICS_ACTION_ADDED_GET_HELP_CONTACT ="User Added Get Help Contact";
    public static  final  String ANALYTICS_ACTION_OPENED_TERMS_CONDITION_SCREEN ="User Opened Terms & Conditions Screen";
    public static  final  String ANALYTICS_ACTION_OPENED_HELP_SCREEN ="User Opened Help Screen in Settings";
    public static  final  String ANALYTICS_ACTION_OPENED_APPLICATION_TERMS_OF_USE ="User Opened Application Terms of Use in Settings";
    public static  final  String ANALYTICS_ACTION_ENTERED_RIGHT_ACCESS_CODE  ="User Entered Correct AccessCode/Credentials";
    public static  final  String ANALYTICS_ACTION_ENTERED_WRONG_ACCESS_CODE  ="User Entered Wrong AccessCode/Credentials";
    public static  final  String ANALYTICS_ACTION_SETUP_CONTACT_SCREEN ="User Opened Setup Contact Screen";
    public static  final  String  ANALYTICS_ACTION_SKIPPED_ON_SETUP_SCREEN ="User Clicked on Skip button on Setup Contact Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_CHALLENGE_SCREEN ="User Opened Challenge Screen to Enter Credentials";
    public static  final  String  ANALYTICS_ACTION_OPENED_HOME_SCREEN ="User Opened Home Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_REPORT_SCREEN ="User Opened Report Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_HIGHER_ED_HELP_SCREEN ="User Opened Higher Ed Help Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_GET_HELP_SCREEN ="User Opened Get Help Screen";
    public static  final  String  ANALYTICS_ACTION_CANCELLED_REPORTING_INCIDENT ="User Cancelled Reporting an Incident on Report Screen";
    public static  final  String  ANALYTICS_ACTION_CAMERA_CLICKED_FOR_MEDIA ="User Clicked on Camera on Report Screen to attach Media";
    public static  final  String  ANALYTICS_ACTION_CANCELED_ATTACHING_MEDIA ="User Cancelled Attaching Media for Reporting an Incident";
    public static  final  String  ANALYTICS_ACTION_ATTACHED_MEDIA ="User Attached Media for Reporting an Incident";
    public static  final  String  ANALYTICS_ACTION_ADDED_NOTE ="User Sent Note Text on Reporting an Incident";
    public static  final  String  ANALYTICS_ACTION_ADDED_MEDIA ="User Sent Media on Reporting an Incident";
    public static  final  String  ANALYTICS_ACTION_ADDED_MEDIA_ON_1_TOUCH ="User Sent Media on 1 Touch Reporting an Incident";
    public static  final  String  ANALYTICS_ACTION_REPORT_ANONYMOUSLY ="User Sent Report Anonymously";
    public static  final  String  ANALYTICS_ACTION_REPORT_NON_ANONYMOUSLY ="User Sent Report Non-Anonymously";
    public static  final  String  ANALYTICS_ACTION_REPORT_CHANGED_REPORT_ANONYMOUSLY_SWITCH ="User Changed Report Anonymously Switch";
    public static  final  String  ANALYTICS_ACTION_REPORT_SELECTED_OTHER_CONTACT ="User Selected Other Contact to Send Report to";
    public static  final  String  ANALYTICS_ACTION_SUBMITTED_INCIDENT_REPORT ="User Submited an Incident Report";
    public static  final  String  ANALYTICS_ACTION_SUBMITTED_INCIDENT_REPORT_FAILED ="User Failed Submitting an Incident Report";
    public static  final  String  ANALYTICS_ACTION_SUBMITTED_1_TOUCH_INCIDENT_REPORT_FAILED ="User Failed submitting 1 Touch report";
    public static  final  String  ANALYTICS_ACTION_CANCELED_1TOUCH_INCIDENT_REPORT ="User Canceled 1 Touch Incident Report";
    public static  final  String  ANALYTICS_ACTION_SUBMITTED_1TOUCH_INCIDENT_REPORT ="User Submited 1 Touch Incident Report";
    public static  final  String  ANALYTICS_ACTION_USER_OPENED_1TOUCH_INCIDENT_REPORT ="User Opened 1 Touch Incident Report";
    public static  final  String  ANALYTICS_ACTION_OPENED_PANIC_SCREEN ="User Opened Panic Screen";
    public static  final  String  ANALYTICS_ACTION_CANCELED_PANIC_INCIDENT_REPORTING ="User Canceled Panic Incident Reporting";
    public static  final  String  ANALYTICS_ACTION_OPENED_CHAPERONE_SCREEN ="User Opened Chaperone Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_SCHOOL_HELP_SCREEN ="User Opened Connect/School Help Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_CHAT_SCREEN ="User Opened Chat Screen to Chat Anonymously";
    public static  final  String  ANALYTICS_ACTION_STARTED_CHATTING ="User Started Chatting Anonymously";
    public static  final  String  ANALYTICS_ACTION_SHARED_MEDIA_OVER_CHAT ="User Shared Media over Chat";
    public static  final  String  ANALYTICS_ACTION_OPENED_REPORTING_INCIDENT_IN_HELP ="User Opened Reporting Incident in Help Settings";
    public static  final  String  ANALYTICS_ACTION_OPENED_GETTING_HELP_IN_HELP ="User Opened Getting Help in Help Settings";
    public static  final  String  ANALYTICS_ACTION_OPENED_CONTACTS_HELP_IN_HELP ="User Opened Contacts Help in Help Settings";
    public static  final  String  ANALYTICS_ACTION_OPENED_ABOUT_STOPIT_IN_HELP ="User Opened About STOPit in Help Settings";
    public static  final  String  ANALYTICS_ACTION_USER_LOGGED_OFF ="User logged off the app";
    public static  final  String  ANALYTICS_ACTION_USER_CALLED_WITH_SOMEONE ="User Called Someone Using Connect Screen";
    public static  final  String  ANALYTICS_ACTION_USER_CHATTED_WITH_SOMEONE ="User Chatted Someone Using Connect Screen";
    public static  final  String  ANALYTICS_ACTION_ALERT_DELETED_MEDIA_AFTER_ATTACHING  ="User deleted Media after Attaching";
    public static  final  String  ANALYTICS_ACTION_MAP_ADDRESS_LOOKUP_FAILED  ="User Device failed Map Address lookup";
    public static  final  String  ANALYTICS_ACTION_OPENED_1TouchHelp_Report_Contact ="User Opened 1 Touch Help for Report Contact on Add/Edit Contact Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_1TouchHelp_1Touch_Contact ="User Opened 1 Touch Help for 1 Touch Contact on Add/Edit Contact Screen";
    public static  final  String  ANALYTICS_ACTION_OPENED_1TouchHelp_Get_Help_Contact ="User Opened 1 Touch Help for Get Help Contact on Add/Edit Contact Screen";
//pop up
    public static  final  String  ANALYTICS_ACTION_INBOX_CLICKED_OPEN_NOTIFICATIONS_ON_POPUP ="User clicked Open Notifications on the pop up for new Notification";
    public static  final  String  ANALYTICS_ACTION_INBOX_CLICKED_CLOSE_NOTIFICATIONS_ON_POPUP ="User clicked Close on the pop up for new Notification";

//menu
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_CHAT_SCREEN_USING_MENU ="User Opened Messenger Screen using Menu";
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_NOTIFICATIONS_SCREEN_USING_MENU ="User Opened Notifications Screen using Menu";
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_UPDATE_CHECK_SCREEN_USING_MENU ="User Opened Update Check Screen using Menu";
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_SETTINGS_SCREEN_USING_MENU ="User Opened Settings Screen using Menu";
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_REPORT_SCREEN_USING_MENU ="User Opened Report Screen using Menu";
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_GET_HELP_SCREEN_USING_MENU ="User Opened Get Help Screen using Menu";

//notification
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_DELETED_READ_NOTIFICATION_WITH_SWIPE ="User deleted the READ notification by swiping";
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_DELETED_UNREAD_NOTIFICATION_WITH_SWIPE ="User deleted the UNREAD notification by swiping";
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_MARKED_NOTIFICATION_UNREAD_WITH_SWIPE ="User marked the notification as unread by swiping";
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_CLICKED_EDIT_ON_EDITING_NOTIFICATIONS ="User clicked Edit on Notifications";
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_SELECTED_NO_WHEN_ASKED_TO_DELETE ="User selected No when asked are you sure you want to delete...?";
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_SELECTED_YES_WHEN_ASKED_TO_DELETE ="User selected Yes when asked are you sure you want to delete...?";
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_CLICKED_CANCEL_ON_EDITING_NOTIFICATIONS ="User clicked Cancel on Editing Notifications";

//content
    public static  final  String  ANALYTICS_ACTION_INBOX_USER_READ_NOTIFICATION ="User read the notification";
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_NOTIFICATION_CONTENT_SCREEN ="User Opened Notification Content Screen";
    public static  final  String  ANALYTICS_ACTION_USER_SELECTED_TRASH_CAN_ON_NOTIFICATION ="User selected the trash can on an open notification";
    public static  final  String  ANALYTICS_ACTION_USER_DELETED_NOTIFICATION_USING_TRASH_CAN ="User deleted the READ notification by selecting the trash can and then Delete Notification";
    public static  final  String  ANALYTICS_ACTION_USER_CLOSED_NOTIFICATION_TRASH_CAN ="User selected the trash can on an open notification and then selected Close";
    public static  final  String  ANALYTICS_ACTION_USER_NAVIGATED_TO_PREVIOUS_NOTIFICATION ="User navigated to previous notification on notification content screen";
    public static  final  String  ANALYTICS_ACTION_USER_NAVIGATED_TO_NEXT_NOTIFICATION ="User navigated to next notification on notification content screen";


//attachment"
    public static  final  String  ANALYTICS_ACTION_INBOX_OPENED_ATTACHMENT_OR_MEDIA_ON_NOTIFICATION ="User opened an attachment or media";

//update
    public static  final  String  ANALYTICS_ACTION_USER_CLICKED_ON_CHECK_UPDATE ="User clicked on check update";

    public static void sendEvent(Context context, String categoryId, String actionId, String labelId){
// Get tracker.
        Tracker t = StopitApplication.tracker;
// Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                .setLabel(labelId)
                .build());
    }

    public static void sendEvent(String categoryId, String actionId, String labelId){
// Get tracker.
        Tracker t = StopitApplication.tracker;
// Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(categoryId)
                .setAction(actionId)
                .setLabel(labelId)
                .build());
    }

}
