package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.ChatAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.ChatMessage;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.SmackConnection;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.SmackService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import java.util.UUID;


public class ChatActivity extends AppBaseActivity {

    private Button sendButton;
    private ArrayList<ChatMessage> messageList;
    private ChatAdapter messageListAdapter;
    private EditText outgoing;
    private ListView messageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //We need to apply different color scheme if this is a workplace app
        if (RemoteDataManager.getSavedOrgType(this)==RemoteDataManager.ORG_TYPE_WORKPLACE) {
            RelativeLayout chatToolBar = (RelativeLayout) findViewById(R.id.chat_toolbar);
            if (chatToolBar != null) {
                chatToolBar.setBackgroundResource(R.drawable.pro_chat_tab);
            }
        }


        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_CHAT_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_CHAT_SCREEN);

        sendButton = (Button) findViewById(R.id.chat_send);
        sendButton.setEnabled(false);

        messageList = RemoteDataManager.getMessageList();
        messageView = (ListView) findViewById(R.id.chat_list);

        Integer[] imageId = {R.drawable.school_white};

        messageListAdapter = new ChatAdapter(this, messageList, imageId);
        messageView.setAdapter(messageListAdapter);

        outgoing = (EditText) findViewById(R.id.outgoing_chat_message);

        outgoing.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (outgoing.getText().toString().trim().length() > 0) {
                    sendButton.setEnabled(true);
                } else {
                    sendButton.setEnabled(false);
                }
            }
        });

        //connect to chat
        toggleService();

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                    String tag = "recess_dialog";
                    DialogFragment recessFragment =
                            RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                    recessFragment.show(getSupportFragmentManager(), tag);
                    return;
                }

                //Try to start service
                toggleService();

                if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)) {
                    showErrorDialog();
                } else {
                    //Send analytics call
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_STARTED_CHATTING, AnalyticsManager.ANALYTICS_ACTION_STARTED_CHATTING);

                    //Add message to the list
                    ChatMessage outgoingMessage = new ChatMessage();
                    outgoingMessage.setMessage(outgoing.getText().toString());
                    outgoingMessage.setMessageDate(new Date());
                    outgoingMessage.setUniqueID(UUID.randomUUID().toString());
                    outgoingMessage.setIncoming(false);

                    messageList.add(outgoingMessage);
                    messageListAdapter.notifyDataSetChanged();

                    //send the message to the queue
                    Intent intent = new Intent(SmackService.SEND_MESSAGE);
                    intent.setPackage(ChatActivity.this.getPackageName());
                    intent.putExtra(SmackService.BUNDLE_MESSAGE_BODY, outgoingMessage.getMessage());
                    intent.putExtra(SmackService.MESSAGE_UUID, outgoingMessage.getUniqueID());
                    intent.putExtra(SmackService.BUNDLE_TO, RemoteDataManager.getRegistration().getChat().getChatAdminUserName());
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                    }
                    ChatActivity.this.sendBroadcast(intent);

                    //Check if we need to show after hours message
                    refreshRegistration();

                    //remove the text typed by user
                    outgoing.setText("");
                    sendButton.setEnabled(false);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }

    private void toggleService() {
        startChatService();
        if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED))
        {
            showErrorDialog();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //Reset unread message count
            RemoteDataManager.setUnreadMessageCount(context, 0);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(1);

            runOnUiThread(new Runnable() {
                public void run() {
                    messageListAdapter.notifyDataSetChanged();
                }
            });

        }
    };

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(SmackService.NEW_MESSAGE);
        filter.addAction(RemoteDataManager.INBOX_NOTIFICATION_BROADCAST_ID);
        registerReceiver(receiver, filter);

        refreshRegistration();


        //We just try to start chat service
        startChatService();

        //Reset unread message count
        RemoteDataManager.setUnreadMessageCount(this, 0);

        //Get message history
        getHistory();


        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
        super.onPause();
    }

    private void refreshRegistration()
    {
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {
                    try {
                        String code = json.getString("code");
                        if (code.equals(RemoteDataManager.RESPONSE_OK)) {
                            if (json.has("data")) {
                                if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ChatActivity.this);
                                    anonymousAlert.setMessage(getResources().getString(R.string.incident_recess_text));
                                    anonymousAlert.setCancelable(false);
                                    anonymousAlert.setPositiveButton(getResources().getString(R.string.OK),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();
                                                }
                                            });

                                    AlertDialog alert = anonymousAlert.create();
                                    alert.show();
                                }
                                if (RemoteDataManager.SHOW_LOG) Log.d("ChatActivity", json.toString());
                                try {
                                    JSONObject data = json.getJSONObject("data");
                                    if (data.has("chat")){
                                        JSONObject chatObject = data.getJSONObject("chat");
                                        if (chatObject.has("after_hour_messenger_message")){
                                            String afterhours = chatObject.getString("after_hour_messenger_message");
                                            AlertDialog.Builder messengerAlert = new AlertDialog.Builder(ChatActivity.this);
                                            messengerAlert.setMessage(afterhours);
                                            messengerAlert.setCancelable(true);
                                            messengerAlert.setPositiveButton(getResources().getString(R.string.OK),
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();

                                                        }
                                                    });

                                            AlertDialog alert = messengerAlert.create();
                                            alert.show();
                                        }
                                    }
                                }catch (JSONException je)
                                {
                                    if (RemoteDataManager.SHOW_LOG) Log.e("ChatActivity", je.getMessage());
                                }
                            }
                        }else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER))
                        {
                            //User is blocked, we need to show blocked screen
                            Intent blockedIntent = new Intent(ChatActivity.this, BlockedGateActivity.class);
                            ChatActivity.this.startActivity(blockedIntent);
                        }
                    }catch (JSONException je)
                    {
                        if (RemoteDataManager.SHOW_LOG) Log.e("ChatActivity", je.getMessage());
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                }
            });
        }
    }

    private void showErrorDialog(){
        //show alert
        AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ChatActivity.this);
        anonymousAlert.setMessage(getResources().getString(R.string.no_server));
        anonymousAlert.setCancelable(false);
        anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        toggleService();
                        dialog.cancel();
                    }
                });
        anonymousAlert.setNegativeButton(getResources().getString(R.string.CANCEL), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        });

        AlertDialog alert = anonymousAlert.create();
        alert.show();
    }
    private void getHistoryAfterRefresh()
    {
        showProgress(getResources().getString(R.string.message_initializing));

        RemoteDataManager.getChatHistory(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Reset unread message count
                RemoteDataManager.setUnreadMessageCount(StopitApplication.getAppContext(), 0);
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(1);
                messageListAdapter.notifyDataSetChanged();
                if (messageList.size() > 1) {
                    messageView.setSelection(messageList.size() - 1);
                }
                try {
                    progressDialog.dismiss();
                } catch (IllegalArgumentException ie) {
                    if (RemoteDataManager.SHOW_LOG) Log.d("ChatActivity", ie.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progressDialog.dismiss();
                    showHistoryErrorDialog();
                } catch (IllegalArgumentException ie) {
                    if (RemoteDataManager.SHOW_LOG) Log.d("ChatActivity", ie.getMessage());
                }
            }
        });

    }
    private void showHistoryErrorDialog(){
        //show alert
        AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ChatActivity.this);
        anonymousAlert.setMessage(getResources().getString(R.string.no_server));
        anonymousAlert.setCancelable(true);
        anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        getHistory();
                    }
                });
        anonymousAlert.setNegativeButton(getResources().getString(R.string.cancel_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert = anonymousAlert.create();
        alert.show();
    }

    private void getHistory()
    {
        if (RemoteDataManager.getRegistration()!= null)
        {
            //We have registration data, so we can get history directly.
            getHistoryAfterRefresh();
            return;
        }
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    if (json.getClass().equals(JSONObject.class)) {
                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    progressDialog.dismiss();

                                    if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                        String tag = "recess_dialog";
                                        DialogFragment recessFragment =
                                                RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                        recessFragment.show(getSupportFragmentManager(), tag);
                                    } else {

                                        getHistoryAfterRefresh();
                                    }
                                }
                            } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(ChatActivity.this, BlockedGateActivity.class);
                                ChatActivity.this.startActivity(blockedIntent);
                            }
                        } catch (JSONException je) {
                            progressDialog.dismiss();
                            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ChatActivity.this);
                            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                            anonymousAlert.setCancelable(false);
                            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            progressDialog.dismiss();
                                            getHistory();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = anonymousAlert.create();
                            alert.show();

                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ChatActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    getHistory();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }
}
