package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.SmackConnection;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.SmackService;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.json.JSONObject;

import java.util.ArrayList;

public class AppBaseActivity extends AppCompatActivity {



    ProgressDialog progressDialog;
    public IBinder smackBinder;
    private Menu mOptionsMenu;
    /** Flag indicating whether we have called bind on the service. */
    private boolean mBound;

    PopupWindow popupWindow;
    TextView inboxUpdateCount;
    TextView unreadCount;
    TextView inboxUnreadCount;

    private AlertDialog alertInbox;
    private AlertDialog alertLogout;



    /** Messenger for communicating with the service. */
    private SmackService mService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_base);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {

                if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
                    actionBar.setLogo(R.drawable.stopit_pro_nav_logo);
                } else {
                    actionBar.setLogo(R.drawable.stopit_nav_logo);
                }

            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    protected void onCreate(Bundle savedInstanceState, boolean menuon, int contentid) {
        super.onCreate(savedInstanceState);



        if (menuon) {
            setContentView(R.layout.activity_app_base);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setLogo(R.drawable.stopit_nav_logo);
                actionBar.setDisplayUseLogoEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayShowTitleEnabled(true);
            }
        }else{
            setContentView(contentid);
        }
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateManager.unregister();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateInboxCounts(false);
        checkForCrashes();
    }

    private void checkForCrashes() {
        //Production HockeyApp Key
        String APP_ID = "5a6e2c63626188b9f53c6cb2a4141635";

        if (!RemoteDataManager.isPROD){
            //Pre-Prod HockeyApp Key
            APP_ID = "3af511121cd4d2cd3d0cd72e3b241c4e";
            //Staging
//            APP_ID = "5af4a8bd2ccc45808ada3f2aaaf7beff";
            //DR
//            APP_ID = "0a12346ea3524f2cba18c3645a6ccd79";
        }
        CrashManager.register(this, APP_ID);
    }

    public void refreshMenuCount(){
        if (mOptionsMenu != null) {
            MenuItem countMenu = mOptionsMenu.findItem(R.id.action_inbox_text);
            MenuItem inboxMenu = mOptionsMenu.findItem(R.id.action_inbox);
            if ((countMenu != null) && (inboxMenu != null) ) {
                int inbox_count = RemoteDataManager.getUnreadInboxMessageCount(this);
                int chat_count = RemoteDataManager.getUnreadMessageCount(this);
                int update_count = RemoteDataManager.getAppUpdateCount(this);
                int total_count = inbox_count + chat_count + update_count;
                if (total_count > 0) {
                    countMenu.setTitle("" + total_count);
                    countMenu.setVisible(true);
                    inboxMenu.setIcon(R.drawable.inbox_main_red);
                }else {
                    countMenu.setVisible(false);
                    inboxMenu.setIcon(R.drawable.inbox_mail_blue);
                }
            }
        }

        updateToolBarCounts();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mOptionsMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings_manage_contacts) {
            Intent manageContactsIntent = new Intent(this, ManageContactsActivity.class);
            AppBaseActivity.this.startActivity(manageContactsIntent);
            return true;
        }
        else if(id == R.id.action_settings_set_language)
        {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_SET_LANGUAGE_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_SET_LANGUAGE_SCREEN);
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName("com.android.settings", "com.android.settings.LanguageSettings");
            startActivity(intent);
            return true;
        }
        else if(id == R.id.action_settings_help)
        {
            Intent appHelpIntent = new Intent(this, AppHelpActivity.class);
            AppBaseActivity.this.startActivity(appHelpIntent);
            return true;
        }
        else if(id == R.id.action_settings_terms_of_use)
        {
            Intent appTermsIntent = new Intent(this, AppTermsActivity.class);
            AppBaseActivity.this.startActivity(appTermsIntent);
            return true;
        }else if (id == R.id.action_settings_logout) {
            showLogoutAlert();
        }else if (id == android.R.id.home)
        {
            onBackPressed();
        }else if ((id == R.id.action_inbox) || (id == R.id.action_inbox_text) || (id == R.id.action_settings_inbox))
        {
            if ( (popupWindow != null) && (popupWindow.isShowing()) ) {
                popupWindow.dismiss();
            }else{
                final View menuItemView = findViewById(R.id.action_inbox); // SAME ID AS MENU ID

                if (popupWindow == null) {
                    LayoutInflater layoutInflater
                            = (LayoutInflater) getBaseContext()
                            .getSystemService(LAYOUT_INFLATER_SERVICE);
                    View popupView = layoutInflater.inflate(R.layout.inbox, null);
                    popupWindow = new PopupWindow(
                            popupView,
                            SlidingPaneLayout.LayoutParams.MATCH_PARENT,
                            SlidingPaneLayout.LayoutParams.WRAP_CONTENT);
                }

                if (popupWindow != null)
                {
                    popupWindow.setBackgroundDrawable(new BitmapDrawable());
                    popupWindow.setOutsideTouchable(true);
                }

                final ActionBar actionBar = getSupportActionBar();
                Rect rectf = new Rect();
                this.findViewById(android.R.id.content).getLocalVisibleRect(rectf);

                int height = 0;



                if (actionBar != null)
                {
                    height += actionBar.getHeight();
                    height += getStatusBarHeight();
                }

                final Button messagesButton = (Button) popupWindow.getContentView().findViewById(R.id.image_inbox_messages);
                if (messagesButton != null)
                {
                    messagesButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                String tag = "recess_dialog";
                                DialogFragment recessFragment =
                                        RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                recessFragment.show(getSupportFragmentManager(), tag);
                            }else {
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_CHAT_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_CHAT_SCREEN_USING_MENU);
                                Intent chatIntent = new Intent(popupWindow.getContentView().getContext(), ChatActivity.class);
                                popupWindow.getContentView().getContext().startActivity(chatIntent);
                                popupWindow.dismiss();
                            }
                        }
                    });
                }


                final Button notificationsButton = (Button) popupWindow.getContentView().findViewById(R.id.image_inbox_notifications);
                if (notificationsButton != null)
                {
                    notificationsButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_NOTIFICATIONS_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_NOTIFICATIONS_SCREEN_USING_MENU);
                            Intent inboxIntent = new Intent(popupWindow.getContentView().getContext(), InboxActivity.class);
                            popupWindow.getContentView().getContext().startActivity(inboxIntent);
                            popupWindow.dismiss();
                        }
                    });
                }
                inboxUpdateCount = (TextView) popupWindow.getContentView().findViewById(R.id.inbox_app_update_count);
                unreadCount = (TextView) popupWindow.getContentView().findViewById(R.id.unread_count);
                inboxUnreadCount = (TextView) popupWindow.getContentView().findViewById(R.id.inbox_unread_count);

                updateToolBarCounts();
                final Button updateButton = (Button) popupWindow.getContentView().findViewById(R.id.image_inbox_updates);
                if (updateButton != null)
                {
                    updateButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_UPDATE_CHECK_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_UPDATE_CHECK_SCREEN_USING_MENU);
                            Intent inboxIntent = new Intent(popupWindow.getContentView().getContext(), UpdateHandlerActivity.class);
                            popupWindow.getContentView().getContext().startActivity(inboxIntent);
                            popupWindow.dismiss();
                        }
                    });
                }

                final Button settingsButton = (Button) popupWindow.getContentView().findViewById(R.id.image_inbox_settings);
                if (settingsButton != null)
                {
                    settingsButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final View menuItemView = findViewById(R.id.action_inbox); // SAME ID AS MENU ID

                            if (menuItemView == null) return;

                            PopupMenu popup = new PopupMenu(AppBaseActivity.this, menuItemView  );
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_SETTINGS_SCREEN_USING_MENU, AnalyticsManager.ANALYTICS_ACTION_INBOX_OPENED_SETTINGS_SCREEN_USING_MENU);
                                    if (item.getItemId() == R.id.action_settings_manage_contacts) {
                                        Intent manageContactsIntent = new Intent(AppBaseActivity.this, ManageContactsActivity.class);
                                        AppBaseActivity.this.startActivity(manageContactsIntent);
                                        return true;
                                    }
                                    else if(item.getItemId() == R.id.action_settings_set_language)
                                    {
                                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_SET_LANGUAGE_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_SET_LANGUAGE_SCREEN);
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.setClassName("com.android.settings", "com.android.settings.LanguageSettings");
                                        startActivity(intent);
                                        return true;
                                    }
                                    else if(item.getItemId() == R.id.action_settings_help)
                                    {
                                        Intent appHelpIntent = new Intent(AppBaseActivity.this, AppHelpActivity.class);
                                        AppBaseActivity.this.startActivity(appHelpIntent);
                                        return true;
                                    }
                                    else if(item.getItemId() == R.id.action_settings_terms_of_use)
                                    {
                                        Intent appTermsIntent = new Intent(AppBaseActivity.this, AppTermsActivity.class);
                                        AppBaseActivity.this.startActivity(appTermsIntent);
                                        return true;
                                    }
                                    return false;
                                }
                            });
                            //Inflating the Popup using xml file
                            popup.getMenuInflater().inflate(R.menu.menu_popup, popup.getMenu());
                            popup.show();
                        }
                    });
                }

                popupWindow.setAnimationStyle(android.R.style.Animation_InputMethod);
                popupWindow.showAtLocation(this.findViewById(android.R.id.content), Gravity.TOP, 0, height);

            }


        }

        return super.onOptionsItemSelected(item);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void updateToolBarCounts() {

        if (inboxUpdateCount != null) {
            if (RemoteDataManager.getAppUpdateCount(this) > 0) {
                inboxUpdateCount.setText(getResources().getString(R.string.inbox_count,RemoteDataManager.getAppUpdateCount(this)));
                inboxUpdateCount.setVisibility(View.VISIBLE);
            } else {
                inboxUpdateCount.setText("");
                inboxUpdateCount.setVisibility(View.INVISIBLE);
            }
        }



        if (unreadCount != null) {
            if (RemoteDataManager.getUnreadMessageCount(this) > 0) {
                unreadCount.setText(getResources().getString(R.string.inbox_count, RemoteDataManager.getUnreadMessageCount(this)));
                unreadCount.setVisibility(View.VISIBLE);
            } else {
                unreadCount.setText("");
                unreadCount.setVisibility(View.INVISIBLE);
            }
        }
        if (inboxUnreadCount != null) {
            if (RemoteDataManager.getUnreadInboxMessageCount(this) > 0) {
                inboxUnreadCount.setText(getResources().getString(R.string.inbox_count,RemoteDataManager.getUnreadInboxMessageCount(this)));
                inboxUnreadCount.setVisibility(View.VISIBLE);
            } else {
                inboxUnreadCount.setText("");
                inboxUnreadCount.setVisibility(View.INVISIBLE);
            }
        }
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mOptionsMenu = menu;
        super.onPrepareOptionsMenu(menu);
        try {
                refreshMenuCount();
            if (RemoteDataManager.getRegistration() != null) {
                MenuItem contactsMenu = menu.findItem(R.id.action_settings_manage_contacts);
                if (contactsMenu != null) {
                    menu.findItem(R.id.action_settings_manage_contacts).setVisible(RemoteDataManager.getRegistration().getOrg().getIsManageContactsPermitted());
                }
            }
        }catch (Exception e)
        {
            if (RemoteDataManager.SHOW_LOG) Log.e("Menu", e.toString());
        }
        return true;
    }



    protected void logout(){
        RemoteDataManager.logoutFromApp(this, RemoteDataManager.getIntellicode(this), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (RemoteDataManager.SHOW_LOG) Log.d("logout", "logout worked");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (RemoteDataManager.SHOW_LOG) Log.d("logout", "logout failed");
            }
        });

        try {
            String channel = "USER_" + RemoteDataManager.getRegistration().getUser().getIntellicode();
            ParsePush.unsubscribeInBackground(channel);
            String OrgChannel = "ORG_ID_" + RemoteDataManager.getRegistration().getOrg().getOrgID();
            ParsePush.unsubscribeInBackground(OrgChannel);
        }catch (Exception e){
            if (RemoteDataManager.SHOW_LOG) Log.e("LOGOUT", e.getMessage());
            //We have to eat this exception.
        }


        RemoteDataManager.setRegistration(null);
        RemoteDataManager.wipeIntellicode(this);

        //Get a list of current channels already subscribed
        ArrayList currChannels = (ArrayList) ParseInstallation.getCurrentInstallation().getList("channels");

        //if the user has any subscriptions, wipe it off
        if (currChannels != null) {
            //now remove unused channels
            if (!currChannels.isEmpty()) {
                for (Object remchan : currChannels) {
                    if (remchan.getClass().equals(String.class)) {
                        ParsePush.unsubscribeInBackground(remchan.toString(), new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                              //We cannot tell user, however we just have to eat the exception.
                            }
                        });
                    }
                }
            }
        }

        //Remove old chats
        RemoteDataManager.resetMessageList();

        Intent landingScreen=new Intent(this,LandingActivity.class);
        landingScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_LOGGED_OFF, AnalyticsManager.ANALYTICS_ACTION_USER_LOGGED_OFF);


        if (!(SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED))) {

            Intent intent = new Intent(this, SmackService.class);
            this.stopService(intent);
        }

        startActivity(landingScreen);
        this.finish();

    }

    void removeProgressDialog()
    {
        progressDialog.dismiss();
    }
    void showProgress(String message)
    {
//start the progress dialog

        progressDialog = ProgressDialog.show(AppBaseActivity.this, "", message);

        new Thread() {

            public void run() {

                try{

                    sleep(6000*60*60);

                } catch (Exception e) {

                    if (RemoteDataManager.SHOW_LOG) Log.e("tag", e.getMessage());

                }

// dismiss the progress dialog

                progressDialog.dismiss();

            }

        }.start();

    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            SmackService.LocalBinder binder = (SmackService.LocalBinder) service;
            mService = binder.getService();
            mService.restart();
            mBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mBound = false;
        }
    };

    //Start Chat if not started already
    void startChatService() {
        try {
            if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

                prefs.edit()
                        .putString("xmpp_jid", RemoteDataManager.getRegistration().getChat().getChatUserName() + "@"
                                + RemoteDataManager.getRegistration().getChat().getChatServerURL())
                        .putString("xmpp_password", RemoteDataManager.getRegistration().getChat().getChatUserPassword())
                        .apply();

                if (RemoteDataManager.getRegistration().getChat().getChatUserName() != null) {
                    //Check state again
                    if (SmackService.getState().equals(SmackConnection.ConnectionState.DISCONNECTED)) {
                        if (mService == null) {
                            Intent startIntent = new Intent(this, SmackService.class);
                            mBound = bindService(startIntent, mConnection, Context.BIND_AUTO_CREATE);
                        }else{
                            mService.restart();
                        }
                    }


                }
            }
        }catch (Exception e)
            {
                //wild card catch
            }
        }

    public void updateInboxCounts(final boolean showPrompt)
    {
        RemoteDataManager.getInboxCounts(AppBaseActivity.this, RemoteDataManager.getIntellicode(this), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                refreshMenuCount();
                int inbox_count = RemoteDataManager.getUnreadInboxMessageCount(AppBaseActivity.this);
                if (showPrompt) {
                    if (inbox_count > 0) showInboxDialog(inbox_count);
                }
            }
        }, null);

    }
    protected void showLogoutAlert()
    {
        if( (alertLogout != null) && (alertLogout.isShowing()) )
        {
            return;
        }
        AlertDialog.Builder dialogLogout = new AlertDialog.Builder(AppBaseActivity.this);

        dialogLogout.setTitle(R.string.title_logout);
        dialogLogout.setMessage(R.string.prompt_logout);
        dialogLogout.setCancelable(true);
        dialogLogout.setPositiveButton(getResources().getString(R.string.YES),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        logout();
                        dialog.cancel();
                    }
                });
        dialogLogout.setNegativeButton(getResources().getString(R.string.CANCEL),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alertLogout = dialogLogout.create();
        alertLogout.show();
    }

    private void showInboxDialog(int count)
    {
        if( (alertInbox != null) && (alertInbox.isShowing()) )
        {
            return;
        }
        AlertDialog.Builder inboxAlert = new AlertDialog.Builder(AppBaseActivity.this);
        if (count >1) {
            inboxAlert.setMessage(getResources().getString(R.string.settings_unread_notifications, count));
        }
        else
        {
            inboxAlert.setMessage(getResources().getString(R.string.settings_unread_notification, count));
        }
        inboxAlert.setCancelable(true);
        inboxAlert.setPositiveButton(getResources().getString(R.string.open_notification),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        gotoInboxScreen();
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_CLICKED_OPEN_NOTIFICATIONS_ON_POPUP, AnalyticsManager.ANALYTICS_ACTION_INBOX_CLICKED_OPEN_NOTIFICATIONS_ON_POPUP);

                    }
                });
        inboxAlert.setNegativeButton(getResources().getString(R.string.waiting_message_prompt_close),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_CLICKED_CLOSE_NOTIFICATIONS_ON_POPUP, AnalyticsManager.ANALYTICS_ACTION_INBOX_CLICKED_CLOSE_NOTIFICATIONS_ON_POPUP);
                        dialog.cancel();
                    }
                });

        alertInbox = inboxAlert.create();
        alertInbox.show();
    }
    private void gotoInboxScreen(){
        if (RemoteDataManager.isConnected(this)){
            Intent inboxIntent = new Intent(AppBaseActivity.this, InboxActivity.class);
            AppBaseActivity.this.startActivity(inboxIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(true);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoInboxScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }

}
