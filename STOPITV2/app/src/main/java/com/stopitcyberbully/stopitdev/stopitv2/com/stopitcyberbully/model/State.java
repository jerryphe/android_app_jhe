package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

/**
 * Object model to store data related to selected country's states obtained from webservice (/v1/countries )
 */
public class State implements CodeNameObject {
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String code;
    private String name;
}
