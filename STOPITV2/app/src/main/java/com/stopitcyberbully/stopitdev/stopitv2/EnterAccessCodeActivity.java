package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;


public class EnterAccessCodeActivity extends Activity {

    private ProgressDialog progressDialog;
    private TextView remainingText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_access_code);

        final EditText intellicode = (EditText) findViewById(R.id.intellicode_text);
        CustomFontHelper.setCustomFont(intellicode, "source-sans-pro.semibold.ttf", this);

        intellicode.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.hint_access_code) + "</small>"));


        final Button next_button = (Button) findViewById(R.id.btn_access_next);

        TextView textHeading = (TextView) findViewById(R.id.enteraccesscode_heading);
        CustomFontHelper.setCustomFont(textHeading, "source-sans-pro.semibold.ttf", this);

        TextView textSubHeading = (TextView) findViewById(R.id.enteraccesscode_subheading);
        textSubHeading.setText(Html.fromHtml(getString(R.string.subheading_access_code)));
        CustomFontHelper.setCustomFont(textSubHeading, "SourceCodePro.Regular.ttf", this);

        remainingText = (TextView) findViewById(R.id.remaining_characters);
        CustomFontHelper.setCustomFont(remainingText, "source-sans-pro.semibold.ttf", this);


// Set some constants
        Bitmap sourceEclipseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ellipse);


// Crop bitmap
        Bitmap eclipseBitmap = Bitmap.createBitmap(sourceEclipseBitmap, 0, 0, sourceEclipseBitmap.getWidth() / 2, sourceEclipseBitmap.getHeight(), null, false);

        final AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        ImageView userImage = (ImageView) findViewById(R.id.image_user);

        ImageView eclipseImage = (ImageView) findViewById(R.id.image_eclipse);
        eclipseImage.setImageBitmap(eclipseBitmap);
        eclipseImage.setAnimation(alphaAnimation);
        userImage.setAnimation(alphaAnimation);


        //upon launch the next button is disabled.
        next_button.setEnabled(false);

        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerDevice(intellicode.getText().toString());
            }
        });

        intellicode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                remainingText.setTextColor(Color.parseColor("#FFFFFF"));
                if (s.length()>0)
                {
                    int remaining = RemoteDataManager.INTELLICODE_LENGTH - s.length();
                    if (remaining > 0) {
                        remainingText.setText(getResources().getString(R.string.more_remaining_characters, remaining));
                    }else{
                        remainingText.setText(getResources().getString(R.string.no_more_remaining_characters));
                    }
                }else {
                    remainingText.setText(getResources().getString(R.string.remaining_characters));
                }
                if (s.length() == RemoteDataManager.INTELLICODE_LENGTH) {
                    intellicode.setLinkTextColor(Color.parseColor("#741EC4"));
                    next_button.setEnabled(true);
                } else {
                    intellicode.setLinkTextColor(Color.parseColor("#52B0EE"));
                    next_button.setEnabled(false);
                }

            }
        });
    }

    private void registerDevice (String intellicode){
        showProgress(getResources().getString(R.string.message_signing_in));

        RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject json) {
                try {
                    if (json.has("code")) {
                        String code = json.getString("code");
                        switch (code) {
                            case RemoteDataManager.RESPONSE_OK:
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ENTERED_RIGHT_ACCESS_CODE, AnalyticsManager.ANALYTICS_ACTION_ENTERED_RIGHT_ACCESS_CODE);
                                getUserContacts();

                                progressDialog.dismiss();
                                Intent activationSuccessIntent = new Intent(EnterAccessCodeActivity.this, ActivationSuccessActivity.class);
                                activationSuccessIntent.setFlags(activationSuccessIntent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK);
                                EnterAccessCodeActivity.this.startActivity(activationSuccessIntent);
                                break;
                            case RemoteDataManager.RESPONSE_DEACTIVATED_USER:
                                showDisabledErrorDialog();
                                break;
                            default:
                                showGenericLoginError();
                                break;
                        }
                    }
                }catch (JSONException je)
                {
                    showGenericLoginError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showGenericLoginError();
            }
        });
    }


    private void getUserContacts () {
        RemoteDataManager.getContacts(this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject o) {
                if (RemoteDataManager.SHOW_LOG) Log.d("EnterAccessCodeActivity", "Received contacts");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (RemoteDataManager.SHOW_LOG) Log.d("EnterAccessCodeActivity", volleyError.toString());
            }
        });
    }
    private void showProgress(String message)
    {
//start the progress dialog

        progressDialog = ProgressDialog.show(EnterAccessCodeActivity.this, "", message);

        new Thread() {

            public void run() {

                try{

                    sleep(6000*60*60);

                } catch (Exception e) {

                    if (RemoteDataManager.SHOW_LOG) Log.e("tag", e.getMessage());

                }

// dismiss the progress dialog

                progressDialog.dismiss();

            }

        }.start();

    }
    private void showErrorDialog()
    {
        progressDialog.dismiss();

        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        remainingText.setText(getResources().getString(R.string.invalid_access_code));
        remainingText.setTextColor(Color.parseColor("#FFBEDA"));

        AlertDialog.Builder successBuider = new AlertDialog.Builder(EnterAccessCodeActivity.this);
            successBuider.setMessage(getResources().getString(R.string.message_access_code_not_found));

        successBuider.setCancelable(true);
        successBuider.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog successDialog = successBuider.create();
        successDialog.show();
    }

    private void showGenericLoginError(){
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ENTERED_WRONG_ACCESS_CODE, AnalyticsManager.ANALYTICS_ACTION_ENTERED_WRONG_ACCESS_CODE);
        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        showErrorDialog();
    }

    private void showDisabledErrorDialog()
    {
        progressDialog.dismiss();

        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        remainingText.setText(getResources().getString(R.string.access_disabled));
        remainingText.setTextColor(Color.parseColor("#FFBEDA"));

        AlertDialog.Builder successBuider = new AlertDialog.Builder(EnterAccessCodeActivity.this);
        successBuider.setMessage(getResources().getString(R.string.access_disabled));

        successBuider.setCancelable(true);
        successBuider.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog successDialog = successBuider.create();
        successDialog.show();
    }
 }
