package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.RemoteWebView;
import com.stopitcyberbully.stopitdev.stopitv2.StopitApplication;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;

import org.json.JSONObject;

/**
 * This class handles inbox type push message i.e. push which has video or some website links as
 * payload.
 */
public class StopitBroadcastReceiver extends ParsePushBroadcastReceiver {
    public static final String ACTION                       =   "com.example.package.MESSAGE";
    public static final String PARSE_EXTRA_DATA_KEY         =   "com.parse.Data";
    public static final String PARSE_JSON_CHANNEL_KEY       =   "com.parse.Channel";

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        String channel = intent.getExtras().getString(PARSE_JSON_CHANNEL_KEY);
        try {
            JSONObject json = new JSONObject(intent.getExtras().getString(PARSE_EXTRA_DATA_KEY));
            if (json.has("aps"))
            {
                JSONObject aps = json.getJSONObject("aps");
                if (aps.has("alert") && json.has("url")) {
                    String alert = aps.getString("alert");
                    String url = json.getString("url");
                    notifyUser(context, alert, url);
                }
            }
            Intent i = new Intent(RemoteDataManager.INBOX_NOTIFICATION_BROADCAST_ID);
            StopitApplication.getAppContext().sendBroadcast(i);

            if (RemoteDataManager.SHOW_LOG) Log.d("Parse", json.toString());

        }
        catch (org.json.JSONException jse){
            if (RemoteDataManager.SHOW_LOG) Log.e("Parse", jse.toString());
        }
        super.onReceive(context, intent);
    }

    public void notifyUser(Context ctx, String alertText, String url)
    {
        Intent intent = new Intent(ctx, RemoteWebView.class);
        intent.putExtra("STOPIT_URL", url);
        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, intent, 0);

        NotificationCompat.Builder b = new NotificationCompat.Builder(ctx);

        b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_push)
                .setTicker(ctx.getResources().getString(R.string.app_name))
                .setContentTitle(ctx.getResources().getString(R.string.app_name) + " " + ctx.getResources().getString(R.string.button_messenger_text))
                .setContentText(alertText)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setContentIntent(contentIntent)
                .setContentInfo(ctx.getResources().getString(R.string.waiting_message_prompt_read));


        NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, b.build());
    }

}
