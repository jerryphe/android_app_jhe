package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.CodeNameObject;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.ArrayList;

/**
 * custom adapter to handle lookup in either code or in name.
 */
public class CodeNameImageListAdapter extends ArrayAdapter<CodeNameObject> implements Filterable

{

    private  ArrayList<? extends CodeNameObject> adapterObject;
    private final ArrayList<? extends CodeNameObject> initialAdapterObject;

    private final Integer[] imageId;
    private CodeNameLookup codeNameLookup;
    private final Activity context;


    public CodeNameImageListAdapter(Activity ctx, ArrayList<? extends CodeNameObject> labels, Integer[] imageId) {
        super(ctx, R.layout.tablecell);
        this.context = ctx;
        this.initialAdapterObject = labels;
        this.imageId = imageId;
        codeNameLookup = new CodeNameLookup();
        codeNameLookup.setMasterArray(labels);
        this.adapterObject = codeNameLookup.getMasterArray();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();

        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.tablecell, parent, false);

            viewHolder.itemImageView = (ImageView) convertView.findViewById(R.id.cell_icon);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.cell_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textView.setText(adapterObject.get(position).getName());
        CustomFontHelper.setCustomFont(viewHolder.textView, "source-sans-pro.semibold.ttf", context);

        if (imageId.length <= position) {
            viewHolder.itemImageView.setImageResource(imageId[0]);
        } else {
            viewHolder.itemImageView.setImageResource(imageId[position]);
        }
        return convertView;
    }

    @Override
    public CodeNameObject getItem(int position) {
        this.adapterObject = codeNameLookup.getMasterArray();
        return this.adapterObject.get(position);
    }

    @Override
    public int getCount() {
        adapterObject = codeNameLookup.getMasterArray();
        return this.adapterObject.size();
    }

    @Override
    public Filter getFilter() {
        return codeNameLookup;
    }

    private static class ViewHolder {
        private ImageView itemImageView;
        private TextView textView;
    }

    private class CodeNameLookup extends Filter {
        private ArrayList<? extends CodeNameObject> masterArray;

        public ArrayList<? extends CodeNameObject> getMasterArray() {
            return masterArray;
        }

        public void setMasterArray(ArrayList<? extends CodeNameObject> masterArray) {
            this.masterArray = new ArrayList<>(masterArray);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            ArrayList<CodeNameObject> filteredResults = new ArrayList<>();

            if (masterArray.size() == 0){
                setMasterArray(initialAdapterObject);
            }
            //constraint is the result from text you want to filter against.
            //objects is your data set you will filter from
            if (constraint != null && constraint.length() >0) {
                int length = masterArray.size();
                int i = 0;
                while (i < length) {
                    CodeNameObject item;
                    item = masterArray.get(i);
                    //do whatever you wanna do here
                    //adding result set output array
                    String searchString = constraint.toString().toLowerCase();
                    if ((item.getName().toLowerCase().startsWith(searchString)) || (item.getCode().toLowerCase().startsWith(searchString))) {
                        filteredResults.add(item);
                    }

                    i++;
                }

                filterResults.values = filteredResults;
                filterResults.count = filteredResults.size();
            }else {
                filterResults.values = initialAdapterObject;
                filterResults.count = initialAdapterObject.size();
            }
            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {
            masterArray = (ArrayList<? extends CodeNameObject>) results.values;
            if (results.count == 0)
                notifyDataSetInvalidated();
            else
                notifyDataSetChanged();
        }
    }
}
