package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.parse.ParseAnalytics;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class LandingActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;

    private TextView remainingText;
    private Button no_code;
    private ImageButton access_help;
    private PopupWindow popupWindow;
    private TextView get_started;
    private TextView txt_or;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        setContentView(R.layout.activity_landing);

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;

        AnalyticsManager.sendEvent(this, AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_LANDING_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_LANDING_SCREEN);

// Set some constants
        Bitmap logoBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.langing_logo);


// Crop bitmap
        Bitmap newBitmap = Bitmap.createBitmap(logoBitmap, 0, 0, logoBitmap.getWidth()/2, logoBitmap.getHeight(), null, false);

// Assign new bitmap to ImageView
        ImageView backImage = (ImageView) findViewById(R.id.landing_back_image);
        backImage.setImageBitmap(newBitmap);

        ImageButton access_help = (ImageButton) findViewById(R.id.access_help);
        if (access_help != null){
            access_help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHelp();
                }
            });
        }

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);

        backImage.setAnimation(alphaAnimation);


        TextView heading = (TextView) findViewById(R.id.landing_heading_view);

        AlphaAnimation alphaTextAnimation = new AlphaAnimation(0.5f, 1.0f);
        alphaTextAnimation.setDuration(3000);

        if (heading != null) {
            heading.setAnimation(alphaAnimation);
        }




        TranslateAnimation tm = new TranslateAnimation(width, 0, 0, 0);
        tm.setStartOffset(1000);
        tm.setDuration(5000);
        backImage.setAnimation(tm);

        CustomFontHelper.setCustomFont(heading, "source-sans-pro.semibold.ttf", this);

        txt_or = (TextView) findViewById(R.id.text_or);

        get_started = (TextView) findViewById(R.id.get_started);

        if (get_started != null)
        {
            CustomFontHelper.setCustomFont(get_started, "source-sans-pro.semibold.ttf", this);
        }

        //Check if user has accepted the TOS or not
        SharedPreferences preferences = this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        int seen = preferences.getInt(getResources().getString(R.string.VIEWED_TOS), 0);
        if (seen != 1) {
            Intent tosIntent = new Intent(this, TermsofServiceGateActivity.class);
            LandingActivity.this.startActivity(tosIntent);
        }



        no_code = (Button) findViewById(R.id.button_no_code);
        if (no_code != null)
        {
            no_code.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent descIntent = new Intent(LandingActivity.this, DescribeActivity.class);
                    LandingActivity.this.startActivity(descIntent);
                    overridePendingTransition(R.anim.abc_grow_fade_in_from_bottom, R.anim.abc_shrink_fade_out_from_bottom);
                }
            });
        }

        remainingText = (TextView) findViewById(R.id.remaining_characters);
        CustomFontHelper.setCustomFont(remainingText, "source-sans-pro.semibold.ttf", this);

        final EditText intellicode = (EditText) findViewById(R.id.intellicode_text);
        CustomFontHelper.setCustomFont(intellicode, "source-sans-pro.semibold.ttf", this);

        intellicode.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.hint_access_code) + "</small>"));


        final Button next_button = (Button) findViewById(R.id.btn_access_next);
        //upon launch the next button is disabled.
        next_button.setEnabled(false);

        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerVanityDevice(intellicode.getText().toString());
            }
        });

        intellicode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                remainingText.setTextColor(Color.parseColor("#FFFFFF"));
                if (s.length() == 0) {
                    remainingText.setText(getResources().getString(R.string.title_blank));
                }
                if (s.length() >= RemoteDataManager.VANITY_INTELLICODE_LENGTH) {

                    next_button.setEnabled(true);
                    no_code.setEnabled(false);
                } else {
                    next_button.setEnabled(false);
                    no_code.setEnabled(true);
                }

            }
        });

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        String intellicode = RemoteDataManager.getIntellicode(this);
        if (intellicode!= null) {
            if (RemoteDataManager.SHOW_LOG) Log.d("LandingActivity", RemoteDataManager.getIntellicode(this));
            Intent homeIntent = new Intent(LandingActivity.this, HomeActivity.class);
            homeIntent.setFlags(homeIntent.getFlags()|Intent.FLAG_ACTIVITY_NEW_TASK);
            LandingActivity.this.startActivity(homeIntent);

        }
    }
    @Override
    public void onBackPressed() {
        //background app on back button press event
        moveTaskToBack(true);
    }
    private void registerVanityDevice(String intellicode){
        showProgress(getResources().getString(R.string.message_signing_in));

        RemoteDataManager.registerVanityDevice(this, intellicode, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                try {
                    String intellicode = response.getString("intellicode");
                    registerDevice(intellicode);
                } catch (JSONException je) {
                    showGenericLoginError();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showGenericLoginError();
            }
        });
    }
    private void registerDevice (String intellicode){
        showProgress(getResources().getString(R.string.message_signing_in));

        RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject json) {
                try {
                    if (json.has("code")) {
                        String code = json.getString("code");
                        switch (code) {
                            case RemoteDataManager.RESPONSE_OK:
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ENTERED_RIGHT_ACCESS_CODE, AnalyticsManager.ANALYTICS_ACTION_ENTERED_RIGHT_ACCESS_CODE);
                                getUserContacts();

                                progressDialog.dismiss();
                                Intent activationSuccessIntent = new Intent(LandingActivity.this, ActivationSuccessActivity.class);
                                activationSuccessIntent.setFlags(activationSuccessIntent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK);
                                LandingActivity.this.startActivity(activationSuccessIntent);
                                break;
                            case RemoteDataManager.RESPONSE_DEACTIVATED_USER:
                                showDisabledErrorDialog();
                                break;
                            default:
                                showGenericLoginError();
                                break;
                        }
                    }
                } catch (JSONException je) {
                    showGenericLoginError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showGenericLoginError();
            }
        });
    }


    private void getUserContacts () {
        RemoteDataManager.getContacts(this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject o) {
                if (RemoteDataManager.SHOW_LOG)
                    Log.d("EnterAccessCodeActivity", "Received contacts");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (RemoteDataManager.SHOW_LOG)
                    Log.d("EnterAccessCodeActivity", volleyError.toString());
            }
        });
    }
    private void showProgress(String message)
    {
//start the progress dialog

        progressDialog = ProgressDialog.show(LandingActivity.this, "", message);

        new Thread() {

            public void run() {

                try{

                    sleep(6000*60*60);

                } catch (Exception e) {

                    if (RemoteDataManager.SHOW_LOG) Log.e("tag", e.getMessage());

                }

// dismiss the progress dialog

                progressDialog.dismiss();

            }

        }.start();

    }
    private void showErrorDialog()
    {
        progressDialog.dismiss();

        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        remainingText.setText(getResources().getString(R.string.invalid_access_code));
        remainingText.setTextColor(Color.parseColor("#990000"));

//        AlertDialog.Builder successBuider = new AlertDialog.Builder(LandingActivity.this);
//        successBuider.setMessage(getResources().getString(R.string.message_access_code_not_found));
//
//        successBuider.setCancelable(true);
//        successBuider.setPositiveButton(getResources().getString(R.string.OK),
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
//
//        AlertDialog successDialog = successBuider.create();
//        successDialog.show();
    }

    private void showGenericLoginError(){
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ENTERED_WRONG_ACCESS_CODE, AnalyticsManager.ANALYTICS_ACTION_ENTERED_WRONG_ACCESS_CODE);
        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);
        if (no_code != null)
        {
            no_code.setEnabled(true);
        }
        showErrorDialog();
    }

    private void showDisabledErrorDialog()
    {
        progressDialog.dismiss();

        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        remainingText.setText(getResources().getString(R.string.access_disabled));
        remainingText.setTextColor(Color.parseColor("#FFBEDA"));

        AlertDialog.Builder successBuider = new AlertDialog.Builder(LandingActivity.this);
        successBuider.setMessage(getResources().getString(R.string.access_disabled));

        successBuider.setCancelable(true);
        successBuider.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog successDialog = successBuider.create();
        successDialog.show();
    }
    private void showHelp(){

        if ((popupWindow != null) && (popupWindow.isShowing()))
        {
            popupWindow.dismiss();
            return;
        }
            if (popupWindow == null) {
                LayoutInflater layoutInflater
                        = (LayoutInflater) getBaseContext()
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.help_popup, null);
                popupWindow = new PopupWindow(
                        popupView,
                        SlidingPaneLayout.LayoutParams.MATCH_PARENT,
                        SlidingPaneLayout.LayoutParams.MATCH_PARENT);
            }

            if (popupWindow != null)
            {
                WebView webView = (WebView) popupWindow.getContentView().findViewById(R.id.help_popup_webview);

                String URL ="file:///android_asset/"+ getString(R.string.onboarding_help_popup)+".html";
                Map<String, String> headers = new HashMap<>();
                headers.put("Accept-Language", Locale.getDefault().getLanguage());
                webView.clearCache(true);
                webView.loadUrl(URL, headers);

                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                popupWindow.setOutsideTouchable(true);
                popupWindow.setAnimationStyle(android.R.style.Animation_InputMethod);
                if (txt_or == null) {
                    popupWindow.showAsDropDown(get_started);
                }else {
                    popupWindow.showAsDropDown(txt_or);
                }
                ImageButton closeHelp = (ImageButton) popupWindow.getContentView().findViewById(R.id.close_help);
                if (closeHelp != null)
                {
                    closeHelp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });
                }


            }

    }

}

