package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdateHandlerActivity extends AppBaseActivity {
    private TextView unreadCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_handler);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        TextView appVersion = (TextView)findViewById(R.id.app_version);
        TextView appBuild = (TextView)findViewById(R.id.app_build);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            if ( (appVersion != null) && (appBuild != null) ) {
                String version = (pInfo.versionName != null) ? pInfo.versionName : "";
                appVersion.setText(this.getResources().getString(R.string.inbox_version_prefix).concat(""+ version));
                appBuild.setText(this.getResources().getString(R.string.inbox_build_prefix).concat("" +pInfo.versionCode));
            }
        }catch (Exception nnfe)
        {
            if ( (appVersion != null) && (appBuild != null) ) {
                appVersion.setText(this.getResources().getString(R.string.inbox_version_error));
                appBuild.setText(this.getResources().getString(R.string.inbox_version_error));
            }
        }


        CrownMolding reportButton = (CrownMolding) findViewById(R.id.btn_report);
        CrownMolding helpButton = (CrownMolding) findViewById(R.id.btn_help);



        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
            reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
            helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.GREEN_MOLDING);
        }else{
            reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.REPORT_MOLDING);
            helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);
        }

        TextView reportView = (TextView) findViewById(R.id.txt_report);
        CustomFontHelper.setCustomFont(reportView, "source-sans-pro.semibold.ttf", this);

        TextView helpView = (TextView) findViewById(R.id.txt_help);
        CustomFontHelper.setCustomFont(helpView, "source-sans-pro.semibold.ttf", this);
        refreshUnreadCount();

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                refreshRegistrationPriorToReport();

            }
        });
        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoHelpScreen();
            }
        });

        Button updateButton = (Button) findViewById(R.id.button_check_for_update);
        if (updateButton != null)
        {
            CustomFontHelper.setCustomFont(updateButton, "source-sans-pro.semibold.ttf", this );
            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_CLICKED_ON_CHECK_UPDATE, AnalyticsManager.ANALYTICS_ACTION_USER_CLICKED_ON_CHECK_UPDATE);

                    RemoteDataManager.hasAppExpired(UpdateHandlerActivity.this, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response != null) {
                                //update the menu with update count of the app update.
                                updateInboxCounts(false);

                                AlertDialog.Builder updateAlert = new AlertDialog.Builder(UpdateHandlerActivity.this);
                                updateAlert.setMessage(getResources().getString(R.string.update_available_description));
                                updateAlert.setPositiveButton(getResources().getString(R.string.update),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                if (RemoteDataManager.SHOW_LOG)
                                                    Log.d("RemoteDataManager", "Optional Update");
                                                //Launch expiry notification
                                                final String my_package_name = "com.stopitcyberbully.mobile";
                                                String url;

                                                try {
                                                    //Check whether Google Play store is installed or not:
                                                    UpdateHandlerActivity.this.getPackageManager().getPackageInfo("com.android.vending", 0);

                                                    url = "market://details?id=" + my_package_name;
                                                } catch (final Exception e) {
                                                    url = "https://play.google.com/store/apps/details?id=" + my_package_name;
                                                }


                                                //Open the app page in Google Play store:
                                                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                                startActivity(intent);
                                            }
                                        });
                                updateAlert.setNegativeButton(getResources().getString(R.string.not_now), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert = updateAlert.create();
                                alert.show();
                            } else {
                                AlertDialog.Builder noUpdate = new AlertDialog.Builder(UpdateHandlerActivity.this);
                                noUpdate.setMessage(getResources().getString(R.string.inbox_no_update));
                                noUpdate.setPositiveButton(getResources().getString(R.string.OK),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alert = noUpdate.create();
                                alert.show();

                            }
                        }
                    }, new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {

                        }
                    });
                }
            });
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }
    private void refreshUnreadCount()
    {
        if (unreadCount == null)
        {
            unreadCount = (TextView) findViewById(R.id.unread_count);
        }
        if (unreadCount != null) {
            if (RemoteDataManager.getUnreadMessageCount(this) > 0) {
                unreadCount.setText(" " + RemoteDataManager.getUnreadMessageCount(this) + " ");
                unreadCount.setVisibility(View.VISIBLE);
            } else {
                unreadCount.setText("");
                unreadCount.setVisibility(View.INVISIBLE);
            }
        }
    }
    private void gotoReportScreen(){
        if (RemoteDataManager.isConnected(this)){
            Intent reportIntent = new Intent(UpdateHandlerActivity.this, ReportActivity.class);
            UpdateHandlerActivity.this.startActivity(reportIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoReportScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void gotoHelpScreen(){
        if (RemoteDataManager.isConnected(this)){
//            if (RemoteDataManager.getRegistration().getOrg().getOrgTypeID() != RemoteDataManager.ORG_TYPE_HIGHERED) {
            Intent helpIntent = new Intent(UpdateHandlerActivity.this, GetHelpActivity.class);
            UpdateHandlerActivity.this.startActivity(helpIntent);
//            } else {
//                Intent helpHigherEdIntent = new Intent(HomeActivity.this, GetHigherEdHelpActivity.class);
//                HomeActivity.this.startActivity(helpHigherEdIntent);
//
//            }
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoHelpScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void refreshRegistrationPriorToReport()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    if (json.getClass().equals(JSONObject.class)) {
                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    progressDialog.dismiss();
                                    if (!RemoteDataManager.getRegistration().getOrg().getIsAlive()) {
                                        //Org is not alive, we need to show blocked screen
                                        Intent blockedIntent = new Intent(UpdateHandlerActivity.this, BlockedGateActivity.class);
                                        blockedIntent.putExtra("ORG_NOT_LIVE", true);
                                        UpdateHandlerActivity.this.startActivity(blockedIntent);
                                    } else if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                        String tag = "recess_dialog";
                                        DialogFragment recessFragment =
                                                RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                        recessFragment.show(getSupportFragmentManager(), tag);
                                    } else {

                                        gotoReportScreen();
                                    }
                                }
                            } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(UpdateHandlerActivity.this, BlockedGateActivity.class);
                                UpdateHandlerActivity.this.startActivity(blockedIntent);
                            }
                        } catch (JSONException je) {
                            progressDialog.dismiss();
                            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(UpdateHandlerActivity.this);
                            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                            anonymousAlert.setCancelable(false);
                            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            progressDialog.dismiss();
                                            refreshRegistrationPriorToReport();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = anonymousAlert.create();
                            alert.show();

                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(UpdateHandlerActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    refreshRegistrationPriorToReport();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }
}
