package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

/**
 * Custom object to store chat server contact information, this is obtained using register device or roster call.
 */
public class Chat {
    private String chatServerURL;
    private int chatServerPort;
    private String chatAdminUserName;
    private String chatUserName;
    private String chatUserPassword;
    private int history_count;

    public int getHistory_count() {
        return history_count;
    }

    public void setHistory_count(int history_count) {
        this.history_count = history_count;
    }

    public String getChatServerURL() {
        return chatServerURL;
    }

    public void setChatServerURL(String chatServerURL) {
        this.chatServerURL = chatServerURL;
    }

    public int getChatServerPort() {
        return chatServerPort;
    }

    public void setChatServerPort(int chatServerPort) {


        this.chatServerPort = chatServerPort;
    }

    public String getChatAdminUserName() {
//        return "admin1@messenger.stopitcyberbully.com";
        return chatAdminUserName + "@" + chatServerURL;
    }

    public void setChatAdminUserName(String chatAdminUserName) {
        this.chatAdminUserName = chatAdminUserName;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public String getChatUserPassword() {
        return chatUserPassword;
    }

    public void setChatUserPassword(String chatUserPassword) {
        this.chatUserPassword = chatUserPassword;
    }
}
