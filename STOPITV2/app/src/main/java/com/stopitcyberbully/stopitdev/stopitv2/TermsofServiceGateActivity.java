package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.ObservableWebView;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


public class TermsofServiceGateActivity extends AppBaseActivity {

    private ObservableWebView webView;
    private String tos;
    private ProgressBar bar;
    private Button accept;
    private Button decline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termsof_service_gate);
        accept = (Button) findViewById(R.id.accept_tos);
        CustomFontHelper.setCustomFont(accept, "source-sans-pro.semibold.ttf", this);

        decline = (Button) findViewById(R.id.decline_tos);
        CustomFontHelper.setCustomFont(decline, "source-sans-pro.semibold.ttf", this);


        TextView privacy_policy = (TextView) findViewById(R.id.privacy_policy);

        CustomFontHelper.setCustomFont(privacy_policy, "source-sans-pro.semibold.ttf", this);


        privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_PRIVACYPOLICY_WEBPAGE, AnalyticsManager.ANALYTICS_ACTION_OPENED_PRIVACYPOLICY_WEBPAGE);

                Intent privacyIntent = new Intent(TermsofServiceGateActivity.this, HelpViewActivity.class);
                privacyIntent.putExtra("HELP_URL", RemoteDataManager.getPrivacyPolicyUrl());
                TermsofServiceGateActivity.this.startActivity(privacyIntent);
            }
        });

        if (savedInstanceState!= null)
        {
            tos = savedInstanceState.getString("APP_TOS");
        }

        bar = (ProgressBar) findViewById(R.id.tos_load_progressbar);

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show alert
                AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(bar.getContext());
                anonymousAlert.setTitle(getResources().getString(R.string.decline_alert_title));
                anonymousAlert.setMessage(getResources().getString(R.string.decline_alert_text));
                anonymousAlert.setCancelable(false);
                anonymousAlert.setPositiveButton(getResources().getString(R.string.tos_decline),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_DECLINED_PRIVACYPOLICY, AnalyticsManager.ANALYTICS_ACTION_DECLINED_PRIVACYPOLICY);

                                Intent declineActivity = new Intent(decline.getContext(), DeclineResultActivity.class);
                                TermsofServiceGateActivity.this.startActivity(declineActivity);
                                dialog.cancel();
                            }
                        });
                anonymousAlert.setNegativeButton(getResources().getString(R.string.OK),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //show decline activity
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = anonymousAlert.create();
                alert.show();
            }
        });


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = accept.getContext().getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("VIEWED_TOS", 1);
                editor.apply();

                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ACCEPTED_PRIVACYPOLICY, AnalyticsManager.ANALYTICS_ACTION_ACCEPTED_PRIVACYPOLICY);

                finish();
            }
        });

        webView = (ObservableWebView) findViewById(R.id.webview_terms_of_use);
        webView.setOnScrollChangedCallback(new ObservableWebView.OnScrollChangedCallback() {
            @Override
            public void onScroll(int l, int t) {
                int height = (int) Math.floor(webView.getContentHeight() * webView.getScale() *.5);
                if (t + webView.getMeasuredHeight() >= height)
                {
                    //This indicates that the user has scrolled to the end of webview
                    accept.setEnabled(true);
                }

            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();
        if (tos==null) {
            fetchTOS();
        }
    }

    @Override
    public void onBackPressed() {
        //background app on back button press event
        moveTaskToBack(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        if (tos != null) {
            outState.putString("APP_TOS", tos);
        }
    }

    private void fetchTOS () {
        RequestQueue queue = RemoteDataManager.sharedManager(this);
        JSONObject jsonObject = new JSONObject();
        JSONObject encyptedJson = new JSONObject();
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();
        try {
            jsonObject.put("lang", Locale.getDefault().getLanguage());
            encyptedJson.put("data", RemoteDataManager.encrypt(specialKey, jsonObject.toString(), randomIV));
        }catch (Exception je){
            if (RemoteDataManager.SHOW_LOG)
                Log.e("AppTerms", je.getMessage());
        }

        bar.setVisibility(View.VISIBLE);

        AuthRequest appTermsRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getTermsOfServiceUrl(), encyptedJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject dataEncrypted) {

                try {
                    if (dataEncrypted.has("data")) {
                        String encryptedtos = dataEncrypted.getString("data");

                        JSONObject data = null;
                        try {
                            String decodedTOS = RemoteDataManager.decrypt(specialKey, encryptedtos, randomIV);
                            data = new JSONObject(decodedTOS);
                        } catch (Exception e) {
                            //Unable to decode
                            if (RemoteDataManager.SHOW_LOG)
                                Log.e("AppTerms", e.getMessage());
                        }
                        if ((data != null) && data.has("system_tos"))
                        {
                            String systemTOS = data.getString("system_tos");
                            JSONObject systemJson = new JSONObject(systemTOS.substring(systemTOS.indexOf("{"), systemTOS.lastIndexOf("}") + 1));
                            String tos = systemJson.getString("text");
                            webView.loadDataWithBaseURL(null, tos, "text/html", "UTF-8", null);
                            bar.setVisibility(View.GONE);
                        }
                    }
                }catch (JSONException je)
                {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_SERVER_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_SERVER_ERROR);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //show alert
                AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(bar.getContext());
                anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                anonymousAlert.setCancelable(false);
                anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                fetchTOS();
                                dialog.cancel();
                            }
                        });


                AlertDialog alert = anonymousAlert.create();
                alert.show();
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);

        queue.add(appTermsRequest);

    }
}
