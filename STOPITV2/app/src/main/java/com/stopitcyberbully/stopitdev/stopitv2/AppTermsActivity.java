package com.stopitcyberbully.stopitdev.stopitv2;


import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


public class AppTermsActivity extends AppBaseActivity {

    private WebView webView;
    private int clickCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_terms);
        ActionBar actionBar = getSupportActionBar();

        webView = (WebView) findViewById(R.id.webview_terms_of_use);

        TextView TermsTitle = (TextView) findViewById(R.id.title_terms_of_use);
        CustomFontHelper.setCustomFont(TermsTitle, "source-sans-pro.semibold.ttf", this);

        if (savedInstanceState != null)
        {
            clickCount = savedInstanceState.getInt("LOGOUT_CLICK_COUNT");
        }

        if (TermsTitle != null) {
            TermsTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickCount++;
                    if (clickCount >= 3) {
                        showLogoutAlert();
                    }
                }
            });
            //Check if this is a workplace user, we will have to change back color for them
            if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
                TermsTitle.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
            }
        }



        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_APPLICATION_TERMS_OF_USE, AnalyticsManager.ANALYTICS_ACTION_OPENED_APPLICATION_TERMS_OF_USE);
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        fetchTOS();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("LOGOUT_CLICK_COUNT", clickCount);
    }

    private void fetchTOS () {
        RequestQueue queue = RemoteDataManager.sharedManager(this);
        JSONObject jsonObject = new JSONObject();
        JSONObject encyptedJson = new JSONObject();
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();
        try {
            jsonObject.put("lang", Locale.getDefault().getLanguage());
            encyptedJson.put("data", RemoteDataManager.encrypt(specialKey, jsonObject.toString(), randomIV));
        }catch (Exception je){
            if (RemoteDataManager.SHOW_LOG)
                Log.e("AppTerms", je.getMessage());
        }


        AuthRequest appTermsRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getTermsOfServiceUrl(), encyptedJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject dataEncrypted) {

                try {
                    if (dataEncrypted.has("data")) {
                        String encryptedtos = dataEncrypted.getString("data");

                        JSONObject data = null;
                        try {
                            String decodedTOS = RemoteDataManager.decrypt(specialKey, encryptedtos, randomIV);
                            data = new JSONObject(decodedTOS);
                        } catch (Exception e) {
                            //Unable to decode
                            if (RemoteDataManager.SHOW_LOG)
                                Log.e("AppTerms", e.getMessage());
                        }

                        if( (data != null) && data.has("system_tos")) {
                            String systemTOS = data.getString("system_tos");
                            JSONObject systemJson = new JSONObject(systemTOS.substring(systemTOS.indexOf("{"), systemTOS.lastIndexOf("}") + 1));
                            String tos = systemJson.getString("text");
                            webView.loadDataWithBaseURL(null, tos, "text/html", "UTF-8", null);
                        }
                    }
                }catch (JSONException je)
                {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_SERVER_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_SERVER_ERROR);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);

        queue.add(appTermsRequest);

        }
    @Override
    protected void onResume() {
        super.onResume();
        //show popup for inbox messages
        updateInboxCounts(true);
    }
}
