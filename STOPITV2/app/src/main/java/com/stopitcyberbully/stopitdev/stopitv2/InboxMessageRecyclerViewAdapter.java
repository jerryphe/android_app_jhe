package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.daimajia.swipe.SwipeLayout;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.InboxMessage;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.MessageStatus;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.ArrayList;


public class InboxMessageRecyclerViewAdapter extends RecyclerView.Adapter<InboxMessageRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<InboxMessage> mValues;
    private final InboxMessageFragment.OnListFragmentInteractionListener mListener;
    private final DateFormat df  = new DateFormat();
    Context mContext;
    int selected_index = -1;

//
    public InboxMessageRecyclerViewAdapter(ArrayList<InboxMessage> items, InboxMessageFragment.OnListFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
        setHasStableIds(false);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_inboxmessage, parent, false);



        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        String dateStr = DateFormat.getDateFormat(mContext).format(mValues.get(position).getCreatedDate());
        holder.mIdView.setText(dateStr);
        holder.mContentView.setText(mValues.get(position).getSubject());



        if (holder.mItem.getMessageStatusID()== MessageStatus.INBOX_MESSAGE_STATUS_NEW)
        {
//            holder.mView.setBackgroundResource(R.drawable.shadow_effect);
            holder.mContentView.setTextColor(Color.parseColor("#000000"));
            holder.mIdView.setTextColor(Color.parseColor("#959595"));
            holder.mUnReadDotView.setVisibility(View.VISIBLE);
            holder.unread.setVisibility(View.INVISIBLE);
            holder.mView.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.swipeLayout.setBackgroundColor(Color.parseColor("#ffffff"));

        }else {
//            holder.mView.setBackgroundResource(R.drawable.read_shadow_effect);
            holder.mContentView.setTextColor(Color.parseColor("#959595"));
            holder.mIdView.setTextColor(Color.parseColor("#959595"));
            holder.mUnReadDotView.setVisibility(View.INVISIBLE);
            holder.unread.setVisibility(View.VISIBLE);
            holder.mView.setBackgroundColor(Color.parseColor("#cacaca"));
            holder.swipeLayout.setBackgroundColor(Color.parseColor("#cacaca"));
        }

        if (holder.mItem.getIsSelected())
        {
            if (holder.mRadioSelected != null) holder.mRadioSelected.setChecked(true);

        }else{
            if (holder.mRadioGroupSelected != null) holder.mRadioGroupSelected.clearCheck();

        }



        if (((InboxActivity) mContext ).isEditing())
        {
            if (holder.mRadioSelected != null) holder.mRadioSelected.setVisibility(View.VISIBLE);
            if (holder.mRadioGroupSelected != null) holder.mRadioGroupSelected.setVisibility(View.VISIBLE);
        }else{
            if (holder.mRadioSelected != null) holder.mRadioSelected.setVisibility(View.INVISIBLE);
            if (holder.mRadioGroupSelected != null) holder.mRadioGroupSelected.setVisibility(View.INVISIBLE);
        }
        if (mListener.isEditing()) {
            holder.swipeLayout.setSwipeEnabled(false);
        }else {
            holder.swipeLayout.setSwipeEnabled(true);
        }

//        holder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (null != mListener) {
//                    // Notify the active callbacks interface (the activity, if the
//                    // fragment is attached to one) that an item has been selected.
//                    mListener.onListFragmentInteraction(holder.mItem, position);
//                }
//            }
//        });
//        if (selected_index == position){
//            holder.swipeLayout.open(true, false);
//        }

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ImageView mUnReadDotView;
        public final RadioButton mRadioSelected;
        public final RadioGroup mRadioGroupSelected;
        public InboxMessage mItem;
        public final SwipeLayout swipeLayout;
        public final LinearLayout linearLayout;

        public final Button delete;
        public final Button unread;
        public final SwipeLayout.SwipeListener swipeListener;



        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.inbox_date_label);
            mContentView = (TextView) view.findViewById(R.id.inbox_subject_label);
            mUnReadDotView = (ImageView) view.findViewById(R.id.image_unread_dot);
            mRadioSelected = (RadioButton) view.findViewById(R.id.inbox_message_selected);
            mRadioGroupSelected = (RadioGroup) view.findViewById(R.id.radio_group_selection);
            swipeLayout =  (SwipeLayout) view.findViewById(R.id.swipe_inbox);
            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
            linearLayout = (LinearLayout) view.findViewById(R.id.bottom_wrapper);


            delete = (Button) swipeLayout.findViewById(R.id.image_item_delete);
            unread = (Button) swipeLayout.findViewById(R.id.image_item_unread);

            swipeListener = new SwipeLayout.SwipeListener() {
                @Override
                public void onStartOpen(SwipeLayout layout) {
                }

                @Override
                public void onOpen(SwipeLayout layout) {
                    if (selected_index != getAdapterPosition()) {
                        mListener.setIsSwipeMenuOpen(true, getAdapterPosition());
                    }
                }

                @Override
                public void onStartClose(SwipeLayout layout) {

                }

                @Override
                public void onClose(SwipeLayout layout) {
                    mListener.setIsSwipeMenuOpen(false, getAdapterPosition());
                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                }
            };

            mRadioSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    boolean unreadSelected = false;
                    boolean anythingSelected = false;
                    if (position < RemoteDataManager.getInboxMessageArrayList().size())
                    {
                        InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);
                        inboxMessage.setIsSelected(!inboxMessage.getIsSelected());
                        if (inboxMessage.getIsSelected())
                        {
                            mRadioSelected.setChecked(true);
                        }else{
                            mRadioSelected.setChecked(false);
                            mRadioGroupSelected.clearCheck();
                        }

                    }
                    for (int i=0; i<RemoteDataManager.getInboxMessageArrayList().size(); i++){
                        InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(i);
                        if (inboxMessage.getIsSelected()) {
                            anythingSelected = true;
                            if (inboxMessage.getMessageStatusID() == MessageStatus.INBOX_MESSAGE_STATUS_READ) {
                                unreadSelected = true;
                                break;
                            }
                        }

                    }
                    mListener.updateActionButtons(unreadSelected, anythingSelected);
                }
            });
            mContentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (((InboxActivity) mContext).isEditing()) {
                        boolean unreadSelected = false;
                        boolean anythingSelected = false;
                        if (position < RemoteDataManager.getInboxMessageArrayList().size()) {
                            InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);
                            inboxMessage.setIsSelected(!inboxMessage.getIsSelected());
                            if (inboxMessage.getIsSelected()) {
                                mRadioSelected.setChecked(true);
                            } else {
                                mRadioSelected.setChecked(false);
                                mRadioGroupSelected.clearCheck();
                            }

                        }
                        for (int i = 0; i < RemoteDataManager.getInboxMessageArrayList().size(); i++) {
                            InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(i);
                            if (inboxMessage.getIsSelected()) {
                                anythingSelected = true;
                                if (inboxMessage.getMessageStatusID() == MessageStatus.INBOX_MESSAGE_STATUS_READ) {
                                    unreadSelected = true;
                                    break;
                                }
                            }

                        }
                        mListener.updateActionButtons(unreadSelected, anythingSelected);
                    } else {
                        InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);

                        mListener.onListFragmentInteraction(inboxMessage, position);
                    }
                }
            });

            if (mListener.isEditing())
            {
                swipeLayout.setSwipeEnabled(false);
                swipeLayout.removeSwipeListener(swipeListener);
            }else {
                swipeLayout.setSwipeEnabled(true);
                swipeLayout.addSwipeListener(swipeListener);
            }

            swipeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);
                    mListener.onListFragmentInteraction(inboxMessage, position);
                }
            });


            Button unread = (Button) swipeLayout.findViewById(R.id.image_item_unread);
            if (unread != null)
            {
                unread.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = getAdapterPosition();
                        if (position < RemoteDataManager.getInboxMessageArrayList().size()) {
                            InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);
                            inboxMessage.setIsSelected(true);
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_MARKED_NOTIFICATION_UNREAD_WITH_SWIPE, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_MARKED_NOTIFICATION_UNREAD_WITH_SWIPE);
                            mListener.showDialogWhileProcessing(v, "unread");
                        }
                    }
                });
            }
            Button delete = (Button) swipeLayout.findViewById(R.id.image_item_delete);

            if (delete != null)
            {
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = getAdapterPosition();
                        if (position < RemoteDataManager.getInboxMessageArrayList().size()) {
                            InboxMessage inboxMessage = RemoteDataManager.getInboxMessageArrayList().get(position);
                            inboxMessage.setIsSelected(true);
                            if (inboxMessage.getMessageStatusID() != MessageStatus.INBOX_MESSAGE_STATUS_READ) {
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_DELETED_UNREAD_NOTIFICATION_WITH_SWIPE, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_DELETED_UNREAD_NOTIFICATION_WITH_SWIPE);
                            }else {
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_INBOX_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_DELETED_READ_NOTIFICATION_WITH_SWIPE, AnalyticsManager.ANALYTICS_ACTION_INBOX_USER_DELETED_READ_NOTIFICATION_WITH_SWIPE);
                            }

                            mListener.showDialogWhileProcessing(v, "delete");
                        }
                    }
                });
            }
            CustomFontHelper.setCustomFont(mIdView, "source-sans-pro.semibold.ttf", mContext);
            CustomFontHelper.setCustomFont(mContentView, "source-sans-pro.semibold.ttf", mContext);

        }


        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

}
