package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings.Secure;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.stopitcyberbully.stopitdev.stopitv2.BlockedGateActivity;
import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.StopitApplication;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.ImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.ChatMessage;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.AWS;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Chat;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Contact;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Country;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.CrisisCenter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.InboxMessage;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.IncidentObject;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.MessageStatus;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Organization;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Registration;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.User;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.UserContacts;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * Core Network related code handler, contains list of api calls being made, remote server url.
 */

public class RemoteDataManager {
    //Server URLs
    public static final boolean isPROD = true;

    //This method gives out the server URL based on the prod boolean flag
    public static String getServerUrl() {

    String SERVER_URL = "https://api-mobile.stopitcyberbully.com";
        if (RemoteDataManager.isPROD)
        {
            SERVER_URL = "https://api-mobile.stopitcyberbully.com";
        }else {
//            SERVER_URL = "https://api-mobile-staging.stopit.fm";
            SERVER_URL = "https://api-mobile-preprod.stopit.fm";
//            SERVER_URL = "https://api-mobile-dr.stopitcyberbully.com";

        }
        return SERVER_URL;
    }


    public static final String INBOX_NOTIFICATION_BROADCAST_ID = "com.stopitcyberbully.push";
    private static final String VANITY_DEVICE_REGISTRATION_URL = "/v2/devices/getIntellicodes";
    private static final String DEVICE_REGISTRATION_URL = "/v2/devices/register";
    private static final String COUNTRY_URL = "/v2/countries";
    private static final String ORG_SEARCH_URL = "/v2/orgs/search";
    private static final String CONTACTS_URL = "/v1/intellicodes/";
    private static final String TERMS_OF_SERVICE_URL = "/v2/tos/latest";
    private static final String INCIDENT_CREATION_URL = "/v1/intellicodes/";
    private static final String PRIVACY_POLICY_URL = "http://www.stopitcyberbully.com/stopit-full-privacy-policy/";
    private static final String CHAT_HISTORY_URL = "/v2/intellicodes/getChatHistory";
    private static final String LOGOUT_URL = "/v2/devices/logouts";



    public static final String RESPONSE_OK = "100000";
    public static final String RESPONSE_ERROR = "100005";
    public static final String RESPONSE_NO_RESULTS = "100017";
    public static final String RESPONSE_INACTIVE_USER = "100021";
    public static final String RESPONSE_DEACTIVATED_USER = "100022";
    public static final String RESPONSE_EXPIRED_USER = "100023";

    public static final int CHAT_HISTORY_SIZE = 50;

    public static final int DEFAULT_TIMEOUT = 10000;
    public static final int CHAT_HISTORY_TIMEOUT = 15000;



    //inbox related
    public static final String INBOX_MESSAGE_COUNT_URL = "/v2/intellicodes/inbox/count";
    public static final String INBOX_MESSAGE_LIST_URL = "/v2/intellicodes/inbox/list";
    public static final String INBOX_MESSAGE_RETRIEVAL_URL = "/v2/intellicodes/inbox/get_message";
    public static final String INBOX_MESSAGE_ATTACHMENT_RETRIEVAL_URL = "/v2/intellicodes/inbox/get_message_url";
    public static final String INBOX_MESSAGE_STATUS_UPDATE_URL = "/v2/intellicodes/inbox/update";
    public static final int INBOX_MESSAGE_PAGE_SIZE = 500;
    public static String getInboxMessageStatusUpdateUrl() {
        return RemoteDataManager.getServerUrl()+ INBOX_MESSAGE_STATUS_UPDATE_URL;
    }

    public static String getInboxMessageRetrieveUrl() {
        return RemoteDataManager.getServerUrl()+ INBOX_MESSAGE_RETRIEVAL_URL;
    }

    public static String getInboxMessageAttachmentRetrievalUrl() {
        return RemoteDataManager.getServerUrl()+ INBOX_MESSAGE_ATTACHMENT_RETRIEVAL_URL;
    }

    public static String getInboxMessageCountUrl() {
        return RemoteDataManager.getServerUrl()+ INBOX_MESSAGE_COUNT_URL;
    }

    public static String getInboxMessageListUrl() {
        return RemoteDataManager.getServerUrl()+ INBOX_MESSAGE_LIST_URL;
    }

    public static String getLogoutUrl() {
        return RemoteDataManager.getServerUrl()+ LOGOUT_URL;
    }

    //Global flag which determines if log is displayed or not.
    public static final boolean SHOW_LOG = false;


    public static String getPrivacyPolicyUrl() {
        return PRIVACY_POLICY_URL;
    }

    public static final long MAX_TOTAL_FILE_SIZE = 263192576;

    //Encryption Related
    static Cipher cipher;
    private static final byte[] iv = getFixedIV();



    private static final String ROSTER_SEARCH_URL = "/v2/intellicodes/search";


    public static final int ORG_TYPE_K12 = 1;
    private static Contact currentContact;

    public static Contact getCurrentContact() {
        return currentContact;
    }

    public static void setCurrentContact(Contact currentContact) {
        RemoteDataManager.currentContact = currentContact;
    }

    public static String getINCIDENT_CREATION_URL() {
        return RemoteDataManager.getServerUrl()+ INCIDENT_CREATION_URL + getRegistration().getUser().getIntellicode() + "/incidents";
    }

    public static String getSaveContactsURL(){
        return RemoteDataManager.getServerUrl()+ CONTACTS_URL + getRegistration().getUser().getIntellicode() + "/contacts";
    }

    public static String getChatHistoryUrl(){
        return RemoteDataManager.getServerUrl()+ CHAT_HISTORY_URL ;
    }


    public static final int ORG_TYPE_HIGHERED = 2;
    public static final int ORG_TYPE_WORKPLACE = 3;

    public static final int INCIDENT_CATEGORY_REGULAR = 1;
    public static final int INCIDENT_CATEGORY_PANIC = 2;
    public static final int INCIDENT_CATEGORY_CHAPERONE = 3;
    public static final int INCIDENT_CATEGORY_1TOUCH = 4;

    //Volley Queues
    private static RequestQueue queue;

    //Singleton Data
    private static Country country;
    private static ArrayList<Country> countryList;
    private static Registration registration;

    public static UserContacts getUserContacts() {
        return userContacts;
    }

    public static void setUserContacts(UserContacts userContacts) {
        RemoteDataManager.userContacts = userContacts;
    }

    private static UserContacts userContacts;
    public static int CURRENT_ORG_TYPE = 1;
    public static ArrayList<Organization> orgList;
    public static Organization selectedOrganization;

    public static void setUnreadMessageCount(Context context, int unreadMessageCount) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("UNREAD_MESSAGE_COUNT", unreadMessageCount);
        editor.apply();

    }

    public static int getUnreadMessageCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        return preferences.getInt("UNREAD_MESSAGE_COUNT", 0);
    }

    public static void setUnreadInboxMessageCount(Context context, int unreadMessageCount) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("UNREAD_INBOX_MESSAGE_COUNT", unreadMessageCount);
        editor.apply();

    }

    public static int getAppUpdateCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        return preferences.getInt("PENDING_UPDATE_COUNT", 0);
    }

    public static void setAppUpdateCount(Context context, int unreadMessageCount) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("PENDING_UPDATE_COUNT", unreadMessageCount);
        editor.apply();

    }

    public static int getUnreadInboxMessageCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        return preferences.getInt("UNREAD_INBOX_MESSAGE_COUNT", 0);
    }

    public static ArrayList<InboxMessage> getInboxMessageArrayList() {
        if (inboxMessageArrayList == null)
        {
            inboxMessageArrayList = new ArrayList<>();
        }
        return inboxMessageArrayList;
    }
    public static void clearInboxMessageArrayList() {
        if (inboxMessageArrayList != null)
        {
            inboxMessageArrayList.clear();
        }
    }

    private static ArrayList<InboxMessage> inboxMessageArrayList;

    public static void resetInboxMessageList(){
        if (RemoteDataManager.inboxMessageArrayList != null) {
            RemoteDataManager.inboxMessageArrayList.clear();
        }else if (inboxMessageArrayList == null)
        {
            inboxMessageArrayList = new ArrayList<>();
        }

    }

    public static void resetMessageList() {
        if (RemoteDataManager.messageList != null) {
            RemoteDataManager.messageList.clear();
        }
    }

    //Chat messages
    private static ArrayList<ChatMessage> messageList;

    public static ArrayList<ChatMessage> getMessageList() {
        if (messageList == null)
        {
            messageList = new ArrayList<>();
        }
        return messageList;
    }

    public static void addMessage(ChatMessage message)
    {
        int unread = RemoteDataManager.getUnreadMessageCount(StopitApplication.getAppContext());
        unread ++;
        RemoteDataManager.setUnreadMessageCount(StopitApplication.getAppContext(), unread);
        ArrayList<ChatMessage>localMessageList = getMessageList();
        localMessageList.add(message);

    }

    public static ArrayList<IncidentObject> getIncidentObjects() {
        return incidentObjects;
    }

    public static void setIncidentObjects(ArrayList<IncidentObject> incidentObjects) {
        RemoteDataManager.incidentObjects = incidentObjects;
    }

    //Incident image array
    private static ArrayList<IncidentObject> incidentObjects;


    public static Organization getSelectedOrganization() {
        return selectedOrganization;
    }

    public static void setSelectedOrganization(Organization selectedOrganization) {
        RemoteDataManager.selectedOrganization = selectedOrganization;
    }

    public static ArrayList<Organization> getOrgList() {
        return orgList;
    }

    public static void setOrgList(ArrayList<Organization> orgList) {
        RemoteDataManager.orgList = orgList;
    }

    public static int getCURRENT_ORG_TYPE() {
        return CURRENT_ORG_TYPE;
    }

    public static void setCURRENT_ORG_TYPE(int CURRENT_ORG_TYPE) {
        RemoteDataManager.CURRENT_ORG_TYPE = CURRENT_ORG_TYPE;
    }




    public static Registration getRegistration() {
        return registration;
    }

    public static void setRegistration(Registration registration) {
        RemoteDataManager.registration = registration;
    }



    //appwide constants
    public static final int INTELLICODE_LENGTH = 12;
    public static final int VANITY_INTELLICODE_LENGTH = 3;

    public static String getCOUNTRY_URL() {
        return RemoteDataManager.getServerUrl() + RemoteDataManager.COUNTRY_URL;
    }
    public static String getOrgSearchURL() {
        return RemoteDataManager.getServerUrl() + ORG_SEARCH_URL;
    }

    public static String getROSTER_SEARCH_URL() {
        return RemoteDataManager.getServerUrl()+ROSTER_SEARCH_URL;
    }
    public static String getDeviceRegistrationUrl() {
        return RemoteDataManager.getServerUrl() +DEVICE_REGISTRATION_URL;
    }
    public static String getVanityRegistrationUrl() {
        return RemoteDataManager.getServerUrl() + VANITY_DEVICE_REGISTRATION_URL;
    }
    public static String getContactsUrl() {
        return RemoteDataManager.getServerUrl() + CONTACTS_URL;
    }
    public static String getTermsOfServiceUrl() {

        return RemoteDataManager.getServerUrl() + TERMS_OF_SERVICE_URL;
    }


    public static ArrayList<Country> getCountryList() {
        return countryList;
    }

    public static void setCountryList(ArrayList<Country> countryList) {
        RemoteDataManager.countryList = countryList;
    }


    public static Country getCountry() {
        return country;
    }

    public static void setCountry(Country country) {
        RemoteDataManager.country = country;
    }




    public static RequestQueue sharedManager (Context context)
    {
        if (queue == null)
        {
            queue = Volley.newRequestQueue(context);
        }
        return queue;
    }
    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }



    public static void getContacts (Context context,  final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        final String contactsURL = RemoteDataManager.getContactsUrl()+RemoteDataManager.getRegistration().getUser().getIntellicode()+"/contacts";

        //register device
        AuthRequest contactRequest = new AuthRequest(Request.Method.GET,
                contactsURL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject json) {
                        try {
                            if (json.has("data")) {
                                userContacts = new UserContacts();
                                JSONObject data = null;
                                if (getEncryptionSalt() != null) {
                                    String contacts = json.getString("data");

                                    try {
                                        String decodedContacts = RemoteDataManager.decrypt(getEncryptionSalt(), contacts);
                                        data = new JSONObject(decodedContacts);
                                    } catch (Exception e) {
                                        //Unable to decode
                                    }
                                }else {

                                    data = json.getJSONObject("data");
                                }
                                if (data != null) {
                                    if (data.has("contacts")) {
                                        ArrayList<Contact> contactList = new ArrayList<>();
                                        JSONArray contactsArray = data.getJSONArray("contacts");
                                        for (int i = 0; i < contactsArray.length(); i++) {
                                            Contact contact = new Contact();
                                            contact.setEmail(contactsArray.getJSONObject(i).getString("email"));
                                            contact.setName(contactsArray.getJSONObject(i).getString("name"));
                                            contact.setMobile_phone(contactsArray.getJSONObject(i).getString("mobile_phone"));
                                            contact.setOrg_id(contactsArray.getJSONObject(i).getInt("org_id"));
                                            contact.setUser_id(contactsArray.getJSONObject(i).getInt("user_id"));

                                            if (contactsArray.getJSONObject(i).getString("is_get_help").equals("Y")) {
                                                contact.setGetHelpContact(true);
                                            }else{
                                                contact.setGetHelpContact(false);
                                            }

                                            if (contactsArray.getJSONObject(i).getString("is_trusted").equals("Y")) {
                                                contact.setTrustedContact(true);
                                            }else{
                                                contact.setTrustedContact(false);
                                            }

                                            if (contactsArray.getJSONObject(i).getString("is_one_touch").equals("Y")) {
                                                contact.setOneTouchContact(true);
                                            }else{
                                                contact.setOneTouchContact(false);
                                            }


                                            if (contact.getOrg_id() > 0) {
                                                //Org contacts always remain selected
                                                contact.setIsSelected(true);
                                                contactList.add(0, contact);
                                            } else {
                                                contactList.add(contact);
                                            }
                                        }

                                        RemoteDataManager.getUserContacts().setContactArrayList(contactList);
                                    }
                                }else{
                                    //This indicates decryption failure
                                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_FETCH_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_FETCH_ERROR);

                                }
                            }

                        }catch (JSONException je)
                        {
                            if (RemoteDataManager.SHOW_LOG) Log.e("EnterAccessCodeActivity", je.toString());
                        }
                        if (successCallBack != null) {
                            successCallBack.onResponse(json);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (failureCallBack != null) {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);
                    failureCallBack.onErrorResponse(volleyError);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(contactRequest);
    }

    public static void registerDevice (final Context context, String intellicode, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {

        JSONObject registerJson = new JSONObject();
        final JSONObject encryptedJson = new JSONObject();
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();

        try {
            registerJson.put("intellicode", intellicode);
            registerJson.put("serial_number", getDeviceID(context));
            encryptedJson.put("data", RemoteDataManager.encrypt(specialKey, registerJson.toString(), randomIV));

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        //register device
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getDeviceRegistrationUrl(), encryptedJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {
                        try {
                            JSONObject json = null;
                            if (dataEncrypted.has("code")) {




                                    String code = dataEncrypted.getString("code");
                                    switch (code) {
                                        case RemoteDataManager.RESPONSE_OK:
                                            if (dataEncrypted.has("data")) {
                                                try {
                                                    String deviceDecryptedData = dataEncrypted.getString("data");
                                                    String deviceDecrypted = RemoteDataManager.decrypt(specialKey, deviceDecryptedData, randomIV);
                                                    json = new JSONObject(deviceDecrypted);
                                                } catch (Exception e) {
                                                    //Unable to decode
                                                }
                                                if (json == null) {
                                                    return;
                                                }
                                                if (json.has("aws")) {
                                                    Registration registration = new Registration();

                                                    if (json.has("aws")) {
                                                        AWS aws = new AWS();
                                                        JSONObject awsData = json.getJSONObject("aws");
                                                        aws.setCognito_pool_id(awsData.getString("cognitoidentitypool_id"));
                                                        aws.setBucket(awsData.getString("bucket"));
                                                        aws.setRegion(awsData.getString("region"));
                                                        registration.setAws(aws);
                                                    }

                                                    if (json.has("user")) {
                                                        User user = new User();
                                                        JSONObject userData = json.getJSONObject("user");
                                                        user.setIntellicode(userData.getString("intellicode"));
                                                        user.setSalt(userData.getString("salt"));
                                                        user.setUserID(userData.getInt("user_id"));
                                                        registration.setUser(user);
                                                        saveIntellicode(context, user.getIntellicode());

                                                    }

                                                    if (json.has("org")) {
                                                        Organization organization = new Organization();
                                                        JSONObject org = json.getJSONObject("org");
                                                        organization.setOrgID(org.getInt("org_id"));
                                                        organization.setOrgName(org.getString("org_name"));
                                                        organization.setOrgTypeID(org.getInt("org_type_id"));
                                                        organization.setLogoPath(org.getString("logo_path"));


                                                        if (org.getInt("user_can_create_contact_in_app") > 0) {
                                                            organization.setIsManageContactsPermitted(true);
                                                        } else {
                                                            organization.setIsManageContactsPermitted(false);
                                                        }

                                                        if (org.getInt("incident_to_report_mgr") > 0) {
                                                            organization.setIsIncidentToReportManagerRequired(true);
                                                        } else {
                                                            organization.setIsIncidentToReportManagerRequired(false);
                                                        }
                                                        if (org.getInt("org_alive_flag") > 0) {
                                                            organization.setIsAlive(true);
                                                        } else {
                                                            organization.setIsAlive(false);
                                                        }
                                                        saveOrgType(context, org.getInt("org_type_id"));

                                                        if (org.getInt("is_in_recess") > 0) {
                                                            organization.setInRecess(true);
                                                        } else {
                                                            organization.setInRecess(false);
                                                        }

                                                        if (org.getInt("include_messenger_in_app") > 0) {
                                                            organization.setIncludeMessenger(true);
                                                        } else {
                                                            organization.setIncludeMessenger(false);
                                                        }

                                                        if (org.has("video_url")) {
                                                            if (!org.getString("video_url").equals("null")) {
                                                                organization.setVideoURL(org.getString("video_url"));
                                                            } else {
                                                                organization.setVideoURL(null);
                                                            }
                                                        } else {
                                                            organization.setVideoURL(null);
                                                        }

                                                        if (org.has("disallow_upload_media")) {
                                                            if (org.getInt("disallow_upload_media") >= 1) {
                                                                organization.setIsMediaUploadDisabled(true);
                                                            } else {
                                                                organization.setIsMediaUploadDisabled(false);
                                                            }
                                                        } else {
                                                            organization.setIsMediaUploadDisabled(false);
                                                        }

                                                        registration.setOrg(organization);
                                                    }

                                                    if (json.has("crisis_center")) {
                                                        JSONObject crisisCenterData = json.getJSONObject("crisis_center");

                                                        //Check if crisis center is active or not
                                                        if (crisisCenterData.getInt("active") > 0) {
                                                            CrisisCenter crisisCenter = new CrisisCenter();
                                                            crisisCenter.setCrisisCenterID(crisisCenterData.getInt("crisis_center_id"));
                                                            crisisCenter.setCrisisCenterName(crisisCenterData.getString("name"));
                                                            crisisCenter.setCrisisCenterPhone(crisisCenterData.getString("phone"));
                                                            crisisCenter.setCrisisCenterEmail(crisisCenterData.getString("email"));
                                                            if ((crisisCenterData.getString("mobile_phone") != null) && !(crisisCenterData.getString("mobile_phone").equals("null"))) {
                                                                crisisCenter.setCrisisCenterMobilePhone(crisisCenterData.getString("mobile_phone"));
                                                            }
                                                            registration.setCrisisCenter(crisisCenter);
                                                        }
                                                    }

                                                    if (json.has("chat")) {
                                                        Chat chat = new Chat();
                                                        JSONObject chatData = json.getJSONObject("chat");

                                                        JSONObject serverData = chatData.getJSONObject("server");
                                                        chat.setChatServerURL(serverData.getString("url"));
                                                        chat.setChatServerPort(serverData.getInt("port"));

                                                        JSONObject adminData = chatData.getJSONObject("admin");
                                                        chat.setChatAdminUserName(adminData.getString("username"));

                                                        JSONObject selfData = chatData.getJSONObject("self");
                                                        chat.setChatUserName(selfData.getString("username"));
                                                        chat.setChatUserPassword(selfData.getString("password"));

                                                        try {
                                                            if (chatData.has("history_message_count")) {
                                                                chat.setHistory_count(chatData.getInt("history_message_count"));
                                                            } else {
                                                                chat.setHistory_count(RemoteDataManager.CHAT_HISTORY_SIZE);
                                                            }
                                                        } catch (JSONException je) {
                                                            chat.setHistory_count(RemoteDataManager.CHAT_HISTORY_SIZE);
                                                        }

                                                        registration.setChat(chat);
                                                    }

                                                    //Get a list of current channels already subscribed
                                                    ArrayList currChannels = (ArrayList) ParseInstallation.getCurrentInstallation().getList("channels");

                                                    String channel = "USER_" + registration.getUser().getIntellicode();
                                                    ParsePush.subscribeInBackground(channel);
                                                    if (currChannels != null) {
                                                        if (!currChannels.isEmpty() && currChannels.contains(channel)) {
                                                            currChannels.remove(channel);
                                                        }
                                                    }

                                                    channel = "ORG_ID_" + registration.getOrg().getOrgID();
                                                    ParsePush.subscribeInBackground(channel);
                                                    if (currChannels != null) {
                                                        if (!currChannels.isEmpty() && currChannels.contains(channel)) {
                                                            currChannels.remove(channel);
                                                        }
                                                    }

                                                    if (currChannels != null) {
                                                        //now remove unused channels
                                                        if (!currChannels.isEmpty()) {
                                                            for (Object remchan : currChannels) {
                                                                if (remchan.getClass().equals(String.class)) {
                                                                    ParsePush.unsubscribeInBackground(remchan.toString());
                                                                }
                                                            }
                                                        }
                                                    }


                                                    RemoteDataManager.setRegistration(registration);

                                                    if (successCallBack != null) {
                                                        successCallBack.onResponse(dataEncrypted);
                                                    }

                                                }
                                            }
                                            break;
                                        case RESPONSE_DEACTIVATED_USER:
                                        case RESPONSE_INACTIVE_USER:
                                        case RESPONSE_EXPIRED_USER:
                                            if (successCallBack != null) {
                                                successCallBack.onResponse(dataEncrypted);
                                            }
                                            break;
                                        default:
                                            if (failureCallBack != null) {
                                                failureCallBack.onErrorResponse(null);
                                            }
                                            break;
                                    }


                            }
                        }catch (JSONException je)
                        {
                            if (failureCallBack!=null)
                            {
                                failureCallBack.onErrorResponse(null);
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (failureCallBack!=null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                    failureCallBack.onErrorResponse(error);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(stringRequest);

    }
    public static void registerVanityDevice (final Context context, String intellicode, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {

        JSONObject registerJson = new JSONObject();
        final JSONObject encryptedJson = new JSONObject();
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();

        try {
            registerJson.put("regis_code", intellicode);
            registerJson.put("serial_number", getDeviceID(context));
            encryptedJson.put("data", RemoteDataManager.encrypt(specialKey, registerJson.toString(), randomIV));

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        //register device
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getVanityRegistrationUrl(), encryptedJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {
                        try {
                            JSONObject json = null;
                            if (dataEncrypted.has("code")) {




                                String code = dataEncrypted.getString("code");
                                switch (code) {
                                    case RemoteDataManager.RESPONSE_OK:
                                        if (dataEncrypted.has("data")) {
                                            try {
                                                String deviceDecryptedData = dataEncrypted.getString("data");
                                                String deviceDecrypted = RemoteDataManager.decrypt(specialKey, deviceDecryptedData, randomIV);
                                                if (successCallBack != null) {
                                                    JSONObject jsonObject = new JSONObject(deviceDecrypted);
                                                    successCallBack.onResponse(jsonObject);
                                                }
                                            } catch (Exception e) {
                                                //Unable to decode
                                            }
                                            if (json == null) {
                                                return;
                                            }



                                        }
                                        break;
                                    case RESPONSE_DEACTIVATED_USER:
                                    case RESPONSE_INACTIVE_USER:
                                    case RESPONSE_EXPIRED_USER:
                                        if (successCallBack != null) {
                                            successCallBack.onResponse(dataEncrypted);
                                        }
                                        break;
                                    default:
                                        if (failureCallBack != null) {
                                            failureCallBack.onErrorResponse(null);
                                        }
                                        break;
                                }


                            }
                        }catch (JSONException je)
                        {
                            if (failureCallBack!=null)
                            {
                                failureCallBack.onErrorResponse(null);
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (failureCallBack!=null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                    failureCallBack.onErrorResponse(error);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(stringRequest);

    }

    public static void logoutFromApp (final Context context, String intellicode, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {

        JSONObject registerJson = new JSONObject();
        final JSONObject encryptedJson = new JSONObject();
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();

        try {
            registerJson.put("intellicode", intellicode);
            encryptedJson.put("data", RemoteDataManager.encrypt(specialKey, registerJson.toString(), randomIV));

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        //register device
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getLogoutUrl(), encryptedJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {
                        try {
                            JSONObject json = null;
                            if (dataEncrypted.has("code")) {




                                String code = dataEncrypted.getString("code");
                                switch (code) {
                                    case RemoteDataManager.RESPONSE_OK:
                                        if (dataEncrypted.has("data")) {
                                            try {
                                                String deviceDecryptedData = dataEncrypted.getString("data");
                                                String deviceDecrypted = RemoteDataManager.decrypt(specialKey, deviceDecryptedData, randomIV);
                                                if (successCallBack != null) {
                                                    JSONObject jsonObject = new JSONObject(deviceDecrypted);
                                                    successCallBack.onResponse(jsonObject);
                                                }
                                            } catch (Exception e) {
                                                //Unable to decode
                                            }
                                            if (json == null) {
                                                return;
                                            }



                                        }
                                        break;
                                    case RESPONSE_DEACTIVATED_USER:
                                    case RESPONSE_INACTIVE_USER:
                                    case RESPONSE_EXPIRED_USER:
                                        if (successCallBack != null) {
                                            successCallBack.onResponse(dataEncrypted);
                                        }
                                        break;
                                    default:
                                        if (failureCallBack != null) {
                                            failureCallBack.onErrorResponse(null);
                                        }
                                        break;
                                }


                            }
                        }catch (JSONException je)
                        {
                            if (failureCallBack!=null)
                            {
                                failureCallBack.onErrorResponse(null);
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (failureCallBack!=null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                    failureCallBack.onErrorResponse(error);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(stringRequest);

    }

    public static void registerByRoster (final Context context, JSONObject jsonObject, final Response.Listener <JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        final JSONObject encryptedJson = new JSONObject();
        final String randomIV = RemoteDataManager.getRandomIV(16);
        final String specialKey = RemoteDataManager.getSpecialKey();

        try {

            encryptedJson.put("data", RemoteDataManager.encrypt(specialKey, jsonObject.toString(), randomIV));

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }

        AuthRequest jsonObjectRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getROSTER_SEARCH_URL(), encryptedJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject json) {
                try {
                    if (json.has("code")) {
                        String code = json.getString("code");
                        switch (code) {
                            case RemoteDataManager.RESPONSE_OK:
                                if (json.has("data")) {
                                    Registration registration = new Registration();
                                    JSONObject data = null;
                                    try {
                                        String deviceDecryptedData = json.getString("data");
                                        String deviceDecrypted = RemoteDataManager.decrypt(specialKey, deviceDecryptedData, randomIV);
                                        data = new JSONObject(deviceDecrypted);
                                    } catch (Exception e) {
                                        //Unable to decode
                                    }
                                    if (data == null) {
                                        return;
                                    }

                                    if (data.has("user")) {
                                        User user = new User();
                                        JSONObject userData = data.getJSONObject("user");
                                        user.setIntellicode(userData.getString("intellicode"));
                                        user.setSalt(userData.getString("salt"));
                                        user.setUserID(userData.getInt("user_id"));
                                        registration.setUser(user);
                                        saveIntellicode(context, user.getIntellicode());
                                    }


                                    if (data.has("org")) {
                                        Organization organization = new Organization();
                                        JSONObject org = data.getJSONObject("org");
                                        organization.setOrgID(org.getInt("org_id"));
                                        organization.setOrgName(org.getString("org_name"));
                                        organization.setOrgTypeID(org.getInt("org_type_id"));
                                        organization.setLogoPath(org.getString("logo_path"));

                                        if (org.getInt("user_can_create_contact_in_app") > 0) {
                                            organization.setIsManageContactsPermitted(true);
                                        } else {
                                            organization.setIsManageContactsPermitted(false);
                                        }
                                        if (org.getInt("incident_to_report_mgr") > 0) {
                                            organization.setIsIncidentToReportManagerRequired(true);
                                        } else {
                                            organization.setIsIncidentToReportManagerRequired(false);
                                        }
                                        if (org.has("org_alive_flag")) {
                                            if (org.getInt("org_alive_flag") > 0) {
                                                organization.setIsAlive(true);
                                            } else {
                                                organization.setIsAlive(false);
                                            }
                                        }else{
                                            organization.setIsAlive(true);
                                        }
                                        saveOrgType(context, org.getInt("org_type_id"));

                                        if (org.getInt("is_in_recess") > 0) {
                                            organization.setInRecess(true);
                                        } else {
                                            organization.setInRecess(false);
                                        }

                                        if (org.getInt("include_messenger_in_app") > 0) {
                                            organization.setIncludeMessenger(true);
                                        } else {
                                            organization.setIncludeMessenger(false);
                                        }

                                        if (org.has("video_url")){
                                            if (!org.getString("video_url").equals("null")) {
                                                organization.setVideoURL(org.getString("video_url"));
                                            }else{
                                                organization.setVideoURL(null);
                                            }
                                        }else{
                                            organization.setVideoURL(null);
                                        }
                                        if (org.has("disallow_upload_media"))
                                        {
                                            if (org.getInt("disallow_upload_media") >=1)
                                            {
                                                organization.setIsMediaUploadDisabled(true);
                                            }else {
                                                organization.setIsMediaUploadDisabled(false);
                                            }
                                        }else {
                                            organization.setIsMediaUploadDisabled(false);
                                        }


                                        registration.setOrg(organization);
                                    }

                                    if (data.has("crisis_center")) {

                                        JSONObject crisisCenterData = data.getJSONObject("crisis_center");

                                        //Check if crisis center is active or not
                                        if (crisisCenterData.getInt("active") > 0) {
                                            CrisisCenter crisisCenter = new CrisisCenter();

                                            crisisCenter.setCrisisCenterID(crisisCenterData.getInt("crisis_center_id"));
                                            crisisCenter.setCrisisCenterName(crisisCenterData.getString("name"));
                                            crisisCenter.setCrisisCenterPhone(crisisCenterData.getString("phone"));
                                            crisisCenter.setCrisisCenterEmail(crisisCenterData.getString("email"));
                                            if ((crisisCenterData.getString("mobile_phone") != null) && !(crisisCenterData.getString("mobile_phone").equals("null"))) {
                                                crisisCenter.setCrisisCenterMobilePhone(crisisCenterData.getString("mobile_phone"));
                                            }
                                            registration.setCrisisCenter(crisisCenter);
                                        }
                                    }

                                    if (data.has("chat")) {
                                        Chat chat = new Chat();
                                        JSONObject chatData = data.getJSONObject("chat");

                                        JSONObject serverData = chatData.getJSONObject("server");
                                        chat.setChatServerURL(serverData.getString("url"));
                                        chat.setChatServerPort(serverData.getInt("port"));

                                        JSONObject adminData = chatData.getJSONObject("admin");
                                        chat.setChatAdminUserName(adminData.getString("username"));

                                        JSONObject selfData = chatData.getJSONObject("self");
                                        chat.setChatUserName(selfData.getString("username"));
                                        chat.setChatUserPassword(selfData.getString("password"));

                                        registration.setChat(chat);
                                    }


                                    String channel = "USER_" + registration.getUser().getIntellicode();
                                    ParsePush.subscribeInBackground(channel);
                                    channel = "ORG_ID_" + registration.getOrg().getOrgID();
                                    ParsePush.subscribeInBackground(channel);
                                    RemoteDataManager.setRegistration(registration);

                                    if (successCallBack != null) {
                                        successCallBack.onResponse(encryptedJson);
                                    }
                                }
                                break;
                            case RESPONSE_DEACTIVATED_USER:
                            case RESPONSE_INACTIVE_USER:
                            case RESPONSE_EXPIRED_USER:
                                if (successCallBack != null) {
                                    successCallBack.onResponse(json);
                                }
                                break;
                            default:
                                if (failureCallBack != null) {
                                    failureCallBack.onErrorResponse(null);
                                }
                                break;
                        }
                    }
                }catch (JSONException je)
                {
                    if (failureCallBack != null)
                    {
                        failureCallBack.onErrorResponse(null);
                    }


                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (failureCallBack != null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);
                    failureCallBack.onErrorResponse(null);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(jsonObjectRequest);
    }

    private static void saveIntellicode(Context context, String intellicode){
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("INTELLICODE", intellicode);
        editor.apply();
    }
    public static String getIntellicode(Context context){
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        return preferences.getString("INTELLICODE", null);
    }
    public static void wipeIntellicode (Context context){
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("INTELLICODE");
        editor.remove("ORG_TYPE");
        editor.apply();

        //remove chat messages too
        if (RemoteDataManager.messageList != null) {
            RemoteDataManager.messageList.clear();
        }

        //remove the inbox messages too
        RemoteDataManager.clearInboxMessageArrayList();


    }
    private static void saveOrgType(Context context, int ORG_TYPE){
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("ORG_TYPE", ORG_TYPE);
        editor.apply();
    }
    public static int getSavedOrgType(Context context){
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        return preferences.getInt("ORG_TYPE", 1);
    }
    public static String getDeviceID(Context context)
    {
        return Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
    }

    public static boolean sendImages(Context context, String incidentID, ImageListAdapter imageAdapter)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        long millitime = calendar.getTimeInMillis();
        boolean showFailureMessage = false;

        if (imageAdapter.getCount()>0) {
            String analytics_label = "Number of attached media: " + imageAdapter.getCount();
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ADDED_MEDIA, analytics_label);
        }

        for (int i=0; i < imageAdapter.getCount(); i ++)
        {

            JSONObject incidentJSON = new JSONObject();
            JSONObject dataJSON = new JSONObject();

            JSONObject attachmentObject = new JSONObject();
            IncidentObject incidentObject = imageAdapter.getItem(i);

            try {
                String filename = "file" + i + "." + incidentObject.getExtension(context);
                attachmentObject.put("filename", filename);


                String dirName = "orgs/" + getRegistration().getOrg().getOrgID() + "/incidents/" + incidentID + "/" + millitime +"/attachments/" + filename;
                attachmentObject.put("url", dirName);



                JSONArray dataArray = new JSONArray();
                dataArray.put(attachmentObject);
                incidentJSON.put("incident_category_id", Integer.toString(RemoteDataManager.INCIDENT_CATEGORY_REGULAR));

                JSONObject submitJSON = new JSONObject();
                dataJSON.put("attachments", dataArray);
                if (RemoteDataManager.getEncryptionSalt() != null)
                {
                    String EncryptedIncident = RemoteDataManager.encrypt(RemoteDataManager.getEncryptionSalt(), dataJSON.toString());
                    submitJSON.put("data", EncryptedIncident);
                }else {
                    submitJSON.put("data", dataJSON);
                }

                //First notify API server
                RemoteDataManager.updateIncident(context, submitJSON, incidentID, ((i + 1) == imageAdapter.getCount()));


                //Now upload images.
                if (!sendToAWS(context, dirName, incidentObject.getFile(context)))
                {
                    showFailureMessage = true;
                }


            }catch (JSONException je)
            {
                if (RemoteDataManager.SHOW_LOG) Log.e("ReportActivity", je.toString());
            }catch (Exception e)
            {
                if (RemoteDataManager.SHOW_LOG) Log.e("ReportActivity", e.toString());
            }
        }
        return showFailureMessage;
    }

    public static void updateIncident(Context context, final JSONObject incidentObject, String incidentID, final boolean finish_after_this)
    {

        RequestQueue queue = RemoteDataManager.sharedManager(context);
        String incidentUpdateURL = RemoteDataManager.getINCIDENT_CREATION_URL() + "/" + incidentID;
        AuthRequest incidentRequest = new AuthRequest(Request.Method.PUT, incidentUpdateURL, incidentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                //This simply indicates that api server has been notified about pending media upload.
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //Show retry option
                //record failure
            }
        }, DEFAULT_TIMEOUT);
        queue.add(incidentRequest);
    }

    public static boolean sendToAWS(Context context, String filename, File file)
    {
        boolean result = false;
        // Initialize the Amazon Cognito credentials provider
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                context, // Context
                RemoteDataManager.getRegistration().getAws().getCognito_pool_id(),
                RemoteDataManager.getRegistration().getAws().getRegion_code()
        );

        TransferManager transferManager = new TransferManager(credentialsProvider);
        Upload upload = transferManager.upload(RemoteDataManager.getRegistration().getAws().getBucket(), filename, file);



        try {
            upload.waitForUploadResult();
            result = true;
        }catch (InterruptedException | AmazonClientException ie)
        {
            if (RemoteDataManager.SHOW_LOG) Log.e("S3TransferManager", ie.toString());

        }finally {
            transferManager.shutdownNow();
        }

        return result;

    }
    private static byte[] getFixedIV()
    {
        char[] iv = new char[]{'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};
        return new String(iv).getBytes();
    }
    public static String getRandomIV(int len)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        return uuid.substring(len);
    }
    public static String getSpecialKey()
    {
        char[] special_chars = new char[] {'b', 'd', '2', '5', '3', '0', 'a', '7', '4', 'w', '1', '9', 'b', 'c', '0', '6', 'a', 'l', 'x', 'g', 'h', '8', 'k', 's', 'b', 'd', '2', '5', '3', '0', 'a', '7'};
        return new String(special_chars);
    }

    public static String getEncryptionSalt()
    {
        String encryptionSalt = null;

        if (getRegistration().getUser().getSalt().length()>0) {
          encryptionSalt = getRegistration().getUser().getIntellicode() + getRegistration().getUser().getSalt();
        }

        return encryptionSalt;
    }
    public static String decrypt(String seed, String encrypted) throws Exception {

        byte[] encryptedBytes = Base64.decode(encrypted.getBytes("utf-8"), Base64.CRLF);
        byte[] keyb = seed.getBytes("utf-8");
        SecretKeySpec skey = new SecretKeySpec(keyb, "AES");
        Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, skey, new IvParameterSpec(iv));


        byte[] clearbyte = dcipher.doFinal(encryptedBytes);
        return new String(clearbyte);
    }
    public static String decrypt(String seed, String encrypted, String ivVar) throws Exception {

        byte[] encryptedBytes = Base64.decode(encrypted.getBytes("utf-8"), Base64.CRLF);
        byte[] keyb = seed.getBytes("utf-8");
        SecretKeySpec skey = new SecretKeySpec(keyb, "AES");
        Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, skey, new IvParameterSpec(ivVar.getBytes()));


        byte[] clearbyte = dcipher.doFinal(encryptedBytes);
        return new String(clearbyte);
    }
    public static String encrypt(String key, String content) throws Exception {
        byte[] input = content.getBytes("utf-8");


        SecretKeySpec skc = new SecretKeySpec(key.getBytes("utf-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skc, new IvParameterSpec(iv));

        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
        cipher.doFinal(cipherText, ctLength);
        return Base64.encodeToString(cipherText, Base64.CRLF);
    }
    public static String encrypt(String key, String content, String ivVar) throws Exception {
        byte[] input = content.getBytes("utf-8");


        SecretKeySpec skc = new SecretKeySpec(key.getBytes("utf-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skc, new IvParameterSpec(ivVar.getBytes()));

        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
        cipher.doFinal(cipherText, ctLength);
        return ivVar + Base64.encodeToString(cipherText, Base64.CRLF);
    }
    public static String getISOCode(){
        return Locale.getDefault().getCountry();
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void hasAppExpired(final Context context, final Response.Listener <JSONObject> optionalCallBack, final Response.Listener mandatoryCallBack)
    {
        try {
             PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            final long appversion = pInfo.versionCode;

            String HEALTH_CHECK_URL = "https://s3.amazonaws.com/stopit-public/app-settings/statuscheck.json";
            if (!RemoteDataManager.isPROD) {
                HEALTH_CHECK_URL = "https://s3.amazonaws.com/stopit-public/app-settings/statuscheck-preprod.json";
            }

            JsonObjectRequest healthRequest = new JsonObjectRequest(Request.Method.GET, HEALTH_CHECK_URL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                        if (RemoteDataManager.SHOW_LOG) Log.d("RemoteDataManager", jsonObject.toString());
                    if (jsonObject.has("android")){
                        try {
                            JSONObject android_data = jsonObject.getJSONObject("android");
                            String minversion_str = (String) android_data.get("minversion");
                            String currversion_str = (String) android_data.get("currversion");

                            float minversion = new Float(minversion_str);
                            float currversion = new Float(currversion_str);

//                            currversion = 90.0f;
//
//                            minversion = 90.0f;
                            if (appversion < minversion){
                                //Mandatory Update
                                if (RemoteDataManager.SHOW_LOG) Log.d("RemoteDataManager", "Mandatory Update");
                                Intent blockedIntent = new Intent(context, BlockedGateActivity.class);
                                blockedIntent.putExtra("MANDATORY_UPDATE", true);
                                context.startActivity(blockedIntent);

                            } else if (appversion < currversion)
                            {
                                //Flag the pending update
                                setAppUpdateCount(context, 1);
                                //Optional Update
                                if (RemoteDataManager.SHOW_LOG) Log.d("RemoteDataManager", "Mandatory Update");
                                //Launch expiry notification
                                final String my_package_name = "com.stopitcyberbully.mobile";
                                String url;

                                try {
                                    //Check whether Google Play store is installed or not:
                                    context.getPackageManager().getPackageInfo("com.android.vending", 0);

                                    url = "market://details?id=" + my_package_name;
                                } catch ( final Exception e ) {
                                    url = "https://play.google.com/store/apps/details?id=" + my_package_name;
                                }


                                //Open the app page in Google Play store:
                                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                                NotificationCompat.Builder b = new NotificationCompat.Builder(context);

                                b.setAutoCancel(true)
                                        .setDefaults(Notification.DEFAULT_ALL)
                                        .setWhen(System.currentTimeMillis())
                                        .setSmallIcon(R.drawable.ic_push)
                                        .setTicker(context.getResources().getString(R.string.app_name))
                                        .setContentTitle(context.getResources().getString(R.string.update_available))
                                        .setContentText(context.getResources().getString(R.string.update_available_description))
                                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                                        .setContentIntent(contentIntent)
                                        .setContentInfo(context.getResources().getString(R.string.waiting_message_prompt_read));


                                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.notify(2, b.build());
                                if (optionalCallBack != null)
                                {
                                    optionalCallBack.onResponse(new JSONObject());
                                }
                            }else {
                                //No update is pending, we can reset this flag to 0
                                setAppUpdateCount(context, 0);
                                if (optionalCallBack != null)
                                {
                                    optionalCallBack.onResponse(null);
                                }
                            }

                        }catch (JSONException je)
                        {
                            //If we do not get expected app update file then we do not need to report anything to the user.
                            //The integrity checks on server side should catch the json formatting error.
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    if (RemoteDataManager.SHOW_LOG)
                        Log.d("RemoteDataManager", volleyError.toString());
                }
            });
            queue.add(healthRequest);

            if (RemoteDataManager.SHOW_LOG) Log.d("AppExpiryCheck", pInfo.versionName);
        }catch (PackageManager.NameNotFoundException nnf){
            if (RemoteDataManager.SHOW_LOG) Log.e("AppExpiryCheck", nnf.getMessage());
        }
    }

    public static void getChatHistory (final Response.Listener <JSONObject> successCallBack, final Response.ErrorListener failureCallBack){

        JSONObject chatHistoryObject = new JSONObject();
        try{
            chatHistoryObject.put("chat_username", RemoteDataManager.getRegistration().getChat().getChatUserName());
            chatHistoryObject.put("intellicode", RemoteDataManager.getRegistration().getUser().getIntellicode());
            chatHistoryObject.put("pageSize", RemoteDataManager.getRegistration().getChat().getHistory_count());
        }catch (JSONException je){
            if (failureCallBack != null){
                failureCallBack.onErrorResponse(null);
            }
            return;
        }
        JSONObject jsonUser = new JSONObject();
        try {
            if (RemoteDataManager.getRegistration().getUser().getSalt().length() > 0) {
                String unEncrypted = chatHistoryObject.toString();
                String password = RemoteDataManager.getRegistration().getUser().getIntellicode() + RemoteDataManager.getRegistration().getUser().getSalt();

                String enCrypted = RemoteDataManager.encrypt(password, unEncrypted);
                jsonUser.put("data", enCrypted);
                jsonUser.put("intellicode", RemoteDataManager.getRegistration().getUser().getIntellicode());
            } else {
                jsonUser.put("data", chatHistoryObject);
            }
        }catch (Exception e)
        {
            return;
        }

        AuthRequest chatHistoryRequest = new AuthRequest(Request.Method.POST, getChatHistoryUrl(), jsonUser, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    if (response.has("data"))
                    {
                        JSONObject data = null;
                        if (getEncryptionSalt() != null) {
                            String history = response.getString("data");

                            try {
                                String decodedHistory = RemoteDataManager.decrypt(getEncryptionSalt(), history);
                                data = new JSONObject(decodedHistory);
                            } catch (Exception e) {
                                //Unable to decode
                            }
                        }else {

                            data = response.getJSONObject("data");
                        }

                        if (data.has("chatHistory"))
                        {
                            JSONArray history = data.getJSONArray("chatHistory");
                            ArrayList<ChatMessage>localMessageList = getMessageList();
                            String fromUserName = RemoteDataManager.getRegistration().getChat().getChatUserName();

//                            localMessageList.clear();

                            for (int i=0; i< history.length(); i++) {
                                JSONObject chat = history.getJSONObject(i);
                                ChatMessage cm = new ChatMessage();
                                if (chat.getString("fromJID").startsWith(fromUserName)) {
                                    cm.setIncoming(false);
                                } else {
                                    cm.setIncoming(true);
                                }
                                cm.setFrom(chat.getString("fromJID"));
                                cm.setTo(chat.getString("toJID"));
                                cm.setMessage(chat.getString("body"));
                                cm.setMessageDate(new Date(Long.parseLong(chat.getString("sentDate"))));

                                if (chat.has("stanza")) {
                                    String stanza = chat.getString("stanza");
                                    if ((stanza != null) && (stanza.length() > 0)) {
                                        try {
                                            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                                            factory.setNamespaceAware(true);
                                            XmlPullParser xpp = factory.newPullParser();

                                            xpp.setInput(new StringReader(stanza));
                                            int eventType = xpp.getEventType();
                                            while (eventType != XmlPullParser.END_DOCUMENT) {
                                                if (eventType == XmlPullParser.START_DOCUMENT) {
                                                    System.out.println("Start document");
                                                } else if (eventType == XmlPullParser.START_TAG) {
                                                    if (xpp.getName().equalsIgnoreCase("message"))
                                                    {
                                                        if (xpp.getAttributeValue(null, "id") != null) {
                                                            cm.setUniqueID(xpp.getAttributeValue(null, "id"));
                                                        }else {
                                                            cm.setUniqueID(chat.getString("messageId"));
                                                        }
                                                    }
                                                    System.out.println("Start tag " + xpp.getName());
                                                } else if (eventType == XmlPullParser.END_TAG) {
                                                    System.out.println("End tag " + xpp.getName());
                                                } else if (eventType == XmlPullParser.TEXT) {
                                                    System.out.println("Text " + xpp.getText());
                                                }
                                                eventType = xpp.next();
                                            }
                                        } catch (XmlPullParserException xe) {
                                            cm.setUniqueID(""+chat.getInt("messageId"));
                                        } catch (IOException ioe) {
                                            cm.setUniqueID(""+chat.getInt("messageId"));
                                        }
                                    }else{
                                        cm.setUniqueID(""+chat.getInt("messageId"));
                                    }
                                }
                                if (!localMessageList.contains(cm)) {
                                    localMessageList.add(cm);
                                }
                            }
                            Collections.sort(localMessageList);

                        }
                    }
                }catch (JSONException je)
                {
                    if (failureCallBack != null){
                        failureCallBack.onErrorResponse(null);
                    }
                }
                if (successCallBack != null) {
                    successCallBack.onResponse(response);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (failureCallBack != null){
                    failureCallBack.onErrorResponse(error);
                }
            }
        }, CHAT_HISTORY_TIMEOUT);
        queue.add(chatHistoryRequest);
    }
//Inbox related
public static void getInboxCounts (final Context context, String intellicode, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
{

    JSONObject registerJson = new JSONObject();
    final JSONObject encryptedJson = new JSONObject();


    try {
        registerJson.put("intellicode", intellicode);
        encryptedJson.put("data", RemoteDataManager.encrypt(getEncryptionSalt(), registerJson.toString()));
        encryptedJson.put("intellicode", intellicode);

    } catch (Exception e) {
        if (failureCallBack!=null)
        {
            failureCallBack.onErrorResponse(null);
        }
    }
    RequestQueue queue = RemoteDataManager.sharedManager(context);

    //register device
    AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getInboxMessageCountUrl(), encryptedJson,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject dataEncrypted) {
                    try {
                        JSONObject json = null;
                        if (dataEncrypted.has("code")) {

                            if (dataEncrypted.has("data")) {

                                try {
                                    String deviceDecryptedData = dataEncrypted.getString("data");
                                    String deviceDecrypted = RemoteDataManager.decrypt(getEncryptionSalt(), deviceDecryptedData);
                                    json = new JSONObject(deviceDecrypted);
                                } catch (Exception e) {
                                    //Unable to decode
                                }
                                if (json == null) {
                                    return;
                                }
                                String code = dataEncrypted.getString("code");
                                switch (code) {
                                    case RemoteDataManager.RESPONSE_OK: {

                                        int unread_count = json.getInt("unread_count");
                                        setUnreadInboxMessageCount(context, unread_count);
                                        if (successCallBack != null) {
                                            successCallBack.onResponse(json);
                                        }

                                    }
                                    break;
                                    case RESPONSE_DEACTIVATED_USER:
                                    case RESPONSE_INACTIVE_USER:
                                    case RESPONSE_EXPIRED_USER:
                                        if (successCallBack != null) {
                                            successCallBack.onResponse(json);
                                        }
                                        break;
                                    default:
                                        if (failureCallBack != null) {
                                            failureCallBack.onErrorResponse(null);
                                        }
                                        break;
                                }
                            }
                        }

                    }catch (JSONException je)
                    {
                        if (failureCallBack!=null)
                        {
                            failureCallBack.onErrorResponse(null);
                        }

                    }
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            if (failureCallBack!=null)
            {
                //This indicates network failure
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                failureCallBack.onErrorResponse(error);
            }
        }
    }, DEFAULT_TIMEOUT);
    queue.add(stringRequest);

}
    public static void getInboxMessageList(final Context context, String intellicode, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {

        JSONObject registerJson = new JSONObject();
        final JSONObject encryptedJson = new JSONObject();


        try {
            registerJson.put("intellicode", intellicode);
            registerJson.put("pageSize", INBOX_MESSAGE_PAGE_SIZE);
            registerJson.put("pageNum", 1);
            registerJson.put("sortBy", "message_status_id" );
            registerJson.put("sortDirection", "asc" );
            encryptedJson.put("data", RemoteDataManager.encrypt(getEncryptionSalt(), registerJson.toString()));
            encryptedJson.put("intellicode", intellicode);

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }
        RequestQueue queue = RemoteDataManager.sharedManager(context);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        //register device
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getInboxMessageListUrl(), encryptedJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {
                        try {
                            JSONObject json = null;
                            if (dataEncrypted.has("code")) {
                                RemoteDataManager.resetInboxMessageList();
                                    String code = dataEncrypted.getString("code");
                                    switch (code) {
                                        case RemoteDataManager.RESPONSE_OK: {
                                            if (dataEncrypted.has("data")) {


                                                try {
                                                    String deviceDecryptedData = dataEncrypted.getJSONObject("data").getString("inbox_list");
                                                    String deviceDecrypted = RemoteDataManager.decrypt(getEncryptionSalt(), deviceDecryptedData);
                                                    JSONArray messageArray = new JSONArray(deviceDecrypted);
                                                    json = new JSONObject();
                                                    json.put("inbox_list", messageArray);
                                                    RemoteDataManager.resetInboxMessageList();
                                                    for (int i = 0; i < messageArray.length(); i++) {
                                                        JSONObject message = messageArray.getJSONObject(i);
                                                        InboxMessage inboxMessage = new InboxMessage(message.getString("subject"),
                                                                message.getString("message_id"),
                                                                sdf.parse(message.getString("created_date")),
                                                                false,
                                                                MessageStatus.fromIntValue(message.getInt("message_status_id")));
                                                        RemoteDataManager.inboxMessageArrayList.add(inboxMessage);
                                                    }
                                                } catch (Exception e) {
                                                    //Unable to decode
                                                    if (failureCallBack != null) {
                                                        failureCallBack.onErrorResponse(null);
                                                    }
                                                }
                                                if (json == null) {
                                                    return;
                                                }


                                                if (successCallBack != null) {
                                                    successCallBack.onResponse(json);
                                                }

                                            }
                                        }
                                        break;
                                        case RESPONSE_NO_RESULTS:
                                            RemoteDataManager.resetInboxMessageList();
                                            if (successCallBack != null) {
                                                successCallBack.onResponse(json);
                                            }
                                            break;
                                        case RESPONSE_DEACTIVATED_USER:
                                        case RESPONSE_INACTIVE_USER:
                                        case RESPONSE_EXPIRED_USER:
                                            if (failureCallBack != null) {
                                                failureCallBack.onErrorResponse(null);
                                            }
                                            break;
                                        default:
                                            if (failureCallBack != null) {
                                                failureCallBack.onErrorResponse(null);
                                            }
                                            break;
                                    }
                                }


                        }catch (JSONException je)
                        {
                            if (failureCallBack!=null)
                            {
                                failureCallBack.onErrorResponse(null);
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (failureCallBack!=null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                    failureCallBack.onErrorResponse(error);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(stringRequest);
        //Refetch the counts
        RemoteDataManager.getInboxCounts(context, intellicode, null, null);

    }
    public static void getInboxMessage(final Context context, String intellicode, String messageID, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {

        JSONObject registerJson = new JSONObject();
        final JSONObject encryptedJson = new JSONObject();


        try {
            registerJson.put("intellicode", intellicode);
            registerJson.put("message_id", messageID);
            encryptedJson.put("data", RemoteDataManager.encrypt(getEncryptionSalt(), registerJson.toString()));
            encryptedJson.put("intellicode", intellicode);

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        //register device
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getInboxMessageRetrieveUrl(), encryptedJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {

                        JSONObject json = null;
                        try {
                            if (dataEncrypted.has("code")) {
                                if (dataEncrypted.getString("code").equals(RemoteDataManager.RESPONSE_OK)) {

                                    if (dataEncrypted.has("data")) {

                                        try {
                                            String messageDecryptedData = dataEncrypted.getString("data");
                                            String messageDecrypted = RemoteDataManager.decrypt(getEncryptionSalt(), messageDecryptedData);
                                            json = new JSONObject(messageDecrypted.substring(messageDecrypted.indexOf("{"), messageDecrypted.lastIndexOf("}") + 1));
                                        } catch (Exception e) {
                                            //Unable to decode
                                            if (failureCallBack != null) {
                                                failureCallBack.onErrorResponse(null);
                                            }
                                        }
                                        if (json != null) {

                                            if (successCallBack != null) {
                                                successCallBack.onResponse(json);
                                            }
                                        }

                                    }
                                } else {
                                    if (RemoteDataManager.SHOW_LOG) Log.e("Inbox", dataEncrypted.toString());
                                    if (failureCallBack != null) {
                                        failureCallBack.onErrorResponse(null);
                                    }
                                }
                            }
                        }catch (JSONException je){
                            if (failureCallBack != null) {
                                failureCallBack.onErrorResponse(null);
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (failureCallBack!=null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                    failureCallBack.onErrorResponse(error);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(stringRequest);

    }
    public static void getInboxMessageAttchment(final Context context, String intellicode, String messageID, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {

        JSONObject registerJson = new JSONObject();
        final JSONObject encryptedJson = new JSONObject();


        try {
            registerJson.put("intellicode", intellicode);
            registerJson.put("message_id", messageID);
            encryptedJson.put("data", RemoteDataManager.encrypt(getEncryptionSalt(), registerJson.toString()));
            encryptedJson.put("intellicode", intellicode);

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        //register device
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getInboxMessageAttachmentRetrievalUrl(), encryptedJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {

                        JSONObject json = null;
                        try {
                            if (dataEncrypted.has("code")) {
                                if (dataEncrypted.getString("code").equals(RemoteDataManager.RESPONSE_OK)) {

                                    if (dataEncrypted.has("data")) {

                                        try {
                                            String messageDecryptedData = dataEncrypted.getString("data");
                                            String messageDecrypted = RemoteDataManager.decrypt(getEncryptionSalt(), messageDecryptedData);
                                            json = new JSONObject(messageDecrypted.substring(messageDecrypted.indexOf("{"), messageDecrypted.lastIndexOf("}") + 1));
                                        } catch (Exception e) {
                                            //Unable to decode
                                            if (failureCallBack != null) {
                                                failureCallBack.onErrorResponse(null);
                                            }
                                        }
                                        if (json != null) {

                                            if (successCallBack != null) {
                                                successCallBack.onResponse(json);
                                            }
                                        }

                                    }
                                } else {
                                    if (RemoteDataManager.SHOW_LOG) Log.e("Inbox", dataEncrypted.toString());
                                    if (failureCallBack != null) {
                                        failureCallBack.onErrorResponse(null);
                                    }
                                }
                            }
                        }catch (JSONException je){
                            if (failureCallBack != null) {
                                failureCallBack.onErrorResponse(null);
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (failureCallBack!=null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                    failureCallBack.onErrorResponse(error);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(stringRequest);

    }
    public static void UpdateInboxMessageStatus(final Context context, String intellicode, String messageID, String action, final Response.Listener<JSONObject> successCallBack, final Response.ErrorListener failureCallBack)
    {

        JSONObject registerJson = new JSONObject();
        final JSONObject encryptedJson = new JSONObject();


        try {
            registerJson.put("intellicode", intellicode);
            registerJson.put("message_id", messageID);
            registerJson.put("action", action);
            encryptedJson.put("data", RemoteDataManager.encrypt(getEncryptionSalt(), registerJson.toString()));
            encryptedJson.put("intellicode", intellicode);

        } catch (Exception e) {
            if (failureCallBack!=null)
            {
                failureCallBack.onErrorResponse(null);
            }
        }
        RequestQueue queue = RemoteDataManager.sharedManager(context);

        //register device
        AuthRequest stringRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getInboxMessageStatusUpdateUrl(), encryptedJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject dataEncrypted) {

                        try {
                            if (dataEncrypted.has("code")) {
                                if (dataEncrypted.getString("code").equals(RemoteDataManager.RESPONSE_OK)) {

                                    if (successCallBack != null) {
                                        successCallBack.onResponse(dataEncrypted);
                                    }
                                } else {
                                    if (failureCallBack != null) {
                                        failureCallBack.onErrorResponse(null);
                                    }
                                }
                            }
                        }catch (JSONException je){
                            if (failureCallBack != null) {
                                failureCallBack.onErrorResponse(null);
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (failureCallBack!=null)
                {
                    //This indicates network failure
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_ALERT_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR, AnalyticsManager.ANALYTICS_ACTION_ALERT_NETWORK_ERROR);

                    failureCallBack.onErrorResponse(error);
                }
            }
        }, DEFAULT_TIMEOUT);
        queue.add(stringRequest);

    }
    public static void deselectInBoxMessages()
    {
        for (InboxMessage inboxMessage: RemoteDataManager.getInboxMessageArrayList())
        {
            inboxMessage.setIsSelected(false);
        }
    }

    public static void markInboxMessagesAsRead()
    {
        Context context = StopitApplication.getAppContext();
        for (InboxMessage inboxMessage: RemoteDataManager.getInboxMessageArrayList())
        {
            if (inboxMessage.getIsSelected())
            {
                RemoteDataManager.UpdateInboxMessageStatus(context, RemoteDataManager.getIntellicode(context), inboxMessage.getMessageID(), "unread", null, null);
            }
        }
    }
}
