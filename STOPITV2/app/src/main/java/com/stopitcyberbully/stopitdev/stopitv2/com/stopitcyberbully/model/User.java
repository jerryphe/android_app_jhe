package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;


/**
 * Object model to store data related to user information obtained from webservice (typically device registration or from roster)
 */
public class User {
    private String intellicode;
    private String salt;
    private int userID;


    public String getIntellicode() {
        return intellicode;
    }

    public void setIntellicode(String intellicode) {
        this.intellicode = intellicode;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }



}
