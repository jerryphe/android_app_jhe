package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

import java.util.ArrayList;

/**
 * Object model to store data related to country information obtained from webservice
 */
public class Country implements CodeNameObject {

    private  String code;
    private String name;
    private ArrayList<State> states;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<State> getStates() {
        return states;
    }

    public void setStates(ArrayList<State> states) {
        this.states = states;
    }


}
