package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.webkit.WebView;

import com.stopitcyberbully.stopitdev.stopitv2.R;

public class RemoteWebView extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_web_view);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        WebView webView = (WebView) findViewById(R.id.remote_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        Intent webIntent = this.getIntent();
        String url = webIntent.getStringExtra("STOPIT_URL");
        if (url != null)
        {
            webView.loadUrl(url);
        }else {
            webView.loadUrl("http://www.stopitcyberbully.com");
        }
    }
}
