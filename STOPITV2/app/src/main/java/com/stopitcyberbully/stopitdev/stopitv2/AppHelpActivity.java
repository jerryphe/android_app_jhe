package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.GenericImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.ArrayList;
import java.util.Collections;


public class AppHelpActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_help);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TextView helpTitle = (TextView) findViewById(R.id.title_help);
        CustomFontHelper.setCustomFont(helpTitle, "source-sans-pro.semibold.ttf", this);
        TextView versionTitle = (TextView) findViewById(R.id.title_version);
        CustomFontHelper.setCustomFont(helpTitle, "source-sans-pro.semibold.ttf", this);

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            versionTitle.setText(getString(R.string.title_version, pInfo.versionName));
        }catch (PackageManager.NameNotFoundException nnf)
        {
            //This means we could not get version name from operating system.
        }



        //Check if this is a workplace user, we will have to change back color for them
        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
            helpTitle.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
            versionTitle.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
        }


        ImageView helpImage = (ImageView) findViewById(R.id.image_help);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        helpImage.setAnimation(alphaAnimation);

        final ListView listview = (ListView) findViewById(R.id.help_menu_listview);
        String[] values = new String[] {
                getResources().getString(R.string.HELP_REPORTING_INCIDENTS),
                getResources().getString(R.string.HELP_GETTING_HELP),
                getResources().getString(R.string.HELP_CONTACTS),
                getResources().getString(R.string.HELP_ABOUT)
        };

        final ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, values);


        Integer[] imageId = {R.drawable.help_white};
        GenericImageListAdapter adapter = new GenericImageListAdapter(this, list, imageId);

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_HELP_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_HELP_SCREEN);


        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent viewHelpIntent = new Intent(AppHelpActivity.this, HelpViewActivity.class);
                switch (position)
                {
                    case 0:
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_REPORTING_INCIDENT_IN_HELP, AnalyticsManager.ANALYTICS_ACTION_OPENED_REPORTING_INCIDENT_IN_HELP);
                        viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/"+ getString(R.string.helpreportingincident)+".html");
                        break;
                    case 1:
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_GETTING_HELP_IN_HELP, AnalyticsManager.ANALYTICS_ACTION_OPENED_GETTING_HELP_IN_HELP);
                        viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/"+ getString(R.string.helpgettinghelp)+".html");
                        break;
                    case 2:
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_CONTACTS_HELP_IN_HELP, AnalyticsManager.ANALYTICS_ACTION_OPENED_CONTACTS_HELP_IN_HELP);
                        viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/"+ getString(R.string.helpcontacts)+".html");
                        break;
                    case 3:
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_ABOUT_STOPIT_IN_HELP, AnalyticsManager.ANALYTICS_ACTION_OPENED_ABOUT_STOPIT_IN_HELP);
                        viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/"+ getString(R.string.aboutstopit)+".html");
                        break;
                    default:
                        viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/"+ getString(R.string.aboutstopit)+".html");
                        break;
                }
                AppHelpActivity.this.startActivity(viewHelpIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //show popup for inbox messages
        updateInboxCounts(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }
}
