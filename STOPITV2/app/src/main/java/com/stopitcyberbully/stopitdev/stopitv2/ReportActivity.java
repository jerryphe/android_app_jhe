package com.stopitcyberbully.stopitdev.stopitv2;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.ImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.ReportContactAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.Contact;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.IncidentObject;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class ReportActivity extends AppBaseActivity {
private static final int LOAD_IMAGES = 1;
    private ArrayList <IncidentObject> incidentObjects;
    private ImageListAdapter imageAdapter;
    private GridView listView;
    private String incidentID;
    private EditText incidentNotes;
    private TextWatcher incidentWatcher;
    private CheckBox checkBox;
    private TextView sendButtonText;
    private CrownMolding bottomSendButton;

    private MenuItem sendMenu;
    private MenuItem disabledSendMenu;

    private final boolean hideAnonymous = true;
    private final boolean hideReportReciepients = true;



    @Override
    protected void onPause() {
        super.onPause();
        incidentNotes.removeTextChangedListener(incidentWatcher);
    }

    @Override
    protected void onResume() {
        super.onResume();
        incidentNotes.addTextChangedListener(incidentWatcher);
        //get contacts for the current user.
        if (RemoteDataManager.getUserContacts() != null) {
            if (RemoteDataManager.getUserContacts().getTrustedContacts() != null) {
                ListView contactListView = (ListView) findViewById(R.id.list_report_contacts);
                ReportContactAdapter reportAdapter = new ReportContactAdapter(this, RemoteDataManager.getUserContacts().getTrustedContacts());
                contactListView.setAdapter(reportAdapter);
                if (hideReportReciepients)
                {
                    if (contactListView != null)
                    {
                        contactListView.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }

    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //do nothing. to avoid rotation
    }
    @Override
    public void onBackPressed() {
        goBack();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        checkBox = (CheckBox) findViewById(R.id.anonymous_checkbox);
        checkBox.setChecked(true);
        if (hideAnonymous)
        {
            checkBox.setVisibility(View.INVISIBLE);
        }else{
            checkBox.setVisibility(View.VISIBLE);
        }

        CustomFontHelper.setCustomFont(checkBox, "source-sans-pro.semibold.ttf", this);



        sendButtonText = (TextView) findViewById(R.id.send_button_bottom_text);
        CustomFontHelper.setCustomFont(sendButtonText, "source-sans-pro.semibold.ttf", this);


        TextView sendReportTo = (TextView) findViewById(R.id.send_report_to);
        CustomFontHelper.setCustomFont(sendReportTo, "source-sans-pro.semibold.ttf", this);

        bottomSendButton = (CrownMolding) findViewById(R.id.send_button_bottom);

        if (bottomSendButton != null) {
            bottomSendButton.setEnabled(false);

            bottomSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refreshRegistrationPriorToIncident();
                }
            });
        }

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_REPORT_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_REPORT_SCREEN);

        sendReportTo.setText(getResources().getString(R.string.title_report_recipients));

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!checkBox.isChecked()) {
                    //show alert
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(checkBox.getContext());
                    anonymousAlert.setTitle(R.string.ARE_YOU_SURE);
                    anonymousAlert.setMessage(getResources().getString(R.string.prompt_remove_anonymous));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.OK),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_REPORT_CHANGED_REPORT_ANONYMOUSLY_SWITCH, AnalyticsManager.ANALYTICS_ACTION_REPORT_CHANGED_REPORT_ANONYMOUSLY_SWITCH);

                                    dialog.cancel();
                                }
                            });
                    anonymousAlert.setNegativeButton(getResources().getString(R.string.CANCEL),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    checkBox.setChecked(true);
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                } else {
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_REPORT_CHANGED_REPORT_ANONYMOUSLY_SWITCH, AnalyticsManager.ANALYTICS_ACTION_REPORT_CHANGED_REPORT_ANONYMOUSLY_SWITCH);

                }
            }
        });


        TextView txtReport = (TextView) findViewById(R.id.header_report_to);
        CustomFontHelper.setCustomFont(txtReport, "source-sans-pro.semibold.ttf", this);


        TextView imgPicketLabel = (TextView) findViewById(R.id.image_picker_label);
        CustomFontHelper.setCustomFont(imgPicketLabel, "source-sans-pro.semibold.ttf", this);

        if (imgPicketLabel != null)
        {
            imgPicketLabel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPicker();
                }
            });
        }

        txtReport.setText(getResources().getString(R.string.title_incident_report_k12));

        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE){
            bottomSendButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
            txtReport.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
            sendReportTo.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
            imgPicketLabel.setTextColor(ContextCompat.getColor(this, R.color.pro_app_text_maroon));
            checkBox.setTextColor(ContextCompat.getColor(this, R.color.pro_app_text_maroon));
        }else{
            sendReportTo.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
        }


        incidentNotes = (EditText) findViewById(R.id.txt_incident_notes);
        CustomFontHelper.setCustomFont(incidentNotes, "source-sans-pro.semibold.ttf", this);


        final CrownMolding imageButton = (CrownMolding) findViewById(R.id.image_picker);

        listView = (GridView) findViewById(R.id.grid_of_incident_images);

        if (savedInstanceState == null) {
            //If it is brand new activity, then we need not initialize the arraylist
            RemoteDataManager.setIncidentObjects(new ArrayList<IncidentObject>());
        }
            incidentObjects = RemoteDataManager.getIncidentObjects();

            imageAdapter = new ImageListAdapter(this, R.layout.imagecells, incidentObjects);

            if (imageAdapter.getCount() > 0) {
                listView.setAdapter(imageAdapter);
            }

        if (RemoteDataManager.getRegistration().getOrg().getIsMediaUploadDisabled())
        {
            imageButton.setVisibility(View.INVISIBLE);
            if (imgPicketLabel != null)
            {
                imgPicketLabel.setVisibility(View.INVISIBLE);
            }
            ImageView imageView = (ImageView) findViewById(R.id.image_picker_camera);
            if (imageView != null)
            {
                imageView.setVisibility(View.INVISIBLE);
            }
        }

        imageAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if ((incidentNotes.getText().toString().trim().length()>0) || (imageAdapter.getCount()>0)) {
                    changeSendMenuState(true);
                }else{
                    changeSendMenuState(false);
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(listView.getContext());
                deleteBuilder.setMessage(getResources().getString(R.string.question_remove_image));
                deleteBuilder.setCancelable(true);
                deleteBuilder.setPositiveButton(getResources().getString(R.string.YES),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ALERT_DELETED_MEDIA_AFTER_ATTACHING, AnalyticsManager.ANALYTICS_ACTION_ALERT_DELETED_MEDIA_AFTER_ATTACHING);

                                incidentObjects.remove(position);
                                imageAdapter.notifyDataSetChanged();
                                dialog.cancel();
                            }
                        });
                deleteBuilder.setNegativeButton(getResources().getString(R.string.NO),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog deleteAlert = deleteBuilder.create();
                deleteAlert.show();
            }
        });

        if (hideReportReciepients)
        {
            if (sendReportTo != null)
            {
                sendReportTo.setVisibility(View.INVISIBLE);
            }
        }

        if (imageButton != null) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPicker();
                }
            });
        }

        if (RemoteDataManager.getSavedOrgType(this)==RemoteDataManager.ORG_TYPE_WORKPLACE)
        {
            imageButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
        }

        //upon load the incident transmission will be disabled, till user selects at least one image or adds some notes.
        changeSendMenuState(false);

        incidentWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if ((incidentNotes.getText().toString().trim().length()>0) || (imageAdapter.getCount()>0)) {
                    changeSendMenuState(true);
                }else{
                    changeSendMenuState(false);
                }
            }
        };

        incidentNotes.addTextChangedListener(incidentWatcher);

        ImageButton access_help = (ImageButton) findViewById(R.id.access_help);
        if (access_help != null){
            access_help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHelp();
                }
            });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case LOAD_IMAGES:
            {
                if (resultCode == RESULT_OK) {

                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ATTACHED_MEDIA, AnalyticsManager.ANALYTICS_ACTION_ATTACHED_MEDIA);

                    // Get resource path
                    Uri imageUri = data.getData();

                    //If we do not get images in import, break and return early.
                    if (imageUri == null) break;

                    IncidentObject incidentObject = new IncidentObject();
                    if (imageUri.toString().startsWith("content://com.google.android.apps.photos.content"))
                    {

                        try {
                            incidentObject.saveImageURIToFile(this, imageUri);

                        }catch (FileNotFoundException fnfe)
                        {

                            AlertDialog.Builder importMediaBuider = new AlertDialog.Builder(ReportActivity.this);
                            importMediaBuider.setMessage(getResources().getString(R.string.media_import_failed));

                            importMediaBuider.setCancelable(true);
                            importMediaBuider.setPositiveButton(getResources().getString(R.string.OK),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog successDialog = importMediaBuider.create();
                            successDialog.show();
                            //We could not import image. We need to break quickly
                            break;
                        }

                    }else {
                        incidentObject.setUri(imageUri);
                    }
                    long total_size;
                    try {
                        if (RemoteDataManager.SHOW_LOG) Log.d(ReportActivity.class.getName(), "The file size is : " + incidentObject.getFileSize(this));

                         total_size = incidentObject.getFileSize(this);
                        for (IncidentObject iObj : incidentObjects) {
                            total_size += iObj.getFileSize(this);
                        }
                    }catch (FileNotFoundException fnfe)
                    {
                        AlertDialog.Builder importMediaBuider = new AlertDialog.Builder(ReportActivity.this);
                        importMediaBuider.setMessage(getResources().getString(R.string.media_import_failed));

                        importMediaBuider.setCancelable(true);
                        importMediaBuider.setPositiveButton(getResources().getString(R.string.OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog successDialog = importMediaBuider.create();
                        successDialog.show();
                        //We could not import image. We need to break quickly
                        break;
                    }

                    //Ensure that we have not exceeded the maximum attachment size
                    if (total_size <= RemoteDataManager.MAX_TOTAL_FILE_SIZE)
                    {
                        incidentObjects.add(incidentObject);
                        if (imageAdapter.getCount() > 0) {
                            listView.setAdapter(imageAdapter);
                            changeSendMenuState(true);
                        }else{
                            if ((incidentNotes.getText().length()>0) || (imageAdapter.getCount()>0)) {
                                changeSendMenuState(true);
                            }else{
                                changeSendMenuState(false);
                            }
                        }
                        imageAdapter.notifyDataSetChanged();
                        listView.scrollBy(0, 0);
                    }else{
                        AlertDialog.Builder maxSizeBuider = new AlertDialog.Builder(ReportActivity.this);

                        maxSizeBuider.setMessage(getResources().getString(R.string.max_attachment_size_exceeded));

                        maxSizeBuider.setCancelable(true);
                        maxSizeBuider.setPositiveButton(getResources().getString(R.string.OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog maxSizeDialog = maxSizeBuider.create();
                        maxSizeDialog.show();
                    }


                }else{
                    //Result code other than RESULT_OK
                    AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CANCELED_ATTACHING_MEDIA, AnalyticsManager.ANALYTICS_ACTION_CANCELED_ATTACHING_MEDIA);

                }
            }
                break;
            default:
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CANCELED_ATTACHING_MEDIA, AnalyticsManager.ANALYTICS_ACTION_CANCELED_ATTACHING_MEDIA);

        }

    }

    //Create the incident if incident creation is successful, stream the media upload.
    private void createIncident(final JSONObject incidentObject)
    {
        showProgress(getResources().getString(R.string.prompt_report_incident));
        RequestQueue queue = RemoteDataManager.sharedManager(this);
        AuthRequest incidentRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getINCIDENT_CREATION_URL(), incidentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("data"))
                {
                    try {
                        JSONObject dataResponse = jsonObject.getJSONObject("data");


                        if (dataResponse.has("incident_id"))
                        {
                            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_INCIDENT_REPORT, AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_INCIDENT_REPORT);

                            String afterHours = null;
                            if (dataResponse.has("after_hour_report_message")) {
                                afterHours = dataResponse.getString("after_hour_report_message");
                            }

                            incidentID = dataResponse.getString("incident_id");
                            if (RemoteDataManager.sendImages(ReportActivity.this, incidentID, imageAdapter))
                            {
                                if (afterHours == null)
                                {
                                    afterHours = ReportActivity.this.getString(R.string.media_upload_failed);
                                }else {
                                    afterHours = afterHours.concat(ReportActivity.this.getString(R.string.media_upload_failed));
                                }
                            }
                            showSuccessDialog(afterHours);

                        }
                    }catch (JSONException je)
                    {
                        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_INCIDENT_REPORT_FAILED, AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_INCIDENT_REPORT_FAILED);

                        //Show retry option
                        showRetryDialog(incidentObject);
                    }


                }else{
                    //Show retry option
                    showRetryDialog(incidentObject);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //Show retry option
                showRetryDialog(incidentObject);
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(incidentRequest);
    }


    private void showRetryDialog(final JSONObject jsonObject)
    {
        progressDialog.dismiss();
        AlertDialog.Builder errorBuilder = new AlertDialog.Builder(ReportActivity.this);
        errorBuilder.setMessage(getResources().getString(R.string.prompt_retry_incident_submission));
        errorBuilder.setCancelable(false);
        errorBuilder.setPositiveButton(getResources().getString(R.string.YES),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        createIncident(jsonObject);
                        dialog.cancel();
                    }
                });
        errorBuilder.setNegativeButton(getResources().getString(R.string.NO),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog errorDialog = errorBuilder.create();
        errorDialog.show();
    }
    private void showSuccessDialog(String afterHoursMessage)
    {
        progressDialog.dismiss();
        AlertDialog.Builder successBuider = new AlertDialog.Builder(ReportActivity.this);
        successBuider.setCancelable(true);


        if (afterHoursMessage == null) {
            successBuider.setMessage(getResources().getString(R.string.prompt_success_incident_submission));

        }else {
            successBuider.setMessage(afterHoursMessage);
            successBuider.setPositiveButton(getResources().getString(R.string.OK),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    });

        }
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (!ReportActivity.this.isDestroyed()) {
                final AlertDialog successDialog = successBuider.create();

                successDialog.show();

                if (afterHoursMessage == null) {
                    final Timer t = new Timer();
                    t.schedule(new TimerTask() {
                        public void run() {
                            successDialog.dismiss();
                            finish();
                            t.cancel();
                        }
                    }, 2000);
                }
            }
        }else{
            final AlertDialog successDialog = successBuider.create();

            successDialog.show();

            if (afterHoursMessage == null) {
                final Timer t = new Timer();
                t.schedule(new TimerTask() {
                    public void run() {
                        successDialog.dismiss();
                        finish();
                        t.cancel();
                    }
                }, 2000);
            }
        }
    }
    private void showContactDialog()
    {
        AlertDialog.Builder noContactsBuilder = new AlertDialog.Builder(ReportActivity.this);
        noContactsBuilder.setMessage(getResources().getString(R.string.prompt_trusted_adult_selection));
        noContactsBuilder.setCancelable(true);
        noContactsBuilder.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog successDialog = noContactsBuilder.create();
        successDialog.show();
    }

    private void goBack(){
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CANCELLED_REPORTING_INCIDENT, AnalyticsManager.ANALYTICS_ACTION_CANCELLED_REPORTING_INCIDENT);

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        if (RemoteDataManager.getSavedOrgType(this)==RemoteDataManager.ORG_TYPE_WORKPLACE)
        {
            inflater.inflate(R.menu.pro_menu_report, menu);
        }else {
            inflater.inflate(R.menu.menu_report, menu);
        }
        sendMenu = menu.findItem(R.id.action_button_send);
        disabledSendMenu = menu.findItem(R.id.action_button_send_disabled);


        getMenuInflater().inflate(R.menu.menu_app_base, menu);


        changeSendMenuState(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_button_send: {
                    int selected_contacts = 0;
                //If there are any contacts, count them
                if (RemoteDataManager.getUserContacts().getTrustedContacts() != null) {
                    for (Contact contact : RemoteDataManager.getUserContacts().getTrustedContacts()) {
                        if (contact.isSelected()) {
                            selected_contacts ++;
                        }
                    }
                }

                //If contacts are not selected,
                if (selected_contacts == 0) {
                    showContactDialog();
                    break;
                }
                refreshRegistrationPriorToIncident();
            }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);

    }


    private void changeSendMenuState (Boolean state)
    {
        if (sendMenu  != null)
        {
            sendMenu.setVisible(state);
            disabledSendMenu.setVisible(!state);
            bottomSendButton.setEnabled(state);
            sendButtonText.setEnabled(state);
        }
    }

    private void prepareIncident()
    {
        try {

            JSONArray contactsArray = new JSONArray();

            //If there are any contacts, count them
            if (RemoteDataManager.getUserContacts().getTrustedContacts() != null) {
                for (Contact contact : RemoteDataManager.getUserContacts().getTrustedContacts()) {
                    if (contact.isSelected()) {
                        JSONObject contactsObject = new JSONObject();
                        contactsObject.put("user_id", contact.getUser_id());
                        contactsArray.put(contactsObject);
                    }
                }
            }

                //If contacts are not selected,
                if (contactsArray.length() == 0) {
                    showContactDialog();
                    return;
                }

            JSONObject incidentJSON = new JSONObject();
            JSONObject dataJSON = new JSONObject();
            dataJSON.put("data", incidentNotes.getText());
            if (incidentNotes.length()>0)
            {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ADDED_NOTE, AnalyticsManager.ANALYTICS_ACTION_ADDED_NOTE);
            }

            JSONArray dataArray = new JSONArray();
            dataArray.put(dataJSON);

            if (checkBox.isChecked()) {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_REPORT_ANONYMOUSLY, AnalyticsManager.ANALYTICS_ACTION_REPORT_ANONYMOUSLY);

                incidentJSON.put("is_anonymous", "1");
            } else {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_REPORT_NON_ANONYMOUSLY, AnalyticsManager.ANALYTICS_ACTION_REPORT_NON_ANONYMOUSLY);

                incidentJSON.put("is_anonymous", "0");
            }

            incidentJSON.put("incident_category_id", Integer.toString(RemoteDataManager.INCIDENT_CATEGORY_REGULAR));
            incidentJSON.put("notes", dataArray);
            incidentJSON.put("contacts", contactsArray);

            JSONObject submitJSON = new JSONObject();
            if (RemoteDataManager.getEncryptionSalt() != null)
            {
                String encryptedString = RemoteDataManager.encrypt(RemoteDataManager.getEncryptionSalt(), incidentJSON.toString());
                submitJSON.put("data", encryptedString);

            }else {
                submitJSON.put("data", incidentJSON);
            }


            createIncident(submitJSON);
        } catch (Exception je) {
            if (RemoteDataManager.SHOW_LOG) Log.e("ReportActivity", je.toString());
        }


    }

    private void refreshRegistrationPriorToIncident()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {
                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    progressDialog.dismiss();

                                    if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                        String tag = "recess_dialog";
                                        DialogFragment recessFragment =
                                                RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                        recessFragment.show(getSupportFragmentManager(), tag);
                                    } else {

                                        prepareIncident();
                                    }
                                }
                            } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(ReportActivity.this, BlockedGateActivity.class);
                                ReportActivity.this.startActivity(blockedIntent);
                            }
                        } catch (JSONException je) {
                            progressDialog.dismiss();
                            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ReportActivity.this);
                            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                            anonymousAlert.setCancelable(false);
                            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            progressDialog.dismiss();
                                            refreshRegistrationPriorToIncident();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = anonymousAlert.create();
                            alert.show();

                        }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ReportActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    refreshRegistrationPriorToIncident();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }

    //region Marshmallow permissions related
    private void showStoragePermissions(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ReportActivity.this);
            anonymousAlert.setMessage(getResources().getString(R.string.photo_settings));
            anonymousAlert.setPositiveButton(getResources().getString(R.string.OK),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        } else {

            // Storage permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(ReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CAMERA_CLICKED_FOR_MEDIA, AnalyticsManager.ANALYTICS_ACTION_CAMERA_CLICKED_FOR_MEDIA);

            Intent imagePickerIntent = new Intent(Intent.ACTION_PICK);
            imagePickerIntent.setType("image/*, video/*");
            startActivityForResult(imagePickerIntent, LOAD_IMAGES);
        }
    }
    //endregion
    private void showHelp(){
//        Intent viewHelpIntent = new Intent(ReportActivity.this, HelpViewActivity.class);
//        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_REPORTING_INCIDENT_IN_HELP, AnalyticsManager.ANALYTICS_ACTION_OPENED_REPORTING_INCIDENT_IN_HELP);
//        viewHelpIntent.putExtra("HELP_URL", "file:///android_asset/"+ getString(R.string.helpreportingincident)+".html");
//        ReportActivity.this.startActivity(viewHelpIntent);
        Intent appHelpIntent = new Intent(this, AppHelpActivity.class);
        ReportActivity.this.startActivity(appHelpIntent);

    }
    private void showPicker()
    {
        if (ActivityCompat.checkSelfPermission(ReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CAMERA_CLICKED_FOR_MEDIA, AnalyticsManager.ANALYTICS_ACTION_CAMERA_CLICKED_FOR_MEDIA);

            Intent imagePickerIntent = new Intent(Intent.ACTION_PICK);
            imagePickerIntent.setType("image/*;video/*");
            startActivityForResult(imagePickerIntent, LOAD_IMAGES);
        } else {
            showStoragePermissions();
        }
    }
}
