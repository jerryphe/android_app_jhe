package com.stopitcyberbully.stopitdev.stopitv2;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;


public class DeclineResultActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setContentView(R.layout.activity_decline_result);

        TextView textView = (TextView) findViewById(R.id.decline_tos_result);
        CustomFontHelper.setCustomFont(textView, "source-sans-pro.semibold.ttf", this);


        Button backButton = (Button) findViewById(R.id.back_button);
        CustomFontHelper.setCustomFont(backButton, "source-sans-pro.semibold.ttf", this);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_BACKBUTTON_AFTER_DECLINE_PRIVACY, AnalyticsManager.ANALYTICS_ACTION_BACKBUTTON_AFTER_DECLINE_PRIVACY);

                finish();
            }
        });

    }

}
