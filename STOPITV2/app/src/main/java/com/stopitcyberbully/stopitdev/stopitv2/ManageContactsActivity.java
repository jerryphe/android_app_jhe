package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.GenericImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.SimpleContactListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;


public class ManageContactsActivity extends AppBaseActivity {

    private SimpleContactListAdapter contactListAdapter;
    private ListView contactList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_contacts);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!= null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        TextView manageContactsTitle = (TextView) findViewById(R.id.title_manage_contacts);
        CustomFontHelper.setCustomFont(manageContactsTitle, "source-sans-pro.semibold.ttf", this);


        final ListView addNewListView = (ListView) findViewById(R.id.contact_addnew_listview);
        String[] values = new String[] {
                getResources().getString(R.string.add_new_contact),
        };

        final ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, values);
        Integer[] imageId = {R.drawable.plus_white};
        GenericImageListAdapter adapter = new GenericImageListAdapter(this, list, imageId);

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_MANAGE_CONTACTS_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_MANAGE_CONTACTS_SCREEN);

        addNewListView.setAdapter(adapter);

        addNewListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent addContactIntent = new Intent(ManageContactsActivity.this, EditContactActivity.class);
                RemoteDataManager.setCurrentContact(null);
                ManageContactsActivity.this.startActivity(addContactIntent);
            }
        });


        //Check if this is a workplace user, we will have to change back color for them
        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {

            View bgView = findViewById(R.id.background_gradient_view);
            if (bgView != null) {
                bgView.setBackgroundResource(R.drawable.pro_onboarding_selector);
            }

            manageContactsTitle.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));

        }


        Integer[] contactImageID = {R.drawable.user_white};

        contactListAdapter = new SimpleContactListAdapter(this, RemoteDataManager.getUserContacts().getNonOrgContacts(), contactImageID);
        contactList = (ListView) findViewById(R.id.contact_selection_listview);
        contactList.setAdapter(contactListAdapter);

        contactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent addContactIntent = new Intent(ManageContactsActivity.this, EditContactActivity.class);
                RemoteDataManager.setCurrentContact(contactListAdapter.getItem(position));
                startActivityForResult(addContactIntent, 1);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Intent refresh = new Intent(this, ManageContactsActivity.class);
            startActivity(refresh);
            this.finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserContacts();
        //show popup for inbox messages
        updateInboxCounts(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }

    private void getUserContacts () {
        RemoteDataManager.getContacts(this, new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject o) {
                Integer[] contactImageID = {R.drawable.user_white};

                contactListAdapter = new SimpleContactListAdapter(ManageContactsActivity.this, RemoteDataManager.getUserContacts().getNonOrgContacts(), contactImageID);
                contactList.setAdapter(contactListAdapter);
                contactListAdapter.notifyDataSetChanged();
                if (RemoteDataManager.SHOW_LOG) Log.d("ManageContactsActivity", "Received contacts");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (RemoteDataManager.SHOW_LOG) Log.d("ManageContactsActivity", volleyError.toString());
            }
        });

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
