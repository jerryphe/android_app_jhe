package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.SmackService;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;


public class GetHigherEdHelpActivity extends AppBaseActivity {

    private TextView unreadCount;
    private AlertDialog alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_higher_ed_help);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        CrownMolding panicButton  = (CrownMolding) findViewById(R.id.button_panic);

        panicButton.setMode(CrownMolding.CROWN_MOLDING_MODE.REPORT_MOLDING);

        CrownMolding helpButton  = (CrownMolding) findViewById(R.id.button_help);
        helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);


        TextView textPanicTitle = (TextView) findViewById(R.id.txt_panic_title);
        CustomFontHelper.setCustomFont(textPanicTitle, "source-sans-pro.semibold.ttf", this);

        TextView textConnectpTitle = (TextView) findViewById(R.id.txt_connect_title);
        CustomFontHelper.setCustomFont(textConnectpTitle, "source-sans-pro.semibold.ttf", this);

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_HIGHER_ED_HELP_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_HIGHER_ED_HELP_SCREEN);



        unreadCount = (TextView)findViewById(R.id.unread_count);






        panicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                    String tag = "recess_dialog";
                    DialogFragment recessFragment =
                            RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                    recessFragment.show(getSupportFragmentManager(), tag);
                }else {
                    gotoPanicScreen();
                }
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoHelpScreen();
            }
        });

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(SmackService.NEW_MESSAGE);
        filter.addAction(RemoteDataManager.INBOX_NOTIFICATION_BROADCAST_ID);
        registerReceiver(receiver, filter);

        refreshUnreadCount();
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        if (alert != null) alert.cancel();
        unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshUnreadCount();
        }
    };

    private void refreshUnreadCount() {

        if (unreadCount == null)
        {
            unreadCount = (TextView)findViewById(R.id.unread_count);
        }

        if (unreadCount != null) {
            if (RemoteDataManager.getUnreadMessageCount(this) > 0) {
                unreadCount.setText(getResources().getString(R.string.inbox_count,RemoteDataManager.getUnreadMessageCount(this)));
                unreadCount.setVisibility(View.VISIBLE);
                showMessengerDialog();
            } else {
                unreadCount.setText("");
                unreadCount.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void showMessengerDialog(){
        if( (alert != null) && (alert.isShowing()) )
        {
            return;
        }
        AlertDialog.Builder messengerAlert = new AlertDialog.Builder(GetHigherEdHelpActivity.this);
        messengerAlert.setMessage(getResources().getString(R.string.waiting_message_prompt));
        messengerAlert.setCancelable(false);
        messengerAlert.setPositiveButton(getResources().getString(R.string.waiting_message_prompt_read),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        gotoChatScreen();

                    }
                });
        messengerAlert.setNegativeButton(getResources().getString(R.string.waiting_message_prompt_close),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alert = messengerAlert.create();
        alert.show();

    }

    private void gotoChatScreen(){
        if (RemoteDataManager.isConnected(this)){
            Intent chatIntent = new Intent(GetHigherEdHelpActivity.this, ChatActivity.class);
            GetHigherEdHelpActivity.this.startActivity(chatIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoChatScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void gotoPanicScreen(){
        if (RemoteDataManager.isConnected(this)){
            Intent panicIntent = new Intent(GetHigherEdHelpActivity.this, PanicMapActivity.class);
            panicIntent.putExtra("INCIDENT_TYPE", RemoteDataManager.INCIDENT_CATEGORY_PANIC);
            startActivityForResult(panicIntent, 1);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoPanicScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void gotoHelpScreen(){
        if (RemoteDataManager.isConnected(this)){
            Intent helpIntent = new Intent(GetHigherEdHelpActivity.this, GetHelpActivity.class);
            GetHigherEdHelpActivity.this.startActivity(helpIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoHelpScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
}
