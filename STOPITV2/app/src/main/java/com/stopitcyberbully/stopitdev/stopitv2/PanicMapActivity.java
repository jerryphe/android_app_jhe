package com.stopitcyberbully.stopitdev.stopitv2;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class PanicMapActivity extends FragmentActivity implements LocationListener{
    private GoogleMap googleMap;
    private double old_latitude = 0.0f;
    private double old_longitude = 0.0f;
    private String incidentID;
    private TextView notificationText;
    private TextView address_text;
    private LocationManager locationManager;


    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(PanicMapActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {

            if (locationManager != null) {
                locationManager.removeUpdates(this);
            }
        }
    }
    @Override
    public void onBackPressed() {
        //ask user if they want to go back
        askQuestion();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (incidentID != null) {
            outState.putString("CURRENT_INCIDENT_ID", incidentID);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        //check if we have old incident id prior to rotation
        if (savedInstanceState != null) {
            incidentID = savedInstanceState.getString("CURRENT_INCIDENT_ID");
        }

        notificationText = (TextView) findViewById(R.id.crisis_center_status);
        CustomFontHelper.setCustomFont(notificationText, "source-sans-pro.semibold.ttf", this);

        address_text = (TextView) findViewById(R.id.user_address);
        CustomFontHelper.setCustomFont(address_text, "source-sans-pro.semibold.ttf", this);

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_PANIC_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_PANIC_SCREEN);



        TextView cancelText = (TextView) findViewById(R.id.panic_cancel_text);
        CustomFontHelper.setCustomFont(cancelText, "source-sans-pro.semibold.ttf", this);

        CrownMolding cancelButton = (CrownMolding) findViewById(R.id.cancel_button);




        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askQuestion();
            }
        });



        if (ActivityCompat.checkSelfPermission(PanicMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            showLocationPermissions();
        }else {
            startLocationUpdates();
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);

        if (googleMap != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom( new LatLng(location.getLatitude(), location.getLongitude()), 13));

            if ((old_latitude != 0.0f) && (old_longitude != 0.0f)) {
                googleMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(old_latitude, old_longitude), latLng)
                        .width(5)
                        .color(Color.parseColor("#005AFF")));
            }
        }
        old_latitude = latitude;
        old_longitude = longitude;


        List<Address> addresses;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size()>0)
            {
                String building = addresses.get(0).getAddressLine(0);
                building = (building == null) ? "" : building + ", ";

                String stateName= addresses.get(0).getAdminArea();
                stateName = (stateName == null) ? "" : stateName + ", ";

                String cityName = addresses.get(0).getLocality();
                cityName = (cityName == null) ? "" : cityName + ", ";

                String countryName = addresses.get(0).getCountryName();
                countryName = (countryName == null) ? "" : countryName ;

                String address = building + cityName + stateName + countryName;
                address_text.setText("Your Location " + address);
                prepapreIncident(location, address);

            }
        }catch (IOException ioe)
        {
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_MAP_ADDRESS_LOOKUP_FAILED, AnalyticsManager.ANALYTICS_ACTION_MAP_ADDRESS_LOOKUP_FAILED);

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
                AlertDialog.Builder nopanic_chaperone = new AlertDialog.Builder(PanicMapActivity.this);
                nopanic_chaperone.setMessage(getResources().getString(R.string.map_not_available));
                nopanic_chaperone.setCancelable(true);
                nopanic_chaperone.setNegativeButton(getResources().getString(R.string.OK),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                nopanic_chaperone.show();
            return false;
        }
    }
    private void prepapreIncident(Location location, String address)
    {

        try {
            JSONObject incidentJSON = new JSONObject();
            JSONObject dataJSON = new JSONObject();
            dataJSON.put("data", address);

            JSONArray dataArray = new JSONArray();
            dataArray.put(dataJSON);
            incidentJSON.put("is_anonymous", "0");

            incidentJSON.put("incident_category_id", Integer.toString( this.getIntent().getIntExtra("INCIDENT_TYPE", RemoteDataManager.INCIDENT_CATEGORY_PANIC)));
            incidentJSON.put("notes", dataArray);

            JSONObject coordinates = new JSONObject();
            coordinates.put("longitude",  Double.toString(location.getLongitude()));
            coordinates.put("latitude", Double.toString(location.getLatitude()));
            incidentJSON.put("coordinates", coordinates);

            JSONObject submitJSON = new JSONObject();
            if (RemoteDataManager.getEncryptionSalt() != null)
            {
                String EncryptedIncident = RemoteDataManager.encrypt(RemoteDataManager.getEncryptionSalt(), incidentJSON.toString());
                submitJSON.put("data", EncryptedIncident);
            }else {
                submitJSON.put("data", incidentJSON);
            }
            if (incidentID == null)
            {
                createIncident(submitJSON);
            }else{
                updateIncident(submitJSON, incidentID);
            }
        }catch (JSONException je){
            notificationText.setText(getResources().getText(R.string.notification_failure));
            if (RemoteDataManager.SHOW_LOG) Log.e("PanicMapActivity", je.toString());
        }catch (Exception e)
        {
            notificationText.setText(getResources().getText(R.string.notification_failure));
            if (RemoteDataManager.SHOW_LOG) Log.e("PanicMapActivity", e.toString());
        }
    }

    //Create the incident if incident creation is successful, stream the media upload.
    private void createIncident(final JSONObject incidentObject)
    {
        RequestQueue queue = RemoteDataManager.sharedManager(this);
        AuthRequest incidentRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getINCIDENT_CREATION_URL(), incidentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("data"))
                {
                    try {
                        JSONObject dataResponse = jsonObject.getJSONObject("data");
                        if (dataResponse.has("incident_id"))
                        {
                            incidentID = dataResponse.getString("incident_id");
                            notificationText.setText(getResources().getText(R.string.notification_success));
                        }
                    }catch (JSONException je)
                    {
                        notificationText.setText(getResources().getText(R.string.notification_failure));
                    }


                }else{
                    notificationText.setText(getResources().getText(R.string.notification_failure));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //Show retry option
                notificationText.setText(getResources().getText(R.string.notification_failure));
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(incidentRequest);
    }
    private void updateIncident(final JSONObject incidentObject, String incidentID)
    {

        RequestQueue queue = RemoteDataManager.sharedManager(this);
        String incidentUpdateURL = RemoteDataManager.getINCIDENT_CREATION_URL() + "/" + incidentID;
        AuthRequest incidentRequest = new AuthRequest(Request.Method.PUT, incidentUpdateURL, incidentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("data"))
                {
                    notificationText.setText(getResources().getText(R.string.notification_success));
                }else{
                    notificationText.setText(getResources().getText(R.string.notification_failure));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                notificationText.setText(getResources().getText(R.string.notification_failure));
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(incidentRequest);
    }
    private void askQuestion()
    {
        AlertDialog.Builder goBackBuilder = new AlertDialog.Builder(PanicMapActivity.this);
        goBackBuilder.setMessage(getResources().getString(R.string.panic_go_back));
        goBackBuilder.setCancelable(false);
        goBackBuilder.setPositiveButton(getResources().getString(R.string.YES),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        goBack();
                        dialog.cancel();
                    }
                });
        goBackBuilder.setNegativeButton(getResources().getString(R.string.NO),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog backDialog = goBackBuilder.create();
        backDialog.show();
    }
    private void goBack(){
        if (ActivityCompat.checkSelfPermission(PanicMapActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {

            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CANCELED_PANIC_INCIDENT_REPORTING, AnalyticsManager.ANALYTICS_ACTION_CANCELED_PANIC_INCIDENT_REPORTING);
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            }
        }
        super.onBackPressed();
    }

    private void showLocationPermissions(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(PanicMapActivity.this);
            anonymousAlert.setMessage(getResources().getString(R.string.location_settings));
            anonymousAlert.setPositiveButton(getResources().getString(R.string.OK),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        } else {

            // Storage permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        }
    }

    private void startLocationUpdates(){
        if (ActivityCompat.checkSelfPermission(PanicMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

            SupportMapFragment supportMapFragment =
                    (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            googleMap = supportMapFragment.getMap();
            if (googleMap != null) {
                googleMap.setMyLocationEnabled(true);
            }
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5 * 60 * 1000, 0, this);


                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    onLocationChanged(location);
                }
            } catch (IllegalArgumentException iae) {
                if (RemoteDataManager.SHOW_LOG) Log.e("PanicActivity", iae.getMessage());
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_APP_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_MAP_ADDRESS_LOOKUP_FAILED, AnalyticsManager.ANALYTICS_ACTION_MAP_ADDRESS_LOOKUP_FAILED);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (ActivityCompat.checkSelfPermission(PanicMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {
            startLocationUpdates();
        }

    }
}
