package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;


/**
 * Object model to store data related to Registration information obtained from webservice (device registration/ roster)
 */
public class Registration {
    private User user;
    private Organization org;
    private CrisisCenter crisisCenter;
    private Chat chat;
    private AWS aws;



    public AWS getAws() {
        return aws;
    }

    public void setAws(AWS aws) {
        this.aws = aws;
    }



    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }

    public CrisisCenter getCrisisCenter() {
        return crisisCenter;
    }

    public void setCrisisCenter(CrisisCenter crisisCenter) {
        this.crisisCenter = crisisCenter;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }


}
