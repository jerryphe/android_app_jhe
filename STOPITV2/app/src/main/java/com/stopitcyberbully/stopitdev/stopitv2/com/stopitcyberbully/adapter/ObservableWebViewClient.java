package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.lang.ref.WeakReference;

/**
 * Custom WebView  to handle enabling/ disabling buttons
 */
class ObservableWebViewClient extends WebViewClient {
    private final WeakReference<Activity> mActivityRef;

    public ObservableWebViewClient(Activity activity) {
        mActivityRef = new WeakReference<>(activity);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.toLowerCase().startsWith("mailto:")) {
            final Activity activity = mActivityRef.get();
            if (activity != null) {
                try {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(activity, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                    activity.startActivity(i);
                    view.reload();
                } catch (Exception e) {
                    return false;
                }
                return true;
            }
        }else if (url.toLowerCase().startsWith("tel:")) {
            final Activity activity = mActivityRef.get();
            if (activity != null) {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(url));
                    activity.startActivity(intent);
                    view.reload();
                } catch (Exception e) {
                    return false;
                }
                return true;
            }
        } else {
            view.loadUrl(url);
        }
        return true;
    }

    private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
    }
}
