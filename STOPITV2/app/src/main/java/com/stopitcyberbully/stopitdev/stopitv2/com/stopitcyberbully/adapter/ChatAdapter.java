package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stopitcyberbully.stopitdev.stopitv2.R;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.ChatMessage;

import java.util.ArrayList;

/**
 * Custom adapter to show the chat messages.
 */
public class ChatAdapter extends ArrayAdapter<ChatMessage>

{

    private final ArrayList<ChatMessage> web;


    public ChatAdapter(Activity context, ArrayList<ChatMessage> labels, Integer[] imageId) {
        super(context, R.layout.tablecell);
        this.web =labels;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();
        if (web.get(position).isIncoming()) {
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.chat_message_incoming, parent, false);
            LinearLayout incomingLayout = (LinearLayout) convertView.findViewById(R.id.chat_incoming_layout);
            if (incomingLayout != null)
            {
                if (RemoteDataManager.getSavedOrgType(this.getContext())==RemoteDataManager.ORG_TYPE_WORKPLACE) {
                    incomingLayout.setBackgroundResource(R.drawable.pro_chat_incoming_background);
                    View arrow = convertView.findViewById(R.id.incoming_arrow);
                    if (arrow != null)
                    {
                        arrow.setBackgroundResource(R.drawable.pro_incoming_arrow);
                    }

                }
            }
        }else{
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.chat_message_outgoing, parent, false);
            LinearLayout outgoingLayout = (LinearLayout) convertView.findViewById(R.id.chat_outgoing_layout);
            if (outgoingLayout != null)
            {
                if (RemoteDataManager.getSavedOrgType(this.getContext())==RemoteDataManager.ORG_TYPE_WORKPLACE) {

                    outgoingLayout.setBackgroundResource(R.drawable.pro_chat_outgoing_background);
                    View arrow = convertView.findViewById(R.id.outgoing_arrow);
                    if (arrow != null)
                    {
                        arrow.setBackgroundResource(R.drawable.pro_outgoing_arrow);
                    }
                }
            }
        }



        viewHolder.textView = (TextView) convertView.findViewById(R.id.chat_message);
        viewHolder.dateView = (TextView) convertView.findViewById(R.id.chat_date);
        convertView.setTag(viewHolder);

        viewHolder.textView.setText(web.get(position).getMessage());
        viewHolder.dateView.setText(web.get(position).getMessageDate().toString());

        return convertView;
    }

    @Override
    public ChatMessage getItem(int position) {
        return this.web.get(position);
    }

    @Override
    public int getCount() {
        return this.web.size();
    }



    private static class ViewHolder {
        private TextView dateView;
        private TextView textView;
    }
}
