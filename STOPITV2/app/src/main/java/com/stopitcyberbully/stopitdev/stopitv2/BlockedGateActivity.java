package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;


public class BlockedGateActivity extends AppCompatActivity {

    private boolean mandatory_update = false;
    private boolean org_not_live = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_gate);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!= null) {
            actionBar.setDisplayUseLogoEnabled(true);
        }

        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            mandatory_update = bundle.getBoolean("MANDATORY_UPDATE");
            org_not_live = bundle.getBoolean("ORG_NOT_LIVE");
        }

        CrownMolding cmBlocked = (CrownMolding) findViewById(R.id.blocked_crown_molding);
        cmBlocked.setMode(CrownMolding.CROWN_MOLDING_MODE.REPORT_MOLDING);

        TextView blockedReason = (TextView) findViewById(R.id.blocked_reason);
        Button retryButton = (Button) findViewById(R.id.retry_block);
        if (mandatory_update) {
            blockedReason.setText(this.getResources().getString(R.string.update_available_description));
            retryButton.setText(this.getResources().getString(R.string.update));
            retryButton.setVisibility(View.VISIBLE);
        }else if (org_not_live){
            blockedReason.setText(this.getResources().getString(R.string.org_not_live));
            retryButton.setVisibility(View.INVISIBLE);
        }else {
            blockedReason.setText(this.getResources().getString(R.string.access_disabled));
            retryButton.setVisibility(View.INVISIBLE);
        }
        CustomFontHelper.setCustomFont(blockedReason, "source-sans-pro.semibold.ttf", this);



        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mandatory_update) {
                    final String my_package_name = "com.stopitcyberbully.mobile";
                    String url;

                    try {
                        //Check whether Google Play store is installed or not:
                        BlockedGateActivity.this.getPackageManager().getPackageInfo("com.android.vending", 0);

                        url = "market://details?id=" + my_package_name;
                    } catch (final Exception e) {
                        url = "https://play.google.com/store/apps/details?id=" + my_package_name;
                    }


                    //Open the app page in Google Play store:
                    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }else {
                    refreshRegistration();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mandatory_update) {
            refreshRegistration();
        }
    }

    private void refreshRegistration()
    {
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    try {

                        String code = json.getString("code");
                        if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                            if (json.has("data")) {
                                //If org is alive we can remove the screen
                                if (RemoteDataManager.getRegistration().getOrg().getIsAlive()) {
                                    finish();
                                    if (RemoteDataManager.SHOW_LOG)
                                        Log.d("BlockedGate", json.toString());
                                }
                            }
                        }
                    }catch (JSONException je)
                    {
                        if (RemoteDataManager.SHOW_LOG) Log.e("BlockedGate", je.getMessage());
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    if (RemoteDataManager.SHOW_LOG) Log.e("BlockedGate", "Unable to get blocked user's status");
                }
            });
        }
    }
    @Override
    public void onBackPressed() {
        //background app on back button press event
        moveTaskToBack(true);

    }
}
