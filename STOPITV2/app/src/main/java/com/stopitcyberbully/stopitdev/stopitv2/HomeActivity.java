package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.squareup.picasso.Picasso;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.SmackService;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;


public class HomeActivity extends AppBaseActivity {

    private ImageView orgLogo;
    private TextView unreadCount;
    private AlertDialog alert;
    boolean shouldRefresh = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        CrownMolding reportButton = (CrownMolding) findViewById(R.id.btn_report);
        CrownMolding helpButton = (CrownMolding) findViewById(R.id.btn_help);



            if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
                reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
                helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.GREEN_MOLDING);
            }else{
                reportButton.setMode(CrownMolding.CROWN_MOLDING_MODE.REPORT_MOLDING);
                helpButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);
            }

        TextView reportView = (TextView) findViewById(R.id.txt_report);
        CustomFontHelper.setCustomFont(reportView, "source-sans-pro.semibold.ttf", this);

        TextView helpView = (TextView) findViewById(R.id.txt_help);
        CustomFontHelper.setCustomFont(helpView, "source-sans-pro.semibold.ttf", this);

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_HOME_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_HOME_SCREEN);

        unreadCount = (TextView) findViewById(R.id.unread_count);


        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                refreshRegistrationPriorToReport();

            }
        });
        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoHelpScreen();
            }
        });

        orgLogo = (ImageView) findViewById(R.id.org_logo);


    }

    @Override
    protected void onResume()
    {
        super.onResume();
        refreshRegistration();

        IntentFilter filter = new IntentFilter();
        filter.addAction(SmackService.NEW_MESSAGE);
        filter.addAction(RemoteDataManager.INBOX_NOTIFICATION_BROADCAST_ID);
        registerReceiver(receiver, filter);

        refreshUnreadCount();


        if ((orgLogo!= null) && (RemoteDataManager.getRegistration() != null))
        {
            if (RemoteDataManager.getRegistration().getOrg().getLogoPath()!= null) {

                if (RemoteDataManager.getRegistration().getOrg().getLogoPath().length() >0) {
                    Picasso.with(this)
                            .load(RemoteDataManager.getRegistration().getOrg().getLogoPath())
                            .into(orgLogo);

                }
            }
        }
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
        if (progressDialog != null) progressDialog.dismiss();
        if (alert != null) alert.cancel();
        unregisterReceiver(receiver);
    }



    private void refreshRegistration()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        final String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    if (!RemoteDataManager.getRegistration().getOrg().getIsAlive()) {
                                        //Org is not alive, we need to show blocked screen
                                        Intent blockedIntent = new Intent(HomeActivity.this, BlockedGateActivity.class);
                                        blockedIntent.putExtra("ORG_NOT_LIVE", true);
                                        HomeActivity.this.startActivity(blockedIntent);
                                    }else {
                                        getUserContacts();
                                        startChatService();
                                        //show popup for inbox messages
                                        updateInboxCounts(true);

                                        if (RemoteDataManager.SHOW_LOG)
                                            Log.d("HomeActivity", json.toString());
                                    }
                                }
                            }else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER))
                            {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(HomeActivity.this, BlockedGateActivity.class);
                                HomeActivity.this.startActivity(blockedIntent);
                            }
                        }catch (JSONException je)
                        {
                            showErrorDialog();

                        }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    showErrorDialog();
                }
            });
        }
    }

    private void getUserContacts () {
        RemoteDataManager.getContacts(this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject o) {
                if (RemoteDataManager.SHOW_LOG) Log.d("HomeActivity", "Received contacts");
                progressDialog.dismiss();
                RemoteDataManager.hasAppExpired(HomeActivity.this, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject o) {
                                //If json is null, it indicates there is no update
                                if (o == null) return;

                                //update the menu with update count of the app update.
                                updateInboxCounts(false);
                                //This indicates, json returned is not null and we have an update available.
                                AlertDialog.Builder updateAlert = new AlertDialog.Builder(HomeActivity.this);
                                updateAlert.setCancelable(true);
                                updateAlert.setMessage(getResources().getString(R.string.update_available_description));
                                updateAlert.setPositiveButton(getResources().getString(R.string.update),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                if (RemoteDataManager.SHOW_LOG) Log.d("RemoteDataManager", "Optional Update");
                                                //Launch expiry notification
                                                final String my_package_name = "com.stopitcyberbully.mobile";
                                                String url;

                                                try {
                                                    //Check whether Google Play store is installed or not:
                                                    HomeActivity.this.getPackageManager().getPackageInfo("com.android.vending", 0);

                                                    url = "market://details?id=" + my_package_name;
                                                } catch ( final Exception e ) {
                                                    url = "https://play.google.com/store/apps/details?id=" + my_package_name;
                                                }


                                                //Open the app page in Google Play store:
                                                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                                startActivity(intent);
                                            }
                                        });
                                updateAlert.setNegativeButton(getResources().getString(R.string.not_now), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                AlertDialog alert = updateAlert.create();
                                alert.show();
                            }
                        },
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object o) {
                                //This app has expired. We need to show user update screen
//                                Intent blockedIntent = new Intent(HomeActivity.this, BlockedGateActivity.class);
//                                HomeActivity.this.startActivity(blockedIntent);
                            }
                        });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (RemoteDataManager.SHOW_LOG) Log.d("HomeActivity", volleyError.toString());
                showErrorDialog();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }
    @Override
    public void onBackPressed() {
        //background app on back button press event
        moveTaskToBack(true);

    }



    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshUnreadCount();
            updateInboxCounts(true);
        }
    };

    private void refreshUnreadCount()
    {
        invalidateOptionsMenu();

        if (unreadCount == null)
        {
            unreadCount = (TextView) findViewById(R.id.unread_count);
        }
        if (unreadCount != null) {
            if (RemoteDataManager.getUnreadMessageCount(this) > 0) {
                unreadCount.setText(getResources().getString(R.string.inbox_count, RemoteDataManager.getUnreadMessageCount(this)));
                unreadCount.setVisibility(View.VISIBLE);
                showMessengerDialog();
            } else {
                unreadCount.setText("");
                unreadCount.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void showMessengerDialog(){
        if( (alert != null) && (alert.isShowing()) )
        {
            return;
        }
        AlertDialog.Builder messengerAlert = new AlertDialog.Builder(HomeActivity.this);
        messengerAlert.setMessage(getResources().getString(R.string.waiting_message_prompt));
        messengerAlert.setCancelable(false);
        messengerAlert.setPositiveButton(getResources().getString(R.string.waiting_message_prompt_read),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        gotoChatScreen();

                    }
                });
        messengerAlert.setNegativeButton(getResources().getString(R.string.waiting_message_prompt_close),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alert = messengerAlert.create();
        alert.show();

    }
    private void showErrorDialog(){
        //show alert
        progressDialog.dismiss();
        AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(HomeActivity.this);
        anonymousAlert.setMessage(getResources().getString(R.string.no_server));
        anonymousAlert.setCancelable(true);
        anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        progressDialog.dismiss();
                        refreshRegistration();
                        dialog.cancel();
                    }
                });

        AlertDialog alert = anonymousAlert.create();
        alert.show();
    }
    private void gotoChatScreen(){
        if (RemoteDataManager.isConnected(this)){
            Intent chatIntent = new Intent(HomeActivity.this, ChatActivity.class);
            HomeActivity.this.startActivity(chatIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(true);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoChatScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void gotoReportScreen(){
        if (RemoteDataManager.isConnected(this)){
            Intent reportIntent = new Intent(HomeActivity.this, ReportActivity.class);
            HomeActivity.this.startActivity(reportIntent);
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(true);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoReportScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }
    private void gotoHelpScreen(){
        if (RemoteDataManager.isConnected(this)){
//            if (RemoteDataManager.getRegistration().getOrg().getOrgTypeID() != RemoteDataManager.ORG_TYPE_HIGHERED) {
                Intent helpIntent = new Intent(HomeActivity.this, GetHelpActivity.class);
                HomeActivity.this.startActivity(helpIntent);
//            } else {
//                Intent helpHigherEdIntent = new Intent(HomeActivity.this, GetHigherEdHelpActivity.class);
//                HomeActivity.this.startActivity(helpHigherEdIntent);
//
//            }
        }else{
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
            anonymousAlert.setCancelable(false);
            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            gotoHelpScreen();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        }
    }

    private void refreshRegistrationPriorToReport()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    if (json.getClass().equals(JSONObject.class)) {
                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    progressDialog.dismiss();
                                    if (!RemoteDataManager.getRegistration().getOrg().getIsAlive())
                                    {
                                        //Org is not alive, we need to show blocked screen
                                        Intent blockedIntent = new Intent(HomeActivity.this, BlockedGateActivity.class);
                                        blockedIntent.putExtra("ORG_NOT_LIVE", true);
                                        HomeActivity.this.startActivity(blockedIntent);
                                    }else if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                        String tag = "recess_dialog";
                                        DialogFragment recessFragment =
                                                RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                        recessFragment.show(getSupportFragmentManager(), tag);
                                    } else {

                                        gotoReportScreen();
                                    }
                                }
                            } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(HomeActivity.this, BlockedGateActivity.class);
                                HomeActivity.this.startActivity(blockedIntent);
                            }
                        } catch (JSONException je) {
                            progressDialog.dismiss();
                            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(HomeActivity.this);
                            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                            anonymousAlert.setCancelable(false);
                            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            progressDialog.dismiss();
                                            refreshRegistrationPriorToReport();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = anonymousAlert.create();
                            alert.show();

                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(HomeActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    refreshRegistrationPriorToReport();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }
}
