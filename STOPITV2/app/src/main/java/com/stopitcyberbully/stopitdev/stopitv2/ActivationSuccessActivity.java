package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.parse.ParseAnalytics;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.OnboardingImageListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import java.util.ArrayList;
import java.util.Collections;


public class ActivationSuccessActivity extends AppCompatActivity {
    TextView heading;
    Bitmap logoBitmap;
    @Override
    protected void onResume() {
        super.onResume();

        VideoView videoView = (VideoView) findViewById(R.id.video_onboarding);

        if (videoView != null) {
            if (RemoteDataManager.getRegistration().getOrg().getVideoURL() != null) {
                if (!videoView.isPlaying()) {
                    Uri videoURI = Uri.parse(RemoteDataManager.getRegistration().getOrg().getVideoURL());
                    videoView.setVisibility(View.VISIBLE);
                    MediaController mc = new MediaController(this);
                    mc.setMediaPlayer(videoView);
                    videoView.setMediaController(mc);
                    videoView.setVideoURI(videoURI);
                    videoView.requestFocus();
                    videoView.start();
                    mc.setAnchorView(videoView);
                    heading.setVisibility(View.INVISIBLE);
                }

            } else {
                videoView.setVisibility(View.INVISIBLE);
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        setContentView(R.layout.activity_activation_success);

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;


// Set some constants
            logoBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.langing_logo);


// Crop bitmap
            Bitmap newBitmap = Bitmap.createBitmap(logoBitmap, 0, 0, logoBitmap.getWidth() / 2, logoBitmap.getHeight(), null, false);

// Assign new bitmap to ImageView
            ImageView backImage = (ImageView) findViewById(R.id.landing_back_image);
            backImage.setImageBitmap(newBitmap);


            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(3000);

            backImage.setAnimation(alphaAnimation);

        heading = (TextView) findViewById(R.id.landing_heading_view);

        //show background logo animation only if there is no video
        if (RemoteDataManager.getRegistration().getOrg().getVideoURL() == null) {

            AlphaAnimation alphaTextAnimation = new AlphaAnimation(0.5f, 1.0f);
            alphaTextAnimation.setDuration(3000);

            heading.setAnimation(alphaAnimation);
        }else {
            heading.setVisibility(View.INVISIBLE);
        }

        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
            heading.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
        }


            TranslateAnimation tm = new TranslateAnimation(width, 0, 0, 0);
            tm.setStartOffset(1000);
            tm.setDuration(5000);
            backImage.setAnimation(tm);

            CustomFontHelper.setCustomFont(heading, "source-sans-pro.semibold.ttf", this);


        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_APPACTIVATED, AnalyticsManager.ANALYTICS_ACTION_APPACTIVATED);




        final ListView listview = (ListView) findViewById(R.id.landing_listview);
        String[] values = new String[] { getResources().getString(R.string.activation_success_continue) };

        final ArrayList<String> list = new ArrayList<>();

        Collections.addAll(list, values);

        Integer[] imageId = {R.drawable.ic_stat_name};
        OnboardingImageListAdapter adapter = new OnboardingImageListAdapter(this, list, imageId);


        if (listview != null) {
            listview.setAdapter(adapter);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    Intent homeIntent = new Intent(ActivationSuccessActivity.this, HomeActivity.class);
                    ActivationSuccessActivity.this.startActivity(homeIntent);
                    overridePendingTransition(R.anim.abc_grow_fade_in_from_bottom, R.anim.abc_shrink_fade_out_from_bottom);
                }
            });
        }


    }

    @Override
    public void onBackPressed() {
        //do nothing to avoid back button
    }
}
