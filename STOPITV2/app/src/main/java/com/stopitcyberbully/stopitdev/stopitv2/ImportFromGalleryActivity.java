package com.stopitcyberbully.stopitdev.stopitv2;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.IncidentObject;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.AuthRequest;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Date;


public class ImportFromGalleryActivity extends AppBaseActivity {

    private String incidentID;
    private EditText editText;
    private IncidentObject incidentObject;
    private boolean isImporting;
    private Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_from_gallery);
        CrownMolding send = (CrownMolding ) findViewById(R.id.send_button_bottom);


        //first check if the user has logged in or not.
        if (RemoteDataManager.getRegistration()==null)
        {
            //user registration
            Intent registrationIntent = new Intent(ImportFromGalleryActivity.this, LandingActivity.class);
            ImportFromGalleryActivity.this.startActivity(registrationIntent);
            return;
        }

        // Check Recess and org alive flag
        if (!RemoteDataManager.getRegistration().getOrg().getIsAlive())
        {
            //Org is not alive, we need to show blocked screen
            Intent blockedIntent = new Intent(ImportFromGalleryActivity.this, BlockedGateActivity.class);
            blockedIntent.putExtra("ORG_NOT_LIVE", true);
            ImportFromGalleryActivity.this.startActivity(blockedIntent);
            return;
        }else if (
                        RemoteDataManager.getRegistration().getOrg().isInRecess()
                ||
                        RemoteDataManager.getRegistration().getOrg().getIsMediaUploadDisabled()
                ) {
            Intent registrationIntent = new Intent(ImportFromGalleryActivity.this, LandingActivity.class);
            ImportFromGalleryActivity.this.startActivity(registrationIntent);
            return;
        }

            if (RemoteDataManager.getRegistration().getOrg().getOrgTypeID() == RemoteDataManager.ORG_TYPE_WORKPLACE) {
            send.setMode(CrownMolding.CROWN_MOLDING_MODE.BLUE_MOLDING);
        }

        editText = (EditText) findViewById(R.id.txt_incident_notes);
        CustomFontHelper.setCustomFont(editText, "source-sans-pro.semibold.ttf", this);

        TextView txtReport = (TextView) findViewById(R.id.header_report_to);
        CustomFontHelper.setCustomFont(txtReport, "source-sans-pro.semibold.ttf", this);

        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
            txtReport.setTextColor(ContextCompat.getColor(this, R.color.pro_heading_blue));
        }




        txtReport.setText(getResources().getString(R.string.title_incident_report_non_k12));

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_USER_OPENED_1TOUCH_INCIDENT_REPORT, AnalyticsManager.ANALYTICS_ACTION_USER_OPENED_1TOUCH_INCIDENT_REPORT);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(ImportFromGalleryActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    isImporting = false;
                    refreshRegistrationPriorToReport();
                } else {
                    isImporting = false;
                    showStoragePermissions();
                }
            }
        });


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String action = intent.getAction();

        // if this is from the share menu
        if (Intent.ACTION_SEND.equals(action)) {
            if (extras.containsKey(Intent.EXTRA_STREAM)) {
                // Get resource path
                imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);

                if (ActivityCompat.checkSelfPermission(ImportFromGalleryActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
                    importImage(imageUri);
                }else{
                    isImporting = true;
                    showStoragePermissions();
                }

            }
        }
    }

    private void importImage (Uri imageUri)
    {
        ImageView importedImage = (ImageView) findViewById(R.id.imported_image);
        incidentObject = new IncidentObject();
        if (imageUri.toString().startsWith("content://com.google.android.apps.photos.content"))
        {

            try {
                incidentObject.saveImageURIToFile(this, imageUri);
                Picasso.with(this).load(incidentObject.getUri()).placeholder(R.drawable.video_placeholder).fit().into(importedImage);


            }catch (FileNotFoundException fnfe)
            {
                AlertDialog.Builder importMediaBuider = new AlertDialog.Builder(ImportFromGalleryActivity.this);
                importMediaBuider.setMessage(getResources().getString(R.string.media_import_failed));

                importMediaBuider.setCancelable(true);
                importMediaBuider.setPositiveButton(getResources().getString(R.string.OK),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog successDialog = importMediaBuider.create();
                successDialog.show();

            }

        }else {
            incidentObject.setUri(imageUri);
            Picasso.with(this).load(imageUri).placeholder(R.drawable.video_placeholder).fit().into(importedImage);
        }
    }

        //Create the incident if incident creation is successful, stream the media upload.
    private void createIncident(final JSONObject incidentObject)
    {
        showProgress(getResources().getString(R.string.prompt_report_incident));
        RequestQueue queue = RemoteDataManager.sharedManager(this);
        AuthRequest incidentRequest = new AuthRequest(Request.Method.POST, RemoteDataManager.getINCIDENT_CREATION_URL(), incidentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (jsonObject.has("data"))
                {
                    try {
                        JSONObject dataResponse = jsonObject.getJSONObject("data");
                        if (dataResponse.has("incident_id"))
                        {
                            incidentID = dataResponse.getString("incident_id");
                            String afterHours = null;
                            if (dataResponse.has("after_hour_report_message")) {
                                afterHours = dataResponse.getString("after_hour_report_message");
                            }
                                sendImages(afterHours);
                        }
                    }catch (JSONException je)
                    {
                        //Show retry option
                        showRetryDialog(incidentObject);
                    }


                }else{
                    //Show retry option
                    showRetryDialog(incidentObject);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //Show retry option
                showRetryDialog(incidentObject);
            }
        }, RemoteDataManager.DEFAULT_TIMEOUT);
        queue.add(incidentRequest);
    }

    private void showRetryDialog(final JSONObject jsonObject)
    {
        removeProgressDialog();
        AlertDialog.Builder errorBuilder = new AlertDialog.Builder(ImportFromGalleryActivity.this);
        errorBuilder.setMessage(getResources().getString(R.string.prompt_retry_incident_submission));
        errorBuilder.setCancelable(true);
        errorBuilder.setPositiveButton(getResources().getString(R.string.YES),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        createIncident(jsonObject);
                        dialog.cancel();
                    }
                });
        errorBuilder.setNegativeButton(getResources().getString(R.string.NO),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog errorDialog = errorBuilder.create();
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY,AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_1_TOUCH_INCIDENT_REPORT_FAILED,AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_1_TOUCH_INCIDENT_REPORT_FAILED);
        errorDialog.show();
    }
    private void showSuccessDialog(String afterhours)
    {
        removeProgressDialog();

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_1TOUCH_INCIDENT_REPORT, AnalyticsManager.ANALYTICS_ACTION_SUBMITTED_1TOUCH_INCIDENT_REPORT);

        AlertDialog.Builder successBuider = new AlertDialog.Builder(ImportFromGalleryActivity.this);
        if (afterhours!= null) {
            successBuider.setMessage(afterhours);
        }else {
            successBuider.setMessage(getResources().getString(R.string.prompt_success_incident_submission));
        }
        successBuider.setCancelable(true);
        successBuider.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        AlertDialog successDialog = successBuider.create();
        successDialog.show();
    }

    private void sendImages(String afterHours)
    {
            JSONObject incidentJSON = new JSONObject();
            JSONObject dataJSON = new JSONObject();

            JSONObject attachmentObject = new JSONObject();


            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            long millitime = calendar.getTimeInMillis();


            String analytics_label = "Number of attached media: 1";
            AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ADDED_MEDIA_ON_1_TOUCH, analytics_label);

            try {

                Context context = ImportFromGalleryActivity.this;

                String filename = "file0." + incidentObject.getExtension(context);
                attachmentObject.put("filename", filename);


                String dirName = "orgs/" + RemoteDataManager.getRegistration().getOrg().getOrgID() + "/incidents/" + incidentID + "/" + millitime +"/attachments/" + filename;
                attachmentObject.put("url", dirName);

                if (!RemoteDataManager.sendToAWS(context, dirName, incidentObject.getFile(context)))
                {
                    if (afterHours == null)
                    {
                        afterHours = ImportFromGalleryActivity.this.getString(R.string.media_upload_failed);
                    }else {
                        afterHours = afterHours.concat(ImportFromGalleryActivity.this.getString(R.string.media_upload_failed));
                    }
                }




                attachmentObject.put("url", dirName);

                JSONArray dataArray = new JSONArray();
                dataArray.put(attachmentObject);
                incidentJSON.put("incident_category_id", RemoteDataManager.INCIDENT_CATEGORY_1TOUCH);

                JSONObject submitJSON = new JSONObject();
                dataJSON.put("attachments", dataArray);
                submitJSON.put("data", dataJSON);
                RemoteDataManager.updateIncident(context, submitJSON, incidentID, true);
                showSuccessDialog(afterHours);


            }catch (Exception je)
            {
                if (RemoteDataManager.SHOW_LOG) Log.e("ImportFromGallery", je.toString());
                //It was directed that we do not tell users about error during uploading an image.
                //We only inform users if the incident does not get created.
                removeProgressDialog();
                finish();
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        if (RemoteDataManager.getSavedOrgType(this)==RemoteDataManager.ORG_TYPE_WORKPLACE)
        {
            inflater.inflate(R.menu.pro_menu_report, menu);
        }else {
            inflater.inflate(R.menu.menu_report, menu);
        }
        MenuItem sendItemDisabled = menu.findItem(R.id.action_button_send_disabled);

        sendItemDisabled.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_button_send: {

                refreshRegistrationPriorToReport();
            }
            return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void prepareIncident(){
        try {
            JSONObject incidentJSON = new JSONObject();
            JSONObject dataJSON = new JSONObject();
            dataJSON.put("data", editText.getText());

            JSONArray dataArray = new JSONArray();
            dataArray.put(dataJSON);

            incidentJSON.put("is_anonymous", "1");
            incidentJSON.put("incident_category_id", RemoteDataManager.INCIDENT_CATEGORY_1TOUCH);
            incidentJSON.put("notes", dataArray);
            final JSONObject submitJSON = new JSONObject();

            if (RemoteDataManager.getEncryptionSalt() != null)
            {
                String EncryptedIncident = RemoteDataManager.encrypt(RemoteDataManager.getEncryptionSalt(), incidentJSON.toString());
                submitJSON.put("data", EncryptedIncident);
            }else {
                submitJSON.put("data", incidentJSON);
            }

            createIncident(submitJSON);

        }catch (JSONException je)
        {
            if (RemoteDataManager.SHOW_LOG) Log.e("ImportActivity", je.toString());
        }catch (Exception e)
        {
            if (RemoteDataManager.SHOW_LOG) Log.e("ImportActivity", e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack(){
        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_CANCELED_1TOUCH_INCIDENT_REPORT, AnalyticsManager.ANALYTICS_ACTION_CANCELED_1TOUCH_INCIDENT_REPORT);

        super.onBackPressed();
    }


    private void refreshRegistrationPriorToReport()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {


                    try {

                        String code = json.getString("code");
                        if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                            if (json.has("data")) {
                                progressDialog.dismiss();

                                if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                    String tag = "recess_dialog";
                                    DialogFragment recessFragment =
                                            RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                    recessFragment.show(getSupportFragmentManager(), tag);
                                } else {

                                    prepareIncident();
                                }
                            }
                        } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                            //User is blocked, we need to show blocked screen
                            Intent blockedIntent = new Intent(ImportFromGalleryActivity.this, BlockedGateActivity.class);
                            ImportFromGalleryActivity.this.startActivity(blockedIntent);
                        }
                    } catch (JSONException je) {
                        progressDialog.dismiss();
                        AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ImportFromGalleryActivity.this);
                        anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                        anonymousAlert.setCancelable(false);
                        anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        progressDialog.dismiss();
                                        refreshRegistrationPriorToReport();
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert = anonymousAlert.create();
                        alert.show();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ImportFromGalleryActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    refreshRegistrationPriorToReport();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }


    //region Marshmallow permissions related
    private void showStoragePermissions(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(ImportFromGalleryActivity.this);
            anonymousAlert.setMessage(getResources().getString(R.string.photo_settings));
            anonymousAlert.setPositiveButton(getResources().getString(R.string.OK),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = anonymousAlert.create();
            alert.show();

        } else {

            // Storage permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(ImportFromGalleryActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
            if (isImporting)
            {
                if (imageUri != null) importImage(imageUri);
            }else {
                refreshRegistrationPriorToReport();
            }
        }
    }
    //endregion
}
