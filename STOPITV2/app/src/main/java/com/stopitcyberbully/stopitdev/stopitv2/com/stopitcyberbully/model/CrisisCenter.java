package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

/**
 * Object model to store data related to Crisis center information obtained from webservice
 */
public class CrisisCenter {
    private int crisisCenterID;
    private String crisisCenterName;
    private String crisisCenterPhone;
    private String crisisCenterEmail;
    private String crisisCenterMobilePhone;

    public int getCrisisCenterID() {
        return crisisCenterID;
    }

    public void setCrisisCenterID(int crisisCenterID) {
        this.crisisCenterID = crisisCenterID;
    }

    public String getCrisisCenterName() {
        return crisisCenterName;
    }

    public void setCrisisCenterName(String crisisCenterName) {
        this.crisisCenterName = crisisCenterName;
    }

    public String getCrisisCenterPhone() {
        return crisisCenterPhone;
    }

    public String getCrisisCenterMobilePhone() {
        return crisisCenterMobilePhone;
    }

    public void setCrisisCenterMobilePhone(String crisisCenterMobilePhone) {
        this.crisisCenterMobilePhone = crisisCenterMobilePhone;
    }

    public void setCrisisCenterPhone(String crisisCenterPhone) {
        this.crisisCenterPhone = crisisCenterPhone;

    }

    public String getCrisisCenterEmail() {
        return crisisCenterEmail;
    }

    public void setCrisisCenterEmail(String crisisCenterEmail) {
        this.crisisCenterEmail = crisisCenterEmail;
    }



}
