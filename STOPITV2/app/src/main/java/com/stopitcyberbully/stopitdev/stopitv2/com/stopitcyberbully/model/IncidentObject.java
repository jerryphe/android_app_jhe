package com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.webkit.MimeTypeMap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Core handler class to keep attachment related information.
 */
public class IncidentObject {
    private Uri uri;
    private String imageFileName;
    private Image placeHolder;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    public Image getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(Image placeHolder) {
        this.placeHolder = placeHolder;
    }

    public String getExtension(Context context)
    {
        ContentResolver contentResolver = context.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return(mime.getExtensionFromMimeType(contentResolver.getType(uri)));

    }

    public String getMimeType(Context context)
    {
        ContentResolver contentResolver = context.getContentResolver();
        return(contentResolver.getType(uri));

    }



    public File getFile( Context context)
    {

        ContentResolver contentResolver = context.getContentResolver();
        String filePath = getFilePathFromContentUri(uri, contentResolver);
        if (filePath == null) return null;

        return new File(filePath);
    }

    public long getFileSize(Context context)
            throws FileNotFoundException
    {
        File mediaFile = getFile(context);
        if (mediaFile == null) throw new FileNotFoundException();
        return mediaFile.length();
    }

    private String getFilePathFromContentUri(Uri selectedVideoUri,
                                             ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        if (cursor == null) return null;
        if (cursor.getColumnCount() == 0) return null;
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }

    public  String getBase64String(Context context)
    {
        try {
            ContentResolver contentResolver = context.getContentResolver();


            Bitmap bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();
            return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        }catch (Exception fnfe)
        {
            return "";
        }
    }

    public void saveImageURIToFile(Context context, Uri remoteurl)
            throws FileNotFoundException
    {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        InputStream is = context.getContentResolver().openInputStream(remoteurl);
        Bitmap bmp = BitmapFactory.decodeStream(is);

        if (bmp == null) throw new FileNotFoundException();

        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        this.imageFileName = MediaStore.Images.Media.insertImage(context.getContentResolver(), bmp, "Title", null);

        Uri localUri = Uri.parse(this.imageFileName);

        this.setUri(localUri);

    }


}
