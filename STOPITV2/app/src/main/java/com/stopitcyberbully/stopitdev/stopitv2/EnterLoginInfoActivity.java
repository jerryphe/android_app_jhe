package com.stopitcyberbully.stopitdev.stopitv2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;

import org.json.JSONException;
import org.json.JSONObject;


public class EnterLoginInfoActivity extends AppCompatActivity {
     private EditText lastNameText;
     private EditText challegeText;
     private Button next;
     private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_login_info);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.stopit_nav_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }


        TextView orgName = (TextView) findViewById(R.id.org_name);
        CustomFontHelper.setCustomFont(orgName, "source-sans-pro.semibold.ttf", this);

        TextView textHeading = (TextView) findViewById(R.id.enteraccesscode_heading);
        CustomFontHelper.setCustomFont(textHeading, "source-sans-pro.semibold.ttf", this);

        TextView textSubHeading = (TextView) findViewById(R.id.enteraccesscode_subheading);
        if (textSubHeading != null) {
            textSubHeading.setText(Html.fromHtml(getString(R.string.subheading_access_code)));
        }
        CustomFontHelper.setCustomFont(textSubHeading, "SourceCodePro.Regular.ttf", this);


// Set some constants
        Bitmap sourceEclipseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ellipse);


// Crop bitmap
        Bitmap eclipseBitmap = Bitmap.createBitmap(sourceEclipseBitmap, 0, 0, sourceEclipseBitmap.getWidth() / 2, sourceEclipseBitmap.getHeight(), null, false);

        final AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        ImageView userImage = (ImageView) findViewById(R.id.image_user);

        ImageView eclipseImage = (ImageView) findViewById(R.id.image_eclipse);
        eclipseImage.setImageBitmap(eclipseBitmap);
        eclipseImage.setAnimation(alphaAnimation);
        userImage.setAnimation(alphaAnimation);


        String orgNameValue = RemoteDataManager.getSelectedOrganization().getOrgName();

        next = (Button) findViewById(R.id.btn_access_next);
        next.setEnabled(false);
        lastNameText = (EditText) findViewById(R.id.last_name);
        CustomFontHelper.setCustomFont(lastNameText, "source-sans-pro.semibold.ttf", this);
        lastNameText.setHint(Html.fromHtml("<small>" + this.getResources().getString(R.string.INTELLICODE_LOOKUP_LASTNAME) + "</small>"));


        challegeText = (EditText) findViewById(R.id.challenge_text);
        CustomFontHelper.setCustomFont(challegeText, "source-sans-pro.semibold.ttf", this);



        progressBar = (ProgressBar) findViewById(R.id.roster_check_progressbar);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                showProgress(getResources().getString(R.string.message_signing_in));
                JSONObject registerJson = new JSONObject();
                try {
                    registerJson.put("last_name", lastNameText.getText());
                    registerJson.put("serial_number", RemoteDataManager.getDeviceID(EnterLoginInfoActivity.this));
                    registerJson.put("org_id", RemoteDataManager.getSelectedOrganization().getOrgID());
                    registerJson.put("roster_type", RemoteDataManager.getSelectedOrganization().getRosterType().toLowerCase());
                    registerJson.put("roster_id", challegeText.getText());
                    registerByRoster(registerJson);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        orgName.setText(orgNameValue);

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_CHALLENGE_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_CHALLENGE_SCREEN);


        String rosterLabel = RemoteDataManager.getSelectedOrganization().getRosterLabel();
        if (rosterLabel != null) {
            challegeText.setHint(Html.fromHtml("<small>" + rosterLabel + "</small>"));

        }

        lastNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                flipButtonState();
            }
        });

        challegeText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                flipButtonState();
            }
        });





    }

    private void flipButtonState()
    {
        if ((lastNameText.getText().length() >0) && (challegeText.getText().length()>0))
        {
            next.setEnabled(true);
        }else{
            next.setEnabled(false);
        }

    }

    private void registerByRoster(JSONObject jsonObject)
    {
        RemoteDataManager.registerByRoster(this, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject o) {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ENTERED_RIGHT_ACCESS_CODE, AnalyticsManager.ANALYTICS_ACTION_ENTERED_RIGHT_ACCESS_CODE);
                getUserContacts();

                progressDialog.dismiss();

                Intent activationSuccessIntent = new Intent(EnterLoginInfoActivity.this, ActivationSuccessActivity.class);
                activationSuccessIntent.setFlags(activationSuccessIntent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK);
                EnterLoginInfoActivity.this.startActivity(activationSuccessIntent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_ENTERED_WRONG_ACCESS_CODE, AnalyticsManager.ANALYTICS_ACTION_ENTERED_WRONG_ACCESS_CODE);
                showErrorDialog();
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private void getUserContacts () {
        RemoteDataManager.getContacts(this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject o) {
                if (RemoteDataManager.SHOW_LOG) Log.d("EnterLoginInfoActivity", "Received contacts");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (RemoteDataManager.SHOW_LOG) Log.d("EnterLoginInfoActivity", volleyError.toString());
            }
        });

    }
    private void showProgress(String message)
    {
        progressDialog = ProgressDialog.show(EnterLoginInfoActivity.this, "", message);
        new Thread() {

            public void run() {

                try{
                    sleep(6000*60*60);
                } catch (Exception e) {
                    if (RemoteDataManager.SHOW_LOG) Log.e("tag", e.getMessage());
                }
                progressDialog.dismiss();

            }

        }.start();

    }
    private void showErrorDialog()
    {
        progressDialog.dismiss();

        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);

        AlertDialog.Builder successBuider = new AlertDialog.Builder(EnterLoginInfoActivity.this);
        successBuider.setMessage(getResources().getString(R.string.message_access_code_not_found));

        successBuider.setCancelable(true);
        successBuider.setPositiveButton(getResources().getString(R.string.OK),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog successDialog = successBuider.create();
        successDialog.show();
    }

}
