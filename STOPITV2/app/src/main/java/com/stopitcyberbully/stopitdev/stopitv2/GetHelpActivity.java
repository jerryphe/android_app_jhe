package com.stopitcyberbully.stopitdev.stopitv2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.CrisisContactListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.adapter.HelpContactListAdapter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.AnalyticsManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.api.RemoteDataManager;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.chat.SmackService;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.model.CrisisCenter;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CrownMolding;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.CustomFontHelper;
import com.stopitcyberbully.stopitdev.stopitv2.com.stopitcyberbully.utils.DottedRibbon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class GetHelpActivity extends AppBaseActivity {

    private CrisisContactListAdapter crisisContactListAdapter;
    private HelpContactListAdapter contactListAdapter;
    private ListView contactList;
    private ListView crisisContactList;
    private TextView unreadCount;
    private AlertDialog alert;
    private TextView headerText;


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if ((RemoteDataManager.getRegistration() != null) && (RemoteDataManager.getRegistration().getOrg().getOrgName() != null))
        {
            outState.putString("SAVED_ORG_NAME", RemoteDataManager.getRegistration().getOrg().getOrgName());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_help);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        headerText = (TextView) findViewById(R.id.text_chat_header);
        //check if we have org name stored in saved instance state
        if (savedInstanceState != null)
        {
            String orgName = savedInstanceState.getString("SAVED_ORG_NAME");
            if (orgName != null)
                headerText.setText(getString(R.string.chat_header_text, orgName));
        }
        if (RemoteDataManager.getRegistration()!=null) {
            refreshScreenElements();
        }else {
            refreshRegistration();
        }

    }

    @Override
    protected void onResume(){
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(SmackService.NEW_MESSAGE);
        filter.addAction(RemoteDataManager.INBOX_NOTIFICATION_BROADCAST_ID);
        registerReceiver(receiver, filter);

        refreshUnreadCount();

        //show popup for inbox messages
        updateInboxCounts(true);

        if (RemoteDataManager.getRegistration() != null) {
            contactListAdapter = new HelpContactListAdapter(this, R.layout.helpcontacts, RemoteDataManager.getUserContacts().getHelpContacts());
            contactList.setAdapter(contactListAdapter);

            //Check if we have crisis center added or not
            if (RemoteDataManager.getRegistration().getCrisisCenter() != null) {
                ArrayList<CrisisCenter> crisisCenters = new ArrayList<>();
                crisisCenters.add(RemoteDataManager.getRegistration().getCrisisCenter());

                crisisContactListAdapter = new CrisisContactListAdapter(this, R.layout.helpcontacts, crisisCenters);
                crisisContactList = (ListView) findViewById(R.id.crisis_contact_list);
                crisisContactList.setAdapter(crisisContactListAdapter);
            }
        }
        if (RemoteDataManager.getRegistration()!=null) {
            refreshScreenElements();
        }else {
            refreshRegistration();
        }


    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (alert != null) alert.cancel();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_base, menu);
        return true;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshUnreadCount();
        }
    };

    private void refreshUnreadCount()
    {
        invalidateOptionsMenu();
        if (unreadCount == null)
        {
            unreadCount = (TextView)findViewById(R.id.unread_count);
        }

        if (unreadCount != null){
            if (RemoteDataManager.getUnreadMessageCount(this) >0) {
                unreadCount.setText(" "+RemoteDataManager.getUnreadMessageCount(this) + " ");
                unreadCount.setVisibility(View.VISIBLE);
                showMessengerDialog();
            }else {
                unreadCount.setText("");
                unreadCount.setVisibility(View.INVISIBLE);
            }
        }

    }

    private void showMessengerDialog(){

        if( (alert != null) && (alert.isShowing()) )
        {
            return;
        }
        AlertDialog.Builder messengerAlert = new AlertDialog.Builder(GetHelpActivity.this);
        messengerAlert.setMessage(getResources().getString(R.string.waiting_message_prompt));
        messengerAlert.setCancelable(false);
        messengerAlert.setPositiveButton(getResources().getString(R.string.waiting_message_prompt_read),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        gotoChatScreen();
                    }
                });
        messengerAlert.setNegativeButton(getResources().getString(R.string.waiting_message_prompt_close),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alert = messengerAlert.create();
        alert.show();

    }

    private void gotoChatScreen(){
        if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
            String tag = "recess_dialog";
            DialogFragment recessFragment =
                    RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
            recessFragment.show(getSupportFragmentManager(), tag);
        }else {
            if (RemoteDataManager.isConnected(this)) {
                Intent chatIntent = new Intent(GetHelpActivity.this, ChatActivity.class);
                GetHelpActivity.this.startActivity(chatIntent);
            } else {
                AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(this);
                anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                anonymousAlert.setCancelable(false);
                anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                gotoChatScreen();
                            }
                        });

                AlertDialog alert = anonymousAlert.create();
                alert.show();

            }
        }
    }

    private void refreshRegistration()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    try {

                        String code = json.getString("code");
                        if (code.equals(RemoteDataManager.RESPONSE_OK)) {
                            getUserContacts();
                        } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                            //User is blocked, we need to show blocked screen
                            Intent blockedIntent = new Intent(GetHelpActivity.this, BlockedGateActivity.class);
                            GetHelpActivity.this.startActivity(blockedIntent);
                        }
                    } catch (JSONException je) {
                        showErrorDialog();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    showErrorDialog();
                }
            });
        }
    }

    private void getUserContacts () {
        RemoteDataManager.getContacts(this, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject o) {
                if (RemoteDataManager.SHOW_LOG) Log.d("GetHelpActivity", "Received contacts");
                progressDialog.dismiss();
                if (RemoteDataManager.getRegistration() != null) {
                    refreshScreenElements();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (RemoteDataManager.SHOW_LOG) Log.d("GetHelpActivity", volleyError.toString());
                showErrorDialog();
            }
        });
    }
    private void showErrorDialog(){
        //show alert
        progressDialog.dismiss();
        AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(GetHelpActivity.this);
        anonymousAlert.setMessage(getResources().getString(R.string.no_server));
        anonymousAlert.setCancelable(false);
        anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        progressDialog.dismiss();
                        refreshRegistration();
                        dialog.cancel();
                    }
                });

        AlertDialog alert = anonymousAlert.create();
        alert.show();
    }

    private void refreshScreenElements(){
        //If we have org name stored in RemoteDataManager use it.
        if (RemoteDataManager.getRegistration().getOrg().getOrgName() != null) {
            headerText.setText(getString(R.string.chat_header_text, RemoteDataManager.getRegistration().getOrg().getOrgName()));
        }
        CustomFontHelper.setCustomFont(headerText, "source-sans-pro.semibold.ttf", this);


        TextView getHelpViaTalkorText = (TextView) findViewById(R.id.get_help_via_talk_or_text);
        CustomFontHelper.setCustomFont(getHelpViaTalkorText, "source-sans-pro.semibold.ttf", this);

        TextView button_messenger_text = (TextView) findViewById(R.id.button_messenger);
        CustomFontHelper.setCustomFont(button_messenger_text, "source-sans-pro.semibold.ttf", this);

        //Check if we have crisis center added or not
        if (RemoteDataManager.getRegistration().getCrisisCenter() != null) {
            ArrayList<CrisisCenter> crisisCenters = new ArrayList<>();
            crisisCenters.add(RemoteDataManager.getRegistration().getCrisisCenter());

            crisisContactListAdapter = new CrisisContactListAdapter(this, R.layout.helpcontacts, crisisCenters);
            crisisContactList = (ListView) findViewById(R.id.crisis_contact_list);
            crisisContactList.setAdapter(crisisContactListAdapter);
        }

        contactListAdapter = new HelpContactListAdapter(this, R.layout.helpcontacts, RemoteDataManager.getUserContacts().getHelpContacts());
        contactList = (ListView) findViewById(R.id.contact_list);
        contactList.setAdapter(contactListAdapter);

        CrownMolding chatButton = (CrownMolding) findViewById(R.id.messenger_background);

        if (RemoteDataManager.getRegistration().getOrg().getIncludeMessenger()) {
            chatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refreshRegistrationPriorToChat();
                }
            });
        }

        unreadCount = (TextView)findViewById(R.id.unread_count);

        AnalyticsManager.sendEvent(AnalyticsManager.ANALYTICS_CATEGORY_USER_ACTIVITY, AnalyticsManager.ANALYTICS_ACTION_OPENED_GET_HELP_SCREEN, AnalyticsManager.ANALYTICS_ACTION_OPENED_GET_HELP_SCREEN);

        if (RemoteDataManager.getSavedOrgType(this) == RemoteDataManager.ORG_TYPE_WORKPLACE) {
            DottedRibbon dottedRibbon = (DottedRibbon) findViewById(R.id.dotted_ribbon);
            chatButton.setMode(CrownMolding.CROWN_MOLDING_MODE.GREEN_MOLDING);
            dottedRibbon.setProMode(true);
            headerText.setTextColor(ContextCompat.getColor(this, R.color.pro_app_text_maroon));
            getHelpViaTalkorText.setTextColor(ContextCompat.getColor(this, R.color.pro_app_text_maroon));
        }else{
            chatButton.setMode(CrownMolding.CROWN_MOLDING_MODE.HELP_MOLDING);
        }

        if (RemoteDataManager.getRegistration().getOrg().getIncludeMessenger()) {
            chatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refreshRegistrationPriorToChat();
                }
            });
        }else {
            chatButton.setMode(CrownMolding.CROWN_MOLDING_MODE.DISABLED_CRISIS_CENTER);
        }
    }
    private void refreshRegistrationPriorToChat()
    {
        showProgress(getResources().getString(R.string.message_initializing));
        String intellicode = RemoteDataManager.getIntellicode(this);
        if((intellicode!= null) && (intellicode.length()>0))
        {
            RemoteDataManager.registerDevice(this, intellicode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject json) {

                    if (json.getClass().equals(JSONObject.class)) {
                        try {

                            String code = json.getString("code");
                            if (code.equals(RemoteDataManager.RESPONSE_OK)) {

                                if (json.has("data")) {
                                    progressDialog.dismiss();

                                    if (RemoteDataManager.getRegistration().getOrg().isInRecess()) {
                                        String tag = "recess_dialog";
                                        DialogFragment recessFragment =
                                                RecessDialog.newInstance(getResources().getString(R.string.incident_recess_text));
                                        recessFragment.show(getSupportFragmentManager(), tag);
                                    } else {

                                        gotoChatScreen();
                                    }
                                }
                            } else if (code.equals(RemoteDataManager.RESPONSE_DEACTIVATED_USER)) {
                                //User is blocked, we need to show blocked screen
                                Intent blockedIntent = new Intent(GetHelpActivity.this, BlockedGateActivity.class);
                                GetHelpActivity.this.startActivity(blockedIntent);
                            }
                        } catch (JSONException je) {
                            progressDialog.dismiss();
                            AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(GetHelpActivity.this);
                            anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                            anonymousAlert.setCancelable(false);
                            anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            progressDialog.dismiss();
                                            refreshRegistrationPriorToChat();
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert = anonymousAlert.create();
                            alert.show();

                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    //show alert
                    progressDialog.dismiss();
                    AlertDialog.Builder anonymousAlert = new AlertDialog.Builder(GetHelpActivity.this);
                    anonymousAlert.setMessage(getResources().getString(R.string.no_server));
                    anonymousAlert.setCancelable(false);
                    anonymousAlert.setPositiveButton(getResources().getString(R.string.RETRY),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    progressDialog.dismiss();
                                    refreshRegistrationPriorToChat();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert = anonymousAlert.create();
                    alert.show();
                }
            });
        }
    }
}
